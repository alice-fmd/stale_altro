/**
 * 
 */
package core.modules.u2f;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import javax.swing.JButton;

import core.global.GlobalVariables;
import core.utilities.HexConversion;

/**
 * @author okhalid
 *
 */
public class Register {
	
	private String Name;
	private String Data="Initialised";
	private int Access;
	private int BitLength;
	private String BaseAddress;
	private JTextField guiDestination;
	private JButton readBtn;
	private JButton writeBtn;
	private ReadWriteButtonHandler handler ;
	public Register(){}
	
	public Register(String name, int access, int bits, String base) 
	{
		setName(name);
		setAccess(access);
		setRegBitLength(bits);
		setRegBaseAddress(base);
	}
	
	public Register(String name, int access, int bits, String base, JTextField guiDestiny) 
	{
		setName(name);
		setAccess(access);
		setRegBitLength(bits);
		setRegBaseAddress(base);
		setGuiDestination(guiDestiny);
	}
	
	public Register(String name, int access, int bits, String base, JTextField guiDestiny, JButton read, JButton write) 
	{
		setName(name);
		setAccess(access);
		setRegBitLength(bits);
		setRegBaseAddress(base);
		setGuiDestination(guiDestiny);
		setGuiButtons(read, write);
	}
	
	public void setName(String name)
	{
		Name = name;
	}
	
	public void setData(String regData)
	{
		Data=regData;
		updateGuiDestination();
	}
	
	public void setData(int regData)
	{
		Data="0x" + Integer.toHexString(regData);
		updateGuiDestination();
	}
	
	public void setAccess(int regAccess)
	{
		Access=regAccess;
	}
	
	public void setRegBitLength(int regBitLength)
	{
		BitLength = regBitLength;
	}
	
	public void setRegBaseAddress(String regBaseAddress)
	{
		BaseAddress = regBaseAddress;
	}
	
	private void setGuiDestination(JTextField guiDestiny)
	{
		handler = new ReadWriteButtonHandler();
		guiDestination = guiDestiny;
		guiDestination.addActionListener(handler);
	}
	
	private void setGuiButtons(JButton read, JButton write)
	{
		readBtn = read;
		writeBtn = write;
	}
	
	private void updateGuiDestination()
	{
		guiDestination.setText(getData());
	}
	
	public String getName()
	{
		return Name;
	}
	
	public String getData()
	{
		return Data;
	}
	
	public int getAccess()
	{
		return Access;
	}
	
	public int getRegBitLength()
	{
		return BitLength;
	}
	
	public String getRegBaseAddress()
	{
		return BaseAddress;
	}
	
	private void setRWButtonStatus()
	{
		if(getAccess() == GlobalVariables.READ_ONLY)
		{
			readBtn.setEnabled(true);
			readBtn.setEnabled(false);
		}
		else
		{
			readBtn.setEnabled(true);
			readBtn.setEnabled(true);
		}
	}
	/***************************************************************************
	 * inner class to handle action events for comment text box
	 **************************************************************************/

	public class ReadWriteButtonHandler implements ActionListener
	    {
	      	public void actionPerformed( ActionEvent event )
	      	{
					if ( event.getSource() == guiDestination ) 
					{
						setRWButtonStatus();
					}
			}
	    }
}
