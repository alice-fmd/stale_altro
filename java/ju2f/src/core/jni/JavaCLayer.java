package core.jni;

import core.global.GlobalVariables;

/**
 * @author okhalid
 *
 */
public class JavaCLayer {

	/**
	 * 
	 */
    	/* @SuppressWarnings("unused") */
	private int data, regCode=1;
	public JavaCLayer() {}
	
	public void run()
	{
		//data = readBoardController(regCode);
		//readBoardController(regCode);
		//system.out.print("\n\nPrinting from Java:" + data + "\n");
		readU2FReg();
	}
	
	public void readU2FReg()
	{
		int ret;
		ret = readU2FRegisters(GlobalVariables.U2FRegisterData);
	}
	public String messageToConsole()
	{
		String message = new String();
		message = "Value received from the register:" + data;
		return message;
	}
	
	public native int readBoardController(int regCode);
	public native int readU2FRegisters(int[] array);
	
	static
	{
	    // System.out.println(System.getProperty("java.library.path"));
		System.loadLibrary("jniu2f");
	}
}
