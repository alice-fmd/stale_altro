/****************************************************************/
/*                                                              */
/*  file: altroscope.c                                          */
/*                                                              */
/* This program allows to access the resources of a ALTRO       */
/* card in a user friendly way and includes some test routines  */
/*                                                              */
/*  Author: Markus Joos, CERN-EP                                */
/*                                                              */
/*  16. Mar. 04  MAJO  created                                  */
/*                                                              */
/****************C 2006 - A nickel program worth a dime**********/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>
#include <tools/get_input.h>
#include "altro.h"


/***********/
/* Globals */
/***********/
int handle, occ = 0;


/*************/
/* Prototypes*/
/*************/
int mainhelp(void);
int dumpdesc(void);
int dumpmem(void);
int altromenu(void);
int funcmenu(void);
int setdebug(void);


/****************/
int setdebug(void)
/****************/
{
  int tl;

  printf("Enter the debug level: ");
  tl = getdecd(20);
  rcc_error_set_debug(P_ID_ALTRO, tl);
  return(0);
}


/****************/
int mainhelp(void)
/****************/
{
  printf("Call Markus Joos, 72364, 160663 if you need help\n");
  return(0);
}


/****************/
int dumpdesc(void)
/****************/
{
  printf("This function has not yet been implemented\n");
  return(0);
}


/***************/
int dumpmem(void)
/***************/
{
  int ret, loop;
  altro_bulk_in_t in;
  char nodename[100];
  
  sprintf(nodename, "%s%d", BASE_NODE, occ);
  ret = ALTRO_Open(nodename, &handle);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }

  ret = ALTRO_Get(handle, &in);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }
  
  printf("%d bytes received\n", in.nbytes);
  for (loop = 0; loop < in.nbytes; loop++)
  {
    printf("Byte %d = %x\n", loop, in.data[loop]);
  }
  
  ret = ALTRO_Close(handle);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }  

  return(0);
}


/******************************/
int main(int argc, char *argv[])
/******************************/
{  
  static int fun = 1;
  
  if ((argc == 2) && (sscanf(argv[1], "%d", &occ) == 1)) {argc--;} else {occ = 0;}
  if (argc != 1)
  {
    printf("This is altroscope.\n\n");
    printf("Usage: altroscope [ALTRO occurrence]\n");
    printf("The occurrence is the number of an altro card as assigned by the driver.\n");
    printf("It matches the number of the device node. (e.g. /dev/usb/altro1)\n");
    printf("Legal values are currently 0 to 4\n");
    exit(0);
  }
  
  printf("\n\n\nThis is altroscope\n");
  while(fun != 0)
  {
    printf("\n");
    printf("Select an option:\n");
    printf("  1 Print help\n");
    printf("  2 ALTRO menu\n");
    printf("  3 Set debugging parameters\n");
    printf("  0 Quit\n");
    printf("Your choice ");
    fun = getdecd(fun);
    if (fun == 1) mainhelp();
    if (fun == 2) altromenu();
    if (fun == 3) setdebug();
  }

  exit(0);
}


/*****************/
int altromenu(void)
/*****************/
{  
  static int fun;
 
  fun = 1; 
  printf("\n\nThis is the sub-menu for the functions of the ALTRO driver and library\n");
  while(fun != 0)
  {
    printf("\n");
    printf("Select an option:\n");
    printf("  1 Dump USB descriptor\n");
    printf("  2 Dump ALTRO memory\n");
    printf("  3 Execute funtions\n");
    printf("  0 Quit\n");
    printf("Your choice ");
    fun = getdecd(fun);
    if (fun == 1) dumpdesc();
    if (fun == 2) dumpmem();
    if (fun == 3) funcmenu();
  }

  return(0);
}


/****************/
int funcmenu(void)
/****************/
{  
  static int ret, fun, loop;
  altro_bulk_out_t out;
  altro_bulk_in_t in;
  altro_control_t cnfg;
  char dnodename[100], nodename[100];
  
  fun = 1;
  while(fun != 0)
  {
    printf("\n");
    printf("Select an option:\n");
    printf("  1 ALTRO_Open\n");
    printf("  2 ALTRO_Close\n");
    printf("  3 ALTRO_Send\n");
    printf("  4 ALTRO_Get\n");
    printf("  5 ALTRO_Config\n");
    printf("  0 Quit\n");
    printf("Your choice ");
    fun = getdecd(fun);

    if (fun == 1) 
    {
      printf("Enter the path and file name of the node: ");
      sprintf(dnodename, "%s%d", BASE_NODE, occ);
      getstrd(nodename, dnodename);
      ret = ALTRO_Open(nodename, &handle);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
    }
 
    if (fun == 2) 
    {
      ret = ALTRO_Close(handle);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
    }    

    if (fun == 3) 
    {
      printf("How many bytes do you want to send: ");
      out.nbytes = getdecd(1);
      for (loop = 0; loop < out.nbytes; loop++)
      {
        printf("Enter byte %d\n", loop);
	out.data[loop] = getdecd(0);
      }
     
      ret = ALTRO_Send(handle, &out);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }    
    }    

    if (fun == 4)
    {
      ret = ALTRO_Get(handle, &in);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
      printf("%d bytes received\n", in.nbytes);    
    } 
    
    if (fun == 5)
    {    
      printf("Enter the request code (8 bit): ");
      cnfg.bRequest = getdecd(0);      
          
      printf("Enter the request type (8 bit): ");
      cnfg.bmRequestType = getdecd(0);  

      printf("Enter the request code (8 bit): ");
      cnfg.bRequest = getdecd(0);  

      printf("Enter the Value (16 bit): ");
      cnfg.wValue = getdecd(0);  

      printf("Enter the Index (16 bit): ");
      cnfg.wIndex = getdecd(0);  

      printf("Enter the numer of data bytes: ");
      cnfg.wLength = getdecd(0);  

      if ((cnfg.bmRequestType & 0x80) == 0)  /*end data to the device*/
      {
	for (loop = 0; loop < cnfg.wLength; loop++)
	{
          printf("Enter data byte %d\n", loop);
	  cnfg.data[loop] = getdecd(0);
	}
      }
      
      ret = ALTRO_Control(handle, &cnfg);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }    
    }     
    
  }
  return(0);
}




