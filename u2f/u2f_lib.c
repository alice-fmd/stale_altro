/************************************************************************/
/*									*/
/*  This is the U2F library 	   				        */
/*									*/
/*  24. Jun. 04  MAJO  created						*/
/*									*/
/*******C 2004 - The software with that certain something****************/

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/file.h>
#include <fcntl.h>
#include "u2f.h"
#define U2F_DEBUG(l,txt) DEBUG_TEXT(P_ID_U2F,l,txt);
#define U2FISOPEN(handle) do {if(!devices[handle].is_open) \
	return(U2F_NOTOPEN);} while(0) 

/***********/
/* Globals */
/***********/
struct u2f_device 
{
  int         is_open;          /* Reference count */
  int         mode;
  int         init;
  u2f_bank_t  imem;
  u2f_bank_t  rmem;
  u2f_bank_t  pmem;
  u2f_bank_t  acl;
  u2f_bank_t  dmh1;
  u2f_bank_t  dml1;
  u2f_bank_t  dmh2;
  u2f_bank_t  dml2;
  u2f_bank_t  registers;
} devices[MAX_U2F_DEVICES] = { { 0, 0, 0 } };
  
// static u_int init = 0, u2f_is_open[MAX_U2F_DEVICES] = {0};
extern int traceLevel, packageId;
// int emode;
// u2f_bank_t u2f_imem, u2f_rmem, u2f_pmem, u2f_acl, u2f_dmh1, 
//   u2f_dml1, u2f_dmh2, u2f_dml2, u2f_registers;

static void write_dummy_data(int handle, u_int which, u_int chip, 
			     u_int chan, u_int len); 
static void write_next_data(int handle, u_int which, u_int *chip, 
			    u_int *chan, u_int len);

/***********************************************/
static u_int U2F_banks_alloc(u2f_bank_t* bank) 
/***********************************************/
{
  U2F_DEBUG(15 ,(" U2F_banks_alloc: called\n"));

  if (!bank) return(RCC_ERROR_RETURN(0, U2F_SUCCESS));

  bank->data = (u_int*)malloc(sizeof(u_int)*bank->size);
  if (!bank->data) 
  {
    U2F_DEBUG(5,
	      ("U2F_banks_alloc: Failed to allocate memory for data bank\n"));
    return(RCC_ERROR_RETURN(0, U2F_MALLOC));
  }
  U2F_DEBUG(15 ,(" U2F_banks_alloc: done\n"));
  return(0);
}

/******************************/
static u_int U2F_banks_init(int handle)
/******************************/
{
  int ret;

  U2F_DEBUG(15 ,(" U2F_banks_init: called\n"));  
  if (devices[handle].init > 0) 
  {
    devices[handle].init++;
    return(RCC_ERROR_RETURN(0, U2F_SUCCESS));
  }
  if (devices[handle].mode == HW) {
    devices[handle].init = 0;
    return(RCC_ERROR_RETURN(0, U2F_SUCCESS));
  }
  ret = U2F_banks_alloc(&(devices[handle].imem));
  if (ret) return(RCC_ERROR_RETURN(0, U2F_MALLOC));

  ret = U2F_banks_alloc(&(devices[handle].pmem));
  if (ret) return(RCC_ERROR_RETURN(0, U2F_MALLOC));

  ret = U2F_banks_alloc(&(devices[handle].rmem));
  if (ret) return(RCC_ERROR_RETURN(0, U2F_MALLOC));

  ret = U2F_banks_alloc(&(devices[handle].acl));
  if (ret) return(RCC_ERROR_RETURN(0, U2F_MALLOC));

  ret = U2F_banks_alloc(&(devices[handle].dmh1));
  if (ret) return(RCC_ERROR_RETURN(0, U2F_MALLOC));

  ret = U2F_banks_alloc(&(devices[handle].dml1));
  if (ret) return(RCC_ERROR_RETURN(0, U2F_MALLOC));

  ret = U2F_banks_alloc(&(devices[handle].dmh2));
  if (ret) return(RCC_ERROR_RETURN(0, U2F_MALLOC));

  ret = U2F_banks_alloc(&(devices[handle].dml2));
  if (ret) return(RCC_ERROR_RETURN(0, U2F_MALLOC));

  ret = U2F_banks_alloc(&(devices[handle].registers));
  if (ret) return(RCC_ERROR_RETURN(0, U2F_MALLOC));

  U2F_DEBUG(15 ,(" U2F_banks_init: done\n"));
  return(RCC_ERROR_RETURN(0, U2F_SUCCESS));
}


/***************************************/
static void U2F_banks_destroy(int handle)
/***************************************/
{
  if (--(devices[handle].init) != 0) return;

  if (devices[handle].mode == HW) return;
  else
    {
      free(devices[handle].imem.data);
      free(devices[handle].pmem.data);
      free(devices[handle].rmem.data);
      free(devices[handle].acl.data);
      free(devices[handle].dmh1.data);
      free(devices[handle].dml1.data);
      free(devices[handle].dmh2.data);
      free(devices[handle].dml2.data);
      free(devices[handle].registers.data);
    }
}


/***********************************************/
u_int U2F_Open(char *node, int* handle, int mode)
/***********************************************/
{  
  u_int ret;
 
  U2F_DEBUG(15 ,(" U2F_Open: called\n"));
  /* open the error package */
  ret = rcc_error_init((err_pid)P_ID_U2F, U2F_err_get);
  if (ret)
  {
    U2F_DEBUG(5 ,(" U2F_Open: Failed to open error package\n"));
    return(RCC_ERROR_RETURN(0, U2F_ERROR_FAIL));
  }
  U2F_DEBUG(20 ,(" U2F_Open: error package opened\n"));

  if (mode == HW) {
    ret = ALTRO_Open(node, handle);
    if (ret)
    {
      U2F_DEBUG(5,(" U2F_Open: Failed to open the ALTRO library\n"));
      return(RCC_ERROR_RETURN(ret, U2F_ALTRO_FAIL));
    }
  }
  else {
    /* convert from ASCII to decimal */
    sscanf(&node[strlen(node) - 1], "%d", handle);  
  }
  U2F_DEBUG(10 ,(" U2F_Open: ALTRO library opened\n"));
  U2F_DEBUG(10 ,(" U2F_Open: handle = %d\n", *handle));

  if (devices[*handle].is_open) 
  {
    devices[*handle].is_open++; /* keep track of multiple open calls */
    U2F_DEBUG(15 ,(" U2F_Open: done\n"));
    return(RCC_ERROR_RETURN(0, U2F_SUCCESS));
  }

  devices[*handle].mode = mode;
  devices[*handle].imem.id    = A_IMEM;
  devices[*handle].imem.size  = S_IMEM;
  devices[*handle].imem.mask1 = 0xffff;
  devices[*handle].imem.mask2 = 0x00ff;
  devices[*handle].imem.data  = 0;

  devices[*handle].pmem.id    = A_PMEM;
  devices[*handle].pmem.size  = S_PMEM;
  devices[*handle].pmem.mask1 = 0xffff;
  devices[*handle].pmem.mask2 = 0x0;
  devices[*handle].pmem.data  = 0;

  devices[*handle].rmem.id    = A_RMEM;
  devices[*handle].rmem.size  = S_RMEM;
  devices[*handle].rmem.mask1 = 0xffff;
  devices[*handle].rmem.mask2 = 0x000f;
  devices[*handle].rmem.data  = 0;

  devices[*handle].acl.id     = A_ACL;
  devices[*handle].acl.size   = S_ACL;
  devices[*handle].acl.mask1  = 0xffff;
  devices[*handle].acl.mask2  = 0x0;
  devices[*handle].acl.data   = 0;

  devices[*handle].dmh1.id    = A_DMH1;
  devices[*handle].dmh1.size  = S_DM;
  devices[*handle].dmh1.mask1 = 0xffff;
  devices[*handle].dmh1.mask2 = 0x000f;
  devices[*handle].dmh1.data  = 0;

  devices[*handle].dml1.id    = A_DML1;
  devices[*handle].dml1.size  = S_DM;
  devices[*handle].dml1.mask1 = 0xffff;
  devices[*handle].dml1.mask2 = 0x000f;
  devices[*handle].dml1.data  = 0;

  devices[*handle].dmh2.id    = A_DMH2;
  devices[*handle].dmh2.size  = S_DM;
  devices[*handle].dmh2.mask1 = 0xffff;
  devices[*handle].dmh2.mask2 = 0x000f;
  devices[*handle].dmh2.data  = 0;

  devices[*handle].dml2.id    = A_DML2;
  devices[*handle].dml2.size  = S_DM;
  devices[*handle].dml2.mask1 = 0xffff;
  devices[*handle].dml2.mask2 = 0x000f;
  devices[*handle].dml2.data  = 0;

  devices[*handle].registers.id    = A_REGISTERS;
  devices[*handle].registers.size  = O_TRCFG2 + 1;
  devices[*handle].registers.mask1 = 0xffff;
  devices[*handle].registers.mask2 = 0xffff;
  devices[*handle].registers.data  = 0;

  /* setup the instruction memory bank information */
  U2F_banks_init(*handle);

  devices[*handle].is_open = 1;
  U2F_DEBUG(10 ,
	     (" U2F_Open: ALTRO library opened for handle %d\n", *handle));
  U2F_DEBUG(15 ,(" U2F_Open: done\n"))
  
  return(U2F_SUCCESS);
}


/*************************/
u_int U2F_Close(int handle)
/*************************/
{
  u_int ret = 0;
  
  U2F_DEBUG(15 ,(" U2F_Close: called\n"));
  U2FISOPEN(handle);
    
  if (devices[handle].is_open > 1) devices[handle].is_open--;
  else 
  {  
    if (devices[handle].mode == HW)
    {
      ret = ALTRO_Close(handle);
      if (ret) 
      {
	U2F_DEBUG(5, (" U2F_Close: Failed to close the ALTRO library "
		      "for handle %d\n", handle));
	return(RCC_ERROR_RETURN(ret, U2F_ALTRO_FAIL)); 
      }
      U2F_DEBUG(10, (" U2F_Close: ALTRO library closed for handle %d\n", 
		     handle));    
    }
    U2F_banks_destroy(handle);
    devices[handle].is_open = 0;
  }
  
  return(U2F_SUCCESS);
}


/*********************************************************************/
static u_int U2F_Memory_Read(int handle, u2f_bank_t* bank, u_int isize, 
			     u_int* osize, u_int offset, u_int* data)
/*********************************************************************/
{
  altro_bulk_out_t out;
  altro_bulk_in_t in;
  u_short *msg;
  u_int ret, msize, loop, *ptr;
  err_str pid, code;

  U2F_DEBUG(15 ,(" U2F_Memory_Read: called\n"));
  U2FISOPEN(handle);

  U2F_DEBUG(20 ,(" U2F_Memory_Read: bank = 0x%04x isize = %d\n", 
			    bank->id, isize));

  msize = isize * 2;    /* one word in the IMEM takes two 16 bit words
			   in the message */ 
 
  if ((isize + offset) > bank->size) 
  {    
    U2F_DEBUG(5 , 
	       ("U2F_Memory_Read: Parameter isize is out of range [0,%d]\n", 
		bank->size));
    return(RCC_ERROR_RETURN(0, U2F_RANGE));
  } 

  if (devices[handle].mode == SW)
  {
    U2F_DEBUG(20,("U2F_Memory_Read: Emulation: isize      = %d\n",isize));
    U2F_DEBUG(20,("U2F_Memory_REad: Emulation: offset     = %d\n",offset));
    U2F_DEBUG(20,("U2F_Memory_Read: Emulation: bank->id   = %d\n",bank->id));
    U2F_DEBUG(20,("U2F_Memory_Read: Emulation: bank->size = %d\n",bank->size));
    *osize = isize;
    for (loop = 0; loop < isize; loop++) 
      data[loop] = bank->data[loop + offset];     
    return(U2F_SUCCESS);
  }

  /* Construct a message - First 4 bits are the instruction, and one
     word in a block takes two 16 bit words in the message */ 
  msg        = (u_short *)&out.data[0];
  msg[0]     = (isize << 5) + I_READ; 
  msg[1]     = bank->id + offset;
  out.nbytes = 4;

  U2F_DEBUG(20 ,("U2F_Memory_Read: msg[0] = 0x%08x\n", msg[0]));
  U2F_DEBUG(20 ,("U2F_Memory_Read: msg[1] = 0x%08x\n", msg[1]));

  /* Tell the USB2FEC that we want to read the a block */
  ret = ALTRO_Send(handle, &out);
  if (ret) 
  {    
    err_str pid, code;
    ALTRO_err_get(ret, pid, code);
    U2F_DEBUG(5,("U2F_Memory_Read: Error from ALTRO_Send %s %s\n",pid, code));
    return(RCC_ERROR_RETURN(ret, U2F_ALTRO_FAIL)); 
  }    

  /* Now read the data */
  ret = ALTRO_Get(handle, &in);
  if (ret) 
  {
    ALTRO_err_get(ret, pid, code);
    U2F_DEBUG(5,("U2F_Memory_Read: Error (%d) from ALTRO_Get: %s\n",ret,code));
    return(RCC_ERROR_RETURN(ret, U2F_ALTRO_FAIL)); 
  }

  U2F_DEBUG(20 , ("U2F_Memory_Read: %d bytes received from the USB2FEC\n", 
		  in.nbytes));
  if (in.nbytes != (msize * 2)) 
  {
    U2F_DEBUG(5 , ("U2F_Memory_Read: Wrong number of bytes received: "
		   "was %d, should be %d\n", in.nbytes, msize * 2));
    *osize = 0;
    return(RCC_ERROR_RETURN(ret, U2F_ODD_BYTES)); 
  }

  /* We got all we needed */
  *osize = isize;
  ptr    = (u_int *) &in.data[0];
  /* copy the data from the altro_bulk_in_t structure to the data array */

  for(loop = 0; loop < *osize; loop++) 
  { 
    data[loop] = ptr[loop];
    U2F_DEBUG(30 , ("\tData # %3d:\t0x%08x 0x%04x 0x%04x\n", 
		    loop, data[loop], in.data[2*loop], in.data[2*loop+1]));
  }
  U2F_DEBUG(15 ,(" U2F_Memory_Read: done\n"));
  return(U2F_SUCCESS);
}


/**********************************************************************/
u_int U2F_IMEM_Read(int handle, u_int isize, u_int *osize, u_int offset, 
		    u_int data[])
/**********************************************************************/
{
  int ret;
  
  U2F_DEBUG(15 ,(" U2F_IMEM_Read: called\n"));
  U2F_DEBUG(20 ,("U2F_IMEM_Read: isize = %d\n", isize));
  ret = U2F_Memory_Read(handle, &(devices[handle].imem), isize, osize, 
			offset, data);
  U2F_DEBUG(15 ,(" U2F_IMEM_Read: done\n"));
  return ret;
}


/**********************************************************************/
u_int U2F_PMEM_Read(int handle, u_int isize, u_int *osize, u_int offset, 
		    u_int data[])
/**********************************************************************/  
{
  int ret;
  
  U2F_DEBUG(15 ,(" U2F_PMEM_Read: called\n"));
  U2F_DEBUG(20 ,("U2F_PMEM_Read: isize = %d\n", isize));
  if (isize > B_PMEM)  
  {    
    printf("\nNumber of words cannot exceed 256 words\n"); 
    U2F_DEBUG(5 , ("U2F_PMEM_Read: Parameter isize is out of range\n"));
    return(RCC_ERROR_RETURN(0, U2F_RANGE));
  }   
  ret = U2F_Memory_Read(handle, &(devices[handle].pmem), isize, osize, 
			offset, data);
  U2F_DEBUG(15 ,(" U2F_PMEM_Read: done\n"));
  return ret;
}
		
  
/**********************************************************************/
u_int U2F_RMEM_Read(int handle, u_int isize, u_int *osize, u_int offset, 
		    u_int data[])
/**********************************************************************/
{
  int ret;
  
  U2F_DEBUG(15 ,(" U2F_RMEM_Read: called\n"));
  U2F_DEBUG(20 ,("U2F_IMEM_Read: isize = %d\n", isize));
  ret = U2F_Memory_Read(handle, &(devices[handle].rmem), isize, osize, 
			offset, data);
  U2F_DEBUG(15 ,(" U2F_RMEM_Read: done\n"));
  return ret;
}

  
/*********************************************************************/
u_int U2F_ACL_Read(int handle, u_int isize, u_int *osize, u_int offset, 
		   u_int data[])
/*********************************************************************/
{
  int ret;
  U2F_DEBUG(15 ,(" U2F_ACL_Read: called\n"));
  U2F_DEBUG(20 ,("U2F_IMEM_Read: isize = %d\n", isize));
  ret = U2F_Memory_Read(handle, &(devices[handle].acl), isize, osize, 
			offset, data);
  U2F_DEBUG(15 ,(" U2F_ACL_Read: done\n"));
  return ret;
}


/*******************************************************************/
u_int U2F_DM_Read(int handle, u_int which, u_int isize, u_int *osize, 
		  u_int offset, u_int data[])
/*******************************************************************/
{
  static u_int low[2048], high[2048];
  u_int losize, hosize, ret, loop;
  
  u2f_bank_t* lbank = 0;
  u2f_bank_t* hbank = 0;
  U2F_DEBUG(15 ,(" U2F_DM_Read: called\n"));
  switch (which) 
  {
    case 1:
      lbank = &(devices[handle].dml1);
      hbank = &(devices[handle].dmh1);
      break;
    case 2:
      lbank = &(devices[handle].dml2);
      hbank = &(devices[handle].dmh2);
      break;
    default:
      U2F_DEBUG(5, ("Unknown bank #:%d\n", which));
      return(RCC_ERROR_RETURN(0, U2F_RANGE));
  }
  
  ret = U2F_Memory_Read(handle, lbank, isize, &losize, offset, low);
  if (ret)
  {
   *osize = 0;
   return ret;
  }
  
  ret = U2F_Memory_Read(handle, hbank, isize, &hosize, offset, high);
  if (ret)
  {
   *osize = 0;
   return ret;
  }
  
  if (losize != hosize) 
  {
    U2F_DEBUG(5 , ("U2F_DM_Read: Inconsistent number of bytes received\n"));
    *osize = 0;
    return(RCC_ERROR_RETURN(ret, U2F_INC_BYTES)); 
  }

  if (losize != isize) 
  {
    U2F_DEBUG(5 , ("U2F_DM%d_Read: Wrong number (%d) of words received "
		   "for the low bits, should be %d\n", which, losize, isize));
    *osize = 0;
    return(RCC_ERROR_RETURN(ret, U2F_ODD_BYTES)); 
  }
  
  if (hosize != isize) 
  {
    U2F_DEBUG(5, ("U2F_DM%d_Read: Wrong number (%d) of words received "
		  "for the hight bits, should be %d\n", which, hosize, isize));
    *osize = 0;
    return(RCC_ERROR_RETURN(ret, U2F_ODD_BYTES)); 
  }
  
  *osize = isize;
  /* copy the data from the altro_bulk_in_t structures to the data array */
  for(loop = 0; loop < *osize; loop++)
  {
    data[4*loop]   = low[loop]          & 0x3ff;
    data[4*loop+1] = (low[loop] >> 10)  & 0x3ff;
    data[4*loop+2] = high[loop]         & 0x3ff;
    data[4*loop+3] = (high[loop] >> 10) & 0x3ff;
  }
  
  U2F_DEBUG(15 ,(" U2F_DM_Read: done\n"));
  return(U2F_SUCCESS); 
}


/***********************************************************************/
u_int U2F_DM1_Read(int handle, u_int isize, u_int *osize, u_int offset, 
		   u_int data[])
/***********************************************************************/
{
  int ret;
  U2F_DEBUG(15 ,(" U2F_DM1_Read: called\n"));
  ret = U2F_DM_Read(handle, 1, isize, osize, offset, data);
  U2F_DEBUG(15 ,(" U2F_DM1_Read: done\n"));
  return(ret);
}


/***********************************************************************/
u_int U2F_DM2_Read(int handle, u_int isize, u_int *osize, u_int offset, 
		   u_int data[])
/***********************************************************************/
{
  int ret;
  U2F_DEBUG(15 ,(" U2F_DM2_Read: called\n"));
  ret = U2F_DM_Read(handle, 2, isize, osize, offset, data);
  U2F_DEBUG(15 ,(" U2F_DM2_Read: done\n"));
  return(ret);
}


/****************************************************/
u_int U2F_Reg_Read(int handle, u_int reg, u_int *data)
/****************************************************/
{
  u_int osize;

  U2F_DEBUG(15 ,(" U2F_Reg_Read: called\n"));
  U2F_DEBUG(10, ("U2F_Reg_Read: Function called for reg = %d\n", reg));
  if ((reg != O_ERRST)  && 
      (reg != O_TRCFG1) && 
      (reg != O_TRCNT)  && 
      (reg != O_IRADD)  && 
      (reg != O_IRDAT)  && 
      (reg != O_EVWORD) && 
      (reg != O_LWADD)  && 
      (reg != O_ACTFEC) && 
      (reg != O_FMIREG) && 
      (reg != O_FMOREG) && 
      (reg != O_TRCFG2))
  {    
    U2F_DEBUG(5, ("U2F_Reg_Read: Unknown register %d\n", reg));
    return(RCC_ERROR_RETURN(0, U2F_RANGE)); 
  }
  int ret = U2F_Memory_Read(handle, &(devices[handle].registers), 1, 
			    &osize, reg, data);
  if (devices[handle].mode == SW) {
    U2F_DEBUG(1, ("Read 0x%08X from register 0x%04X\n",*data, reg));
    if (reg == O_EVWORD && 
	devices[handle].registers.data[O_EVWORD] == 0x0 &&
	devices[handle].registers.data[O_TRCFG2] & 0x2) {
      U2F_DEBUG(1, ("Making hardware trigger in emulation mode\n"));
      U2F_Exec_Command(handle,C_SWTRG,0);
    }
  }
  return ret;
}  

/********************************************/
u_int U2F_FMIREG_Read(int handle, u_int* data)
/********************************************/
{
  int ret;
  
  U2F_DEBUG(15 ,(" U2F_FMIREG_Read: called\n"));
  ret =  U2F_Reg_Read(handle, O_FMIREG, data);
  U2F_DEBUG(15 ,(" U2F_FMIREG_Read: done\n"));
  return(ret);
}


/********************************************/
u_int U2F_FMOREG_Read(int handle, u_int* data)
/********************************************/
{
  int ret;
  
  U2F_DEBUG(15 ,(" U2F_FMOREG_Read: called\n"));
  ret =  U2F_Reg_Read(handle, O_FMOREG, data);
  U2F_DEBUG(15 ,(" U2F_FMOREG_Read: done\n"));
  return(ret);
}


/*******************************************/
u_int U2F_ERRST_Read(int handle, u_int* data)
/*******************************************/
{
  int ret;
  
  U2F_DEBUG(15 ,(" U2F_ERRST_Read: called\n"));
  ret =  U2F_Reg_Read(handle, O_ERRST, data);
  U2F_DEBUG(15 ,(" U2F_ERRST_Read: done\n"));
  return(ret);
}


/********************************************/
u_int U2F_TRCFG1_Read(int handle, u_int* data)
/********************************************/
{
  int ret;
  
  U2F_DEBUG(15 ,(" U2F_TRCFG1_Read: called\n"));
  ret =  U2F_Reg_Read(handle, O_TRCFG1, data);
  U2F_DEBUG(15 ,(" U2F_TRCFG1_Read: done\n"));
  return(ret);
}


/*******************************************/
u_int U2F_TRCNT_Read(int handle, u_int* data)
/*******************************************/
{
  int ret;
  
  U2F_DEBUG(15 ,(" U2F_TRCNT_Read: called\n"));
  ret =  U2F_Reg_Read(handle, O_TRCNT, data);
  U2F_DEBUG(15 ,(" U2F_TRCNT_Read: done\n"));
  return(ret);
}


/*******************************************/
u_int U2F_LWADD_Read(int handle, u_int* data)
/*******************************************/
{
  int ret;
  
  U2F_DEBUG(15 ,(" U2F_LWADD_Read: called\n"));
  ret =  U2F_Reg_Read(handle, O_LWADD, data);
  U2F_DEBUG(15 ,(" U2F_LWADD_Read: done\n"));
  return(ret);
}


/*******************************************/
u_int U2F_IRADD_Read(int handle, u_int* data)
/*******************************************/
{
  int ret;
  
  U2F_DEBUG(15 ,(" U2F_IRADD_Read: called\n"));
  ret =  U2F_Reg_Read(handle, O_IRADD, data);
  U2F_DEBUG(15 ,(" U2F_IRADD_Read: done\n"));
  return(ret);
}


/*******************************************/
u_int U2F_IRDAT_Read(int handle, u_int* data)
/*******************************************/
{
  int ret;
  
  U2F_DEBUG(15 ,(" U2F_IRDAT_Read: called\n"));
  ret =  U2F_Reg_Read(handle, O_IRDAT, data);
  U2F_DEBUG(15 ,(" U2F_IRDAT_Read: done\n"));
  return(ret);
}


/********************************************/
u_int U2F_EVWORD_Read(int handle, u_int* data)
/********************************************/
{
  int ret;
  
  U2F_DEBUG(15 ,(" U2F_EVWORD_Read: called\n"));
  ret =  U2F_Reg_Read(handle, O_EVWORD, data);
  U2F_DEBUG(15 ,(" U2F_EVWORD_Read: done\n"));
  return(ret);
}


/********************************************/
u_int U2F_ACTFEC_Read(int handle, u_int* data)
/********************************************/
{
  int ret;
  
  U2F_DEBUG(15 ,(" U2F_ACTFEC_Read: called\n"));
  ret =  U2F_Reg_Read(handle, O_ACTFEC, data);
  U2F_DEBUG(15 ,(" U2F_ACTFEC_Read: done\n"));
  return(ret);
}


/********************************************/
u_int U2F_RDOMOD_Read(int handle, u_int* data)
/********************************************/
{
  int ret;
  
  U2F_DEBUG(15 ,(" U2F_RDOMOD_Read: called\n"));
  ret =  U2F_Reg_Read(handle, O_TRCFG2, data);
  U2F_DEBUG(15 ,(" U2F_RDOMOD_Read: done\n"));
  return(ret);
}


/*****************************************************/
u_int U2F_ERRST_decode(u_int data, errst_t *parameters)
/*****************************************************/
{
  U2F_DEBUG(15 ,(" U2F_ERRST_decode: called\n"));
  parameters->pattern_error = data & 0x1; 
  parameters->abort         = (data >> 1) & 0x1;
  parameters->timeout       = (data >> 2) & 0x1;
  parameters->altro_error   = (data >> 3) & 0x1;
  
  U2F_DEBUG(15 ,(" U2F_ERRST_decode: done\n"));
  return(U2F_SUCCESS);
}


/******************************************************/
u_int U2F_TRCFG1_decode(u_int data, trcfg_t *parameters)
/******************************************************/
{
  U2F_DEBUG(15 ,(" U2F_TRCFG1_decode: called\n"));
  parameters->tw    =data & 0x3fff;
  parameters->bmd   =(data >> 14) & 0x1;
  parameters->mode  =(data >> 15) & 0x3;
  parameters->pop   =(data >> 18) & 0x1;
  parameters->remb  =(data >> 17) & 0xf;
  parameters->empty =(data >> 21) & 0x1;
  parameters->full  =(data >> 22) & 0x1;
  parameters->rd_pt =(data >> 23) & 0x7;
  parameters->wr_pt =(data >> 26) & 0x7;
  U2F_DEBUG(15 ,(" U2F_TRCFG1_decode: done\n"));

  return(U2F_SUCCESS);
}
  
  
/*****************************************************/
u_int U2F_TRCNT_decode(u_int data, trcnt_t *parameters)
/*****************************************************/
{
  U2F_DEBUG(15 ,(" U2F_TRCNT_decode: called\n"));
  parameters->nta = data & 0xffff;
  parameters->ntr = (data >> 16) & 0xffff;

  U2F_DEBUG(15 ,(" U2F_TRCNT_decode: done\n"));
  return(U2F_SUCCESS);
}

/**************************************************/
u_int U2F_ERRST_Get(int handle, errst_t *parameters) 
/**************************************************/
{
  u_int data, ret;
  
  U2F_DEBUG(15 ,(" U2F_ERRST_Get: called\n"));
  ret = U2F_ERRST_Read(handle, &data);
  if (ret) return ret;
  ret = U2F_ERRST_decode(data, parameters);
  U2F_DEBUG(15 ,(" U2F_ERRST_Get: done\n"));
  return ret;
}


/***************************************************/
u_int U2F_TRCFG1_Get(int handle, trcfg_t *parameters) 
/***************************************************/
{
  u_int data, ret;
  
  U2F_DEBUG(15 ,(" U2F_TRCFG1_Get: called\n"));
  ret = U2F_TRCFG1_Read(handle, &data);
  if (ret) return ret;
  ret = U2F_TRCFG1_decode(data, parameters);
  U2F_DEBUG(15 ,(" U2F_TRCFG1_Get: done\n"));
  return ret;
}


/**************************************************/
u_int U2F_TRCNT_Get(int handle, trcnt_t *parameters) 
/**************************************************/
{
  u_int data, ret;

  U2F_DEBUG(15 ,(" U2F_TRCNT_Get: called\n"));
  ret = U2F_TRCNT_Read(handle, &data);
  if (ret) return ret;
  ret = U2F_TRCNT_decode(data, parameters);
  U2F_DEBUG(15 ,(" U2F_TRCNT_Get: done\n"));
  return ret;
}

/**********************************************************************/
static u_int U2F_Memory_Write(int handle, u2f_bank_t* bank, u_int osize, 
			      u_int offset, u_int data[])
/**********************************************************************/
{
  altro_bulk_out_t out;
  u_short *msg, ptr;
  u_int ret, msize, loop;
  
  U2F_DEBUG(15 ,(" U2F_Memory_Write: called\n"));
  U2FISOPEN(handle);
  U2F_DEBUG(10 ,("U2F_Memory_Write: bank = 0x%04x  offset = 0x%04x  "
		 "osize = %d\n", bank->id, offset, osize));

  /* one word in the IMEM takes two 16 bit words in the message */
  msize = osize * 2;  
  if ((osize + offset) > bank->size) 
  {    
    U2F_DEBUG(5 , ("U2F_Memory_Write: Parameter osize (%d [%d]) is "
		   "out of range [0,%d)\n", osize, offset, bank->size));
    return(RCC_ERROR_RETURN(0, U2F_RANGE));
  } 
 
  if (devices[handle].mode == SW)
  {
    U2F_DEBUG(20,("U2F_Memory_Write: Emulation: osize      = %d\n", osize));
    U2F_DEBUG(20,("U2F_Memory_Write: Emulation: offset     = %d\n", offset));
    U2F_DEBUG(20,("U2F_Memory_Write: Emulation: bank->id   = 0x%0x4\n", 
		  bank->id));
    U2F_DEBUG(20,("U2F_Memory_Write: Emulation: bank->size = %d\n",
		  bank->size));
    for (loop = 0; loop < osize; loop++) 
      bank->data[loop + offset] = (data[loop] & 
				   (bank->mask1 + (bank->mask2 << 16)));
    return(U2F_SUCCESS);
  }
 
  /* Construct a message */
  msg    = (u_short *)&out.data[0];
  msg[0] = (osize << 5) + I_WRITE; 
  msg[1] = bank->id + offset;
  ptr    = 2;
  for(loop = 0; loop < osize; loop++) 
  {
    msg[ptr]   = data[loop] & bank->mask1;
    msg[ptr+1] = (bank->mask2 ? (data[loop] >> 16) & bank->mask2 : 0);
    U2F_DEBUG(30 , ("\tData # %3d:\tx%08x 0x%04x 0x%04x\n", 
		    loop, data[loop], msg[ptr], msg[ptr+1]));
    ptr += 2;
  } 
  out.nbytes = (2 + msize) * 2;  /*  16 bit word to byte */

  /* Tell the USB2FEC that we want to write the memory bank */ 
  ret = ALTRO_Send(handle, &out);
  if (ret) 
  {      
    U2F_DEBUG(5 ,("U2F_Memory_Write: Error from ALTRO_Send\n"));
    return(RCC_ERROR_RETURN(ret, U2F_ALTRO_FAIL)); 
  }    
  U2F_DEBUG(20 ,("U2F_Memory_Write: done (ret = %d)\n", ret));

  return(U2F_SUCCESS);  
}


/***********************************************************************/
u_int U2F_IMEM_Write(int handle, u_int osize, u_int offset, u_int data[])
/***********************************************************************/
{
  u_int ret;
  
  U2F_DEBUG(15 ,(" U2F_IMEM_Write: called\n"));
  U2F_DEBUG(10 ,(" U2F_IMEM_Write: osize=%d\n", osize));
  ret = U2F_Memory_Write(handle, &(devices[handle].imem), osize, offset, data);
  U2F_DEBUG(15 ,(" U2F_IMEM_Write: done\n"));
  return ret;
}


/************************************************************************/
u_int U2F_PMEM_Write (int handle, u_int osize, u_int offset, u_int data[])
/************************************************************************/
{
  u_int ret;

  U2F_DEBUG(15 ,(" U2F_PMEM_Write: called\n"));
  U2F_DEBUG(10 ,(" U2F_PMEM_Write: osize=%d\n", osize));
  if (osize  > B_PMEM) 
  {    
    printf("\nNumber of words cannot exceed 256 words\n"); 
    U2F_DEBUG(5 , (" U2F_PMEM_Write: Parameter isize is out of range\n"));
    return(RCC_ERROR_RETURN(0, U2F_RANGE));
  }  
  ret = U2F_Memory_Write(handle, &(devices[handle].pmem), osize, offset, data);
  U2F_DEBUG(15 ,(" U2F_PMEM_Write: done\n"));
  return ret;
}

/************************************************************************/
u_int U2F_RMEM_Write (int handle, u_int osize, u_int offset, u_int data[])
/************************************************************************/
{
  u_int ret;
  
  U2F_DEBUG(15 ,(" U2F_RMEM_Write: called\n"));
  U2F_DEBUG(10 ,(" U2F_IMEM_Write: osize=%d\n", osize));
  ret = U2F_Memory_Write(handle, &(devices[handle].rmem), osize, offset, data);
  U2F_DEBUG(15 ,(" U2F_RMEM_Write: done\n"));
  return ret;
}

/************************************************************************/
u_int U2F_ACL_Write (int handle, u_int osize, u_int offset, u_int data[])
/************************************************************************/
{
  u_int ret;
  
  U2F_DEBUG(15 ,(" U2F_ACL_Write: called\n"));
  U2F_DEBUG(10 ,(" U2F_IMEM_Write: osize=%d\n", osize));
  ret = U2F_Memory_Write(handle, &(devices[handle].acl), osize, offset, data);
  U2F_DEBUG(15 ,(" U2F_ACL_Write: done\n"));
  return ret;
}

/*********************************************************************/
u_int U2F_DM_Write (int handle, u_int which, u_int osize, u_int offset, 
		    u_int data[])
/*********************************************************************/
{
  static u_int low[2048], high[2048];
  u_int ret, loop;
  
  u2f_bank_t* lbank = 0;
  u2f_bank_t* hbank = 0;
  U2F_DEBUG(15 ,(" U2F_DM_Write: called\n"));
 
  switch (which) 
  {
    case 1:
      lbank = &(devices[handle].dml1);
      hbank = &(devices[handle].dmh1);
      break;
    case 2:
      lbank = &(devices[handle].dml2);
      hbank = &(devices[handle].dmh2);
      break;
    default:
      U2F_DEBUG(5, ("Unknown bank #:%d\n", which));
      return(RCC_ERROR_RETURN(0, U2F_RANGE));
  }

  U2F_DEBUG(20, ("Writing to DM%d of size %d starting at %d\n", 
		 which, osize, offset));
  for (loop = 0; loop < osize; loop++) 
  {
    low[loop]   = (data[4*loop+0] & 0x3ff) + ((data[4*loop+1] & 0x3ff)<<10);
    high[loop]  = (data[4*loop+2] & 0x3ff) + ((data[4*loop+3] & 0x3ff)<<10);
  }  
 
  ret = U2F_Memory_Write(handle, lbank, osize, offset, low);
  ret = U2F_Memory_Write(handle, hbank, osize, offset, high);
  
  U2F_DEBUG(15 ,(" U2F_DM_Write: done\n"));
  return ret;
}

  
/***********************************************************************/
u_int U2F_DM1_Write (int handle, u_int osize, u_int offset, u_int data[])
/***********************************************************************/
{
  int ret;
  
  U2F_DEBUG(15 ,(" U2F_DM1_Write: called\n"));
  ret = U2F_DM_Write(handle, 1, osize, offset, data);
  U2F_DEBUG(15 ,(" U2F_DM1_Write: done\n"));
  return ret;
}


/***********************************************************************/
u_int U2F_DM2_Write (int handle, u_int osize, u_int offset, u_int data[])
/***********************************************************************/
{
  int ret;

  U2F_DEBUG(15 ,(" U2F_DM2_Write: called\n"));
  ret = U2F_DM_Write(handle, 2, osize, offset, data);
  U2F_DEBUG(15 ,(" U2F_DM2_Write: done\n"));
  return ret;
}

/****************************************************/
u_int U2F_Reg_Write(int handle, u_int reg, u_int data)
/****************************************************/
{
  int ret;

  U2F_DEBUG(15 ,(" U2F_Reg_Write: called\n"));
  U2F_DEBUG(20 , (" U2F_Reg_Write: reg = %d  data = %d\n", reg, data));
  
  if ((reg != O_TRCFG1) && 
      (reg != O_ACTFEC) && 
      (reg != O_FMIREG) && 
      (reg != O_FMOREG) && 
      (reg != O_TRCFG2) && 
      (reg != O_RESREG) && 
      (reg != O_ERRREG) && 
      (reg != O_INTMOD) && 
      (reg != O_INTREG)) 
  {    
    U2F_DEBUG(5,(" U2F_Reg_Write: The parameter reg is out of range "
		 "(reg=%d)\n", reg));
    return(RCC_ERROR_RETURN(0, U2F_RANGE)); 
  } 
  ret = U2F_Memory_Write(handle, &(devices[handle].registers), 1, reg, &data);
  
  U2F_DEBUG(15 ,(" U2F_Reg_Write: done\n"));
  return(U2F_SUCCESS);
}


/********************************************/
u_int U2F_TRCFG1_Write(int handle, u_int data)
/********************************************/
{
  int ret;

  U2F_DEBUG(15 ,(" U2F_TRCFG1_Write: called\n"));
  ret = U2F_Reg_Write(handle, O_TRCFG1, data);
  U2F_DEBUG(15 ,(" U2F_TRCFG1_Write: done\n"));
  return ret;
}


/********************************************/
u_int U2F_ACTFEC_Write(int handle, u_int data)
/********************************************/
{
  int ret;

  U2F_DEBUG(15 ,(" U2F_ACTFEC_Write: called\n"));
  ret = U2F_Reg_Write(handle, O_ACTFEC, data);
  U2F_DEBUG(15 ,(" U2F_ACTFEC_Write: done\n"));
  return ret;
}


/******************************************************/
u_int U2F_TRCFG1_encode(u_int *data, trcfg_t parameters)
/******************************************************/
{
  U2F_DEBUG(15 , (" U2F_TRCFG1_encode: called\n"));
  if (parameters.tw > 0x3fff) 
  {    
    U2F_DEBUG(5 , (" U2F_TRCFG1_encode: Parameter tw is out of range\n"));
    return(RCC_ERROR_RETURN(0, U2F_RANGE));
  }       
  
  if (parameters.bmd > 2) 
  {    
    U2F_DEBUG(5 , (" U2F_TRCFG1_encode: Parameter bmd is out of range\n"));
    return(RCC_ERROR_RETURN(0, U2F_RANGE));
  }     

  if ((parameters.mode > 3) || (parameters.mode == 1)) 
  {    
    U2F_DEBUG(5 , (" U2F_TRCFG1_encode: Parameter mode is out of range\n"));
    return(RCC_ERROR_RETURN(0, U2F_RANGE));
  }  

  *data = (parameters.tw + 
	   (parameters.bmd << 14) + 
	   (parameters.mode << 15) + 0x40000);
  U2F_DEBUG(20 , (" U2F_TRCFG1_encode: The encoded data word is %d\n", *data));
  U2F_DEBUG(15 , (" U2F_TRCFG1_encode: done\n"));
  return(U2F_SUCCESS);
}

  
/**************************************************/
u_int U2F_TRCFG1_Set(int handle, trcfg_t parameters)
/**************************************************/
{
  u_int data, ret;
  
  U2F_DEBUG(15 , (" U2F_TRCFG1_Set: called\n"));
  ret = U2F_TRCFG1_encode(&data, parameters);
  if (ret) 
    return ret;

  ret = U2F_TRCFG1_Write(handle, data);
  U2F_DEBUG(15 , (" U2F_TRCFG1_Set: done\n"));
  return ret;
}

  

  
/*******************************************************/
u_int U2F_File_Read(char *name, u_int size, u_int data[])
/*******************************************************/
{
  u_int loop;
  FILE *inf;
  
  U2F_DEBUG(15 , (" U2F_File_Read: called\n"));
  /* open the input file */
  inf = fopen(name, "r");
  if (inf == 0) 
  {
    U2F_DEBUG(20 ,(" U2F_File_Read:  Can't open input file\n"));
    return(RCC_ERROR_RETURN(0, U2F_FILE));
  }

  /* read  the file */
  for (loop = 0; loop < size; loop++) 
  {
    fscanf(inf,"%x",&data[loop]);
    if (feof(inf)) 
    {  
      U2F_DEBUG(20 ,(" U2F_File_Read: The file is too short\n"));
      return(RCC_ERROR_RETURN(0, U2F_NODATA));
    }
  }
  fclose(inf);
  U2F_DEBUG(15 , (" U2F_File_Read: done\n"));
  return(U2F_SUCCESS);
}

  
/********************************************************/
u_int U2F_File_Write(char *name, u_int size, u_int data[])
/********************************************************/
{
  u_int loop;
  FILE *outf;

  U2F_DEBUG(15 , (" U2F_File_Write: called\n"));
  outf = fopen(name, "a+");
  if (outf==0) 
  {
    U2F_DEBUG(5 ,(" U2F_File_Write:  Can't open output file\n"));
    return(RCC_ERROR_RETURN(0, U2F_FILE));
  }
  
  /* write  the file */
  for (loop = 0; loop < size; loop++)
    fprintf(outf,"%x\n",data[loop]);

  fclose(outf);
  U2F_DEBUG(15 , (" U2F_File_Write: done\n"));
  return(U2F_SUCCESS);  
}

/***********************************************************/
static u_int SW_Exec_Command(int handle, u_int command, u_int data) 
{
  static u_int chip[MAX_U2F_DEVICES]      = { 0 };
  static u_int channel[MAX_U2F_DEVICES]   = { 0 };
  static u_int mem[MAX_U2F_DEVICES]       = { 0 };
  static u_int ntimebins[MAX_U2F_DEVICES] = { 0 };
  static int   first                      = 1;
  u_int        i;
  if (first) {
    first = 0;
    for (i = 0; i < MAX_U2F_DEVICES; i++) {
      ntimebins[i] = 256;
      mem[i]       = 1;
      channel[i]   = 0;
      chip[i]      = 0;
    }
  }
  
  U2F_DEBUG(20 , ("SW_Exec_Command: Emulation: command = %d\n", command));
  U2F_DEBUG(20 , ("SW_Exec_Command: Emulation: data    = %d\n", data));
  U2F_DEBUG(20 , ("SW_Exec_Command: command = %d\n", command));
  switch(command) {
  case C_RS_STATUS:
    U2F_DEBUG(1, ("Status reset"));
    devices[handle].registers.data[O_ERRST] = 0x0;
    break;
  case C_RS_TRCFG:
    U2F_DEBUG(1, ("Trigger config reset"));
    devices[handle].registers.data[O_TRCFG1] = 0x0;
    break;  
  case C_RS_TRCNT:
    U2F_DEBUG(1, ("Trigger counter reset"));
    devices[handle].registers.data[O_TRCNT] = 0x0;
    break;
  case C_EXEC:
    U2F_DEBUG(1, ("Execute instruction memory at offset %d\n", data));
    for (i = data; i < devices[handle].imem.size; i++) {
      U2F_DEBUG(1, ("%3d  0x%08x\n", i, devices[handle].imem.data[i]));
      if (devices[handle].imem.data[i] == 0x390000) break;
      if (devices[handle].imem.data[i] & (1 << 22)) {
	if (devices[handle].imem.data[i] & (1 << 20)) 
	  devices[handle].registers.data[O_IRDAT] = 
	    devices[handle].imem.data[i];
	else 
	  devices[handle].registers.data[O_IRADD] = 
	    devices[handle].imem.data[i];
	if (!(devices[handle].imem.data[i] & (1 << 17)) && 
	    (devices[handle].imem.data[i]  & (1 << 18)) && 
	    (devices[handle].imem.data[i]  &  0xf) == 0xa) {
	  /** Update aquisition window */
	  i++;
	  ntimebins[handle] = ((devices[handle].imem.data[i] & 0x3ff) - 
			       ((devices[handle].imem.data[i] >> 10) & 0x3ff));
	  U2F_DEBUG(1, ("%3d  0x%08x\n", i, devices[handle].imem.data[i]));
	  U2F_DEBUG(1, ("Number of time bins %d\n", ntimebins[handle]));
	}
      }
    }
    return(ALTRO_SUCCESS);
  case C_ABORT:
    U2F_DEBUG(1, ("Abort current instructions\n"));
    break;
  case C_FECRST:
    U2F_DEBUG(1, ("Reset front end cards\n"));
    break;
  case C_SWTRG:
    U2F_DEBUG(1, ("Software trigger\n"));
    devices[handle].registers.data[O_EVWORD] =  0x1B;
    devices[handle].registers.data[O_TRCNT]  += 0x1001;
    devices[handle].registers.data[O_LWADD]  += 0x0;
    chip[handle]    = 0;
    channel[handle] = 0;
    mem[handle]     = 0;
    write_next_data(handle, 1, &chip[handle], &channel[handle], 
		    ntimebins[handle]);
    write_next_data(handle, 2, &chip[handle], &channel[handle], 
		    ntimebins[handle]);
    break;
  case C_RS_DMEM1:
    U2F_DEBUG(1, ("DMEM 1 reset\n"));
    devices[handle].registers.data[O_EVWORD] ^= 0x2;
    write_next_data(handle, 1, &chip[handle], &channel[handle], 
		    ntimebins[handle]);
    if ((devices[handle].registers.data[O_EVWORD] & 0x3) == 0) 
      devices[handle].registers.data[O_EVWORD] = 0x4;
    U2F_DEBUG(1, ("EVWORD 0x%x\n", devices[handle].registers.data[O_EVWORD]));
    break;
  case C_RS_DMEM2:
    U2F_DEBUG(1, ("DMEM 2 reset\n"));
    devices[handle].registers.data[O_EVWORD] ^= 0x1;
    write_next_data(handle, 2, &chip[handle], &channel[handle], 
		    ntimebins[handle]);
    if ((devices[handle].registers.data[O_EVWORD] & 0x3) == 0) 
      devices[handle].registers.data[O_EVWORD] = 0x4;
    U2F_DEBUG(1, ("EVWORD 0x%x\n", devices[handle].registers.data[O_EVWORD]));
    break;      
  case C_TRG_CLR:
    U2F_DEBUG(1, ("Event word reset"));
    devices[handle].registers.data[O_EVWORD] = 0x0;
    break;
  case C_WRFM:
    U2F_DEBUG(1, ("Write FM"));
    break;
  case C_RDFM:
    U2F_DEBUG(1, ("Read FM"));
    break;
  case C_RDABORT:
    U2F_DEBUG(1, ("Abort large block read out"));
    break;
  }
  return(U2F_SUCCESS);
}


/***********************************************************/
u_int U2F_Exec_Command(int handle, u_int command, u_int data)
/***********************************************************/
{
  altro_bulk_out_t out;
  u_short *msg;
  u_int address, ret;
  
  U2F_DEBUG(15 , (" U2F_Exec_Command: called\n"));
  U2F_DEBUG(20 , (" U2F_Exec_Command: command = 0x%08x\n",command));
  U2F_DEBUG(20 , (" U2F_Exec_Command: data    = 0x%08x\n",data));
  
  U2FISOPEN(handle);

  if ((command != C_RS_STATUS) && 
      (command != C_RS_TRCFG)  && 
      (command != C_RS_TRCNT)  && 
      (command != C_EXEC)      && 
      (command != C_ABORT)     && 
      (command != C_FECRST)    && 
      (command != C_SWTRG)     && 
      (command != C_TRG_CLR)   && 
      (command != C_RS_DMEM1)  && 
      (command != C_RS_DMEM2)  && 
      (command != C_WRFM)      && 
      (command != C_RDFM)      &&
      (command != C_RDABORT))
  {    
    U2F_DEBUG(5 , ("U2F_Exec_Command:  The parameter <command> is "
		   "out of range\n"));
    return(RCC_ERROR_RETURN(0, U2F_RANGE));
  } 
  
  if ((command == C_EXEC) && (data > 0xff)) 
  {    
    U2F_DEBUG(5,("U2F_Exec_Command:  The parameter <data> is out of range\n"));
    return(RCC_ERROR_RETURN(0, U2F_RANGE));
  } 

  if (devices[handle].mode == SW) return SW_Exec_Command(handle,command, data);

  address = command;
  if (command == C_EXEC) address += data;
  if (command == C_SCCOMMAND) {
    address += data & 0xffff;
    msg    = (u_short*)&out.data[0];
    msg[0] = 0x20 + I_WRITE;
    msg[1] = address;
    msg[2] = (data >> 16) & 0xffff;
    msg[3] = 0;
    out.nbytes = 8;
    U2F_DEBUG(20 , (" U2F_Exec_Command: msg[0] = 0x%02x\n", msg[0]));
    U2F_DEBUG(20 , (" U2F_Exec_Command: msg[1] = 0x%02x\n", msg[1]));
    U2F_DEBUG(20 , (" U2F_Exec_Command: msg[2] = 0x%02x\n", msg[2]));
    U2F_DEBUG(20 , (" U2F_Exec_Command: msg[3] = 0x%02x\n", msg[3]));  
  }
  else {
    /* Construct a message */
    msg    = (u_short *)&out.data[0];
    msg[0] = I_WRITE; 
    msg[1] = address;
    out.nbytes = 4;
  }
  
  /* Send the message  */
  U2F_DEBUG(20 , ("U2F_Exec_Command: Calling ALTRO_Send\n"));
  ret = ALTRO_Send(handle, &out);
  if (ret) 
  {      
    U2F_DEBUG(5 , ("U2F_Exec_Command: Failed to send\n"));
    return(RCC_ERROR_RETURN(ret, U2F_ALTRO_FAIL)); 
  }      
  U2F_DEBUG(15 , (" U2F_Exec_Command: done\n"));
  return(U2F_SUCCESS);  
}


/**********************************/
u_int U2F_Exec_RS_STATUS(int handle)
/**********************************/
{
  int ret;
  
  U2F_DEBUG(15 , (" U2F_Exec_RS_STATUS: called\n"));
  ret = U2F_Exec_Command(handle, C_RS_STATUS, 0);
  U2F_DEBUG(15 , (" U2F_Exec_RS_STATUS: done\n"));
  return ret;
} 


/*********************************/
u_int U2F_Exec_RS_TRCFG(int handle)
/*********************************/
{
  int ret;
  
  U2F_DEBUG(15 , (" U2F_Exec_RS_TRCFG: called\n"));
  ret = U2F_Exec_Command(handle, C_RS_TRCFG, 0);
  U2F_DEBUG(15 , (" U2F_Exec_RS_TRCFG: done\n"));
  return ret;
} 


/*********************************/
u_int U2F_Exec_RS_TRCNT(int handle)
/*********************************/
{
  int ret;
  
  U2F_DEBUG(15 , (" U2F_Exec_RS_TRCNT: called\n"));
  ret = U2F_Exec_Command(handle, C_RS_TRCNT, 0);
  U2F_DEBUG(15 , (" U2F_Exec_RS_TRCNT: done\n"));
  return ret;
}  


/*****************************************/
u_int U2F_Exec_EXEC(int handle, u_int data)
/*****************************************/
{
  int ret;
  
  U2F_DEBUG(15 , (" U2F_Exec_EXEC: called\n"));
  ret = U2F_Exec_Command(handle, C_EXEC, data);
  U2F_DEBUG(15 , (" U2F_Exec_EXEC: done\n"));
  return ret;
} 


/******************************/
u_int U2F_Exec_ABORT(int handle)
/******************************/
{
  int ret;
  
  U2F_DEBUG(15 , (" U2F_Exec_ABORT: called\n"));
  ret = U2F_Exec_Command(handle, C_ABORT, 0);
  U2F_DEBUG(15 , (" U2F_Exec_ABORT: done\n"));
  return ret;
} 


/*******************************/
u_int U2F_Exec_FECRST(int handle)
/*******************************/
{
  int ret;
  
  U2F_DEBUG(15 , (" U2F_Exec_FECRST: called\n"));
  ret = U2F_Exec_Command(handle, C_FECRST, 0);
  U2F_DEBUG(15 , (" U2F_Exec_FECRST: done\n"));
  return ret;
}

/******************************/
u_int U2F_Exec_SWTRG(int handle)
/******************************/
{
  int ret;
  
  U2F_DEBUG(15 , (" U2F_Exec_SWTRG: called\n"));
  ret = U2F_Exec_Command(handle, C_SWTRG, 0);
  U2F_DEBUG(15 , (" U2F_Exec_SWTRG: done\n"));
  return ret;
} 


/********************************/
u_int U2F_Exec_TRG_CLR(int handle)
/********************************/
{
  int ret;
  
  U2F_DEBUG(15 , (" U2F_Exec_TRG_CLR: called\n"));
  ret = U2F_Exec_Command(handle, C_TRG_CLR, 0);
  U2F_DEBUG(15 , (" U2F_Exec_TRG_CLR: done\n"));
  return ret;
} 


/*********************************/
u_int U2F_Exec_RS_DMEM1(int handle)
/*********************************/
{
  int ret;
  
  U2F_DEBUG(15 , (" U2F_Exec_RS_DMEM1: called\n"));
  ret = U2F_Exec_Command(handle, C_RS_DMEM1, 0);
  U2F_DEBUG(15 , (" U2F_Exec_RS_DMEM1: done\n"));
  return ret;
}  


/*********************************/
u_int U2F_Exec_RS_DMEM2(int handle)
/*********************************/
{
  int ret;
  
  U2F_DEBUG(15 , (" U2F_Exec_RS_DMEM2: called\n"));
  ret = U2F_Exec_Command(handle, C_RS_DMEM2, 0);
  U2F_DEBUG(15 , (" U2F_Exec_RS_DMEM2: done\n"));
  return ret;
}
     

/*************************/
u_int U2F_Reset(int handle)
/*************************/
{
  altro_bulk_out_t out;
  u_short *msg;
  u_int ret;
  
  U2F_DEBUG(15 , (" U2F_Reset: called\n"));
  U2FISOPEN(handle);

  if (devices[handle].mode == SW)
  {
    U2F_DEBUG(20 , ("U2F_Reset: Emulation: Nothiong to be done\n"));
    return(0);
  }

  /* Construct a message */
  msg = (u_short *)&out.data[0];
  msg[0] = I_GRESET; 
  out.nbytes = 2;

  /* Tell the USB2FEC that we want to reset the card */
  ret = ALTRO_Send(handle, &out);
  if (ret) 
  {      
    U2F_DEBUG(5 ,(" U2F_Reset: Error from ALTRO_Send\n"));
    return(RCC_ERROR_RETURN(ret, U2F_ALTRO_FAIL)); 
  } 
  U2F_DEBUG(15 , (" U2F_Reset: done\n"));
  return(0);
}



/*********************************************************************/
u_int U2F_ReadOut(int handle, u_int bsize, u_int *osize, u_char data[], 
		  u_int mode)
/*********************************************************************/
{
  u_int loop, ret = 0, cont, index;
  altro_bulk_in_t in;
  err_str pid, code;
  
  U2F_DEBUG(15 , (" U2F_ReadOut: called\n"));
  U2FISOPEN(handle);

  if (mode & M_FIRST)
  {
    U2F_DEBUG(20 ,(" U2F_ReadOut: Enabling read-out mode\n"));
    ret = U2F_Reg_Write(handle, O_TRCFG2, 3);
    if (ret)
    {
      ALTRO_err_get(ret, pid, code);
      U2F_DEBUG(5 ,(" U2F_ReadOut: Error (%d) from U2F_Reg_Write: %s\n", 
		    ret, code));
      return(RCC_ERROR_RETURN(0, ret)); 
    }
  } 

  if (mode & M_TRIGGER)
  {
    U2F_DEBUG(10 ,(" U2F_ReadOut: Sending S/W trigger\n"));
    ret = U2F_Exec_Command(handle, C_SWTRG, 0);
    if (ret)
    {
      ALTRO_err_get(ret, pid, code);
      U2F_DEBUG(5 ,(" U2F_ReadOut: Error (%d) from U2F_Exec_Command: %s\n", 
		    ret, code));
      return(RCC_ERROR_RETURN(0, ret)); 
    }
  }

  U2F_DEBUG(10 ,(" U2F_ReadOut: Reading data\n"));
  cont = 1;
  index = 0;
  while(cont)
  {
    if ((index + 1024) > bsize)
    {
      U2F_DEBUG(5 ,(" U2F_ReadOut: The next USB burst would overflow "
		    "the data array\n"));
      
      if (mode & M_LAST)
      {
	U2F_DEBUG(20 ,(" U2F_ReadOut: Aborting read-out mode\n"));
	ret = U2F_Exec_Command(handle, C_RDABORT, 0);
	if (ret)
	{
	  ALTRO_err_get(ret, pid, code);
	  U2F_DEBUG(5 ,(" U2F_ReadOut: Error (%d) from U2F_Exec_Command: %s\n",
			ret, code));
	  return(RCC_ERROR_RETURN(0, ret)); 
	}

	U2F_DEBUG(20 ,(" U2F_ReadOut: Disabling read-out mode\n"));
	ret = U2F_Reg_Write(handle, O_TRCFG2, 2);
	if (ret)
	{
	  ALTRO_err_get(ret, pid, code);
	  U2F_DEBUG(5 ,(" U2F_ReadOut: Error (%d) from U2F_Reg_Write: %s\n", 
			ret, code));
	  return(RCC_ERROR_RETURN(0, ret)); 
	}
      }

      *osize = index;
      return(RCC_ERROR_RETURN(ret, U2F_LOW_MEM)); 
    }

    ret = ALTRO_Get(handle, &in);
    if (ret) 
    {
      ALTRO_err_get(ret, pid, code);
      U2F_DEBUG(5 ,(" U2F_ReadOut: Error (%d) from ALTRO_Get: %s\n", 
		    ret, code));
      return(RCC_ERROR_RETURN(ret, U2F_ALTRO_FAIL)); 
    } 

    if (in.nbytes)
    { 
      U2F_DEBUG(20 ,(" U2F_ReadOut: %d bytes received\n", in.nbytes));
      /* copy the data from the altro_bulk_in_t structure to the data array */
      for(loop = 0; loop < in.nbytes; loop++) 
      { 
	data[index] = in.data[loop];
        if (index < 16)
          U2F_DEBUG(30 ,(" U2F_ReadOut: byte %d = 0x%02x\n", 
			 loop, in.data[loop]));
	index++;
      }
    }
    else
    {
      cont = 0;  
      U2F_DEBUG(20 ,(" U2F_ReadOut: Timeout, no data received. "
		     "Terminating function\n"));
    }
  }
  
  if (mode & M_LAST)
  {
    U2F_DEBUG(20 ,(" U2F_ReadOut: Aborting read-out mode\n"));
    ret = U2F_Exec_Command(handle, C_RDABORT, 0);
    if (ret)
    {
      ALTRO_err_get(ret, pid, code);
      U2F_DEBUG(5 ,(" U2F_ReadOut: Error (%d) from U2F_Exec_Command: %s\n", 
		    ret, code));
      return(RCC_ERROR_RETURN(0, ret)); 
    }
	    
    U2F_DEBUG(20 ,(" U2F_ReadOut: Disabling read-out mode\n"));
    ret = U2F_Reg_Write(handle, O_TRCFG2, 0);
    if (ret)
    {
      ALTRO_err_get(ret, pid, code);
      U2F_DEBUG(5 ,(" U2F_ReadOut: Error (%d) from U2F_Reg_Write: %s\n", 
		    ret, code));
      return(RCC_ERROR_RETURN(0, ret)); 
    }
  }

  *osize = index;
  U2F_DEBUG(15 , (" U2F_ReadOut: done\n"));
  return(0);
}

/****************************/
u_int make_dummy_data(u_int i) 
/****************************/
{
  return i; // 48 + (u_int)(5 * (float)rand() / RAND_MAX);
}

/************************************************************************/
void write_next_data(int handle, u_int which, u_int *chip, u_int *channel, 
		     u_int len) 
/************************************************************************/
{
  u_int mem = 0;
  for (;*chip < devices[handle].acl.size; (*chip)++) {
    for (;*channel < 16; (*channel)++) {
      if (devices[handle].acl.data[*chip] & (1 << *channel)) {
	mem = which;
	break;
      }
    }
    if (mem) break;
  }
  if (!mem) return;
  write_dummy_data(handle, mem, *chip, *channel, len);
  (*channel)++;
  if ((*channel) >= 16) {
    (*channel) = 0;
    (*chip)++;
  }
}

	

/********************************************************************/
void write_dummy_data(int handle, u_int which, u_int chip, u_int chan, 
		      u_int len) 
/********************************************************************/
{
  typedef unsigned long long w40_t;
  static u_int data[1024+4+4];
  w40_t trailer = ((((w40_t)0x2aaa)            << 26) + 
		   (((w40_t)((len+2) & 0x3ff)) << 16) + 
		   (((w40_t)0xa)               << 12) + 
		   (((w40_t)(chip & 0xff))     << 4) + 
		   (((w40_t)(chan & 0xf))));
  u_int i, j = 0, k; /* , n = (len+2) / 4, m = (len+2) % 4; */
  // int ii;
  
  U2F_DEBUG(1, ("Preparing psuedo data to DMEM%d (chip %d, chan %d) "
		"of size %d\n", which, chip, chan, len));
  for (i = 0; i < len; i++) 
  {
    data[i] = make_dummy_data(i);
  }
  data[i] = len-1; i++;
  data[i] = len+2; i++;
  if (!(i % 4 == 0)) {
    for (j = 0; j < (i % 4); j++) {
      data[i] = 0x2aa;
      i++;
    }
  }
  data[i] =  (int)((trailer >>  0) & 0x3ff);  i++;
  data[i] =  (int)((trailer >> 10) & 0x3ff);  i++;
  data[i] =  (int)((trailer >> 20) & 0x3ff);  i++;
  data[i] =  (int)((trailer >> 30) & 0x3ff);  i++;
  
  U2F_DEBUG(1, ("Writing psuedo data to DMEM%d of size %d\n", which, j));
#if 0
  for (i = 0; i < j/4; i++) {
    printf("%4d 0x%03X 0x%03X 0x%03X 0x%03X\n", i,  
	   data[4*i+0], data[4*i+1],data[4*i+2],data[4*i+3]);
  }
#endif
  k = (which == 2 ? ((i/4) << 8) : i/4);
  devices[handle].registers.data[O_LWADD]  |= k;
  k = (which == 2 ? 0x1 : 0x2);
  devices[handle].registers.data[O_EVWORD] |= k;
  U2F_DM_Write(handle, which, i / 4, 0, data);
}
/***************************************************************/
unsigned int U2F_err_get(err_pack err, err_str pid, err_str code)
/***************************************************************/
{ 
  strcpy(pid, P_ID_ALTRO_STR);

  switch (RCC_ERROR_MINOR(err))
  {  
    case U2F_SUCCESS:    strcpy(code, U2F_SUCCESS_STR);     break;
    case U2F_FILE:       strcpy(code, U2F_FILE_STR);        break;
    case U2F_ALTRO_FAIL: strcpy(code, U2F_ALTRO_FAIL_STR);  break;
    case U2F_RANGE:      strcpy(code, U2F_RANGE_STR);       break;
    case U2F_ODD_BYTES:  strcpy(code, U2F_ODD_BYTES_STR);   break;
    case U2F_NODATA:     strcpy(code, U2F_NODATA_STR);      break;
    case U2F_INC_BYTES:  strcpy(code, U2F_INC_BYTES_STR);   break;
    case U2F_LOW_MEM:    strcpy(code, U2F_LOW_MEM_STR);     break;
    default:             strcpy(code, U2F_NO_CODE_STR);     
      return(RCC_ERROR_RETURN(0, U2F_NO_CODE)); break;
  }
  return(RCC_ERROR_RETURN(0, U2F_SUCCESS));
}
