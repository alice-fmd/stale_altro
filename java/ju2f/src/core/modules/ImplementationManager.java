/**
 * 
 */
package core.modules;

import gui.GuiManager;
import core.jni.JavaCLayer;
import core.modules.fec.FecManager;
import core.modules.u2f.U2fManager;

/**
 * @author okhalid
 *
 */
public class ImplementationManager 
{

public FecManager fecManager;
public U2fManager u2fManager;	
public GuiManager guiManager;

public ImplementationManager(GuiManager gui)
{
	initGuiManagerReference(gui);
	run();	
}

/*
 * This method has to be called before run() call in the rootController.
 */
public void initGuiManagerReference(GuiManager gui)
{
	guiManager = gui;
}

public void run()
{
	initU2F();
	initFEC();
	initJniLayer();
}

private void initU2F()
{
	u2fManager = new U2fManager();
	u2fManager.run();
}

private void initFEC()
{
	fecManager = new FecManager();
	fecManager.run();
}

private void initJniLayer()
{
	//This should read the BoardControl on the fec and display the output in the console
    JavaCLayer bc = new JavaCLayer();
    bc.run();
}

}
