/************************************************************************/
/*									*/
/*  This is the FEC library 	   				        */
/*									*/
/*  09. Jun. 05  L. Musa & H. Santos					*/
/*									*/
/************************************************************************/

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/file.h>
#include <fcntl.h>
#include <tools/get_input.h>
#include <altro/altro.h>
#include <tools/rcc_time_stamp.h>
#include "u2f.h"
#include "fec.h"



int main(int argc, char *argv[])
{
u_int ret, rcode, rdata, ccode,cdata;
u_int *reg_data;
u_int system_init=1, testmode=0, ncycles = 10, write =1;
u_int channel_address, fec_address = 9;
u_int loop, tick=0;
static char fname[200] = "/home/rcu/NEW_U2F/altro_usb/log_files/backplane_test.log";
FILE *file;

/***********************************************************************/
/*		Program Options					       */
/***********************************************************************/

  for (loop = 1; loop < argc; loop++) 
  {
    if (argv[loop][0] == '-') 
    {
      switch (argv[loop][1]) 
      {
        case 'h': 
	  printf("Usage: %s [OPTIONS]\n\n", argv[0]); 
	  printf("Options:\n"); 
	  printf("  -h\t\tThis help:\n"); 
	  printf("  -d LEVEL\trun in DEBUG Mode [%d]\n", testmode); 
	  printf("  -a FEC Address [%d]\n", fec_address);
	  printf("  -n CYCLES\tNumber of WRITE/READ Cycles [%d]\n", ncycles);
	  printf("  -t MODE\t0 for READ Cycle, 1 for WRITE Cycle, 2 for WRITE & READ & COMPARE [%d]\n\n",write);
	  return 0;
	break;
	case 'd': testmode = strtol(argv[loop+1], NULL, 0); loop++; break;
	case 'a': fec_address = strtol(argv[loop+1], NULL, 0); loop++; break;
	case 'n': ncycles    = strtol(argv[loop+1], NULL, 0); loop++; break;
	case 't': write  = strtol(argv[loop+1], NULL, 0); loop++;
	break;
      default:
	fprintf(stderr, "Unknown option %s, try %s -h", argv[loop], argv[0]);
	return 1;
      }
    }
  }



/***********************************************************
    System Initialization 
***********************************************************/
if(system_init)   
{
/* Open U2F_LIB */
ret = U2F_Open(HW, DEFAULT_NODE);
if (ret)
  rcc_error_print(stdout, ret);

/* Open the rcc_time_stamp library */
ret = ts_open(1, TS_DUMMY);
if (ret)
  rcc_error_print(stdout, ret);

/* Switch on FEC with Address = 0  */
rcode = O_ACTFEC;
rdata = 0x2ff;
ret = U2F_Reg_Write(rcode, rdata);
if (ret)
  rcc_error_print(stdout, ret);
    
/* delay 1000000 us */
ts_delay(1000000);

/* Reset FEC */
ccode = C_FECRST;
cdata = 0;
ret = U2F_Exec_Command(ccode, cdata);
ccode = C_FECRST;
cdata = 0;
ret = U2F_Exec_Command(ccode, cdata);
if (ret)
  rcc_error_print(stdout, ret);

    
/* delay 100 us */
ts_delay(100);
}


/***********************************************************/
/*    WRITE & READ  REGISTER				   */	
/***********************************************************/

channel_address = fec_address << 7;
if(ncycles)
{
  for(loop =0; loop < ncycles; loop++)
  {
    if(write == 1)
    {
      ret = Write_ALTRO_Reg(0, channel_address, 0, 0x00000);
      if(ret)
        rcc_error_print(stdout, ret);
    }
    else if (write == 2)
    {
      ret = Write_ALTRO_Reg(0, channel_address, 0, 0xabcd);
      if(ret)
        rcc_error_print(stdout, ret);

      ret = Read_ALTRO_Reg(0, 0x0, 0x11, reg_data);
      if(ret)
        rcc_error_print(stdout, ret);
      
      if(*reg_data != 0xabcd)
      {
        file = fopen(fname,"w");
        fprintf(file, "Error in cycle Nr. %d\n", loop);
        fprintf(file, "Written: 0xabcd;\t\tRead: %x\n", *reg_data);
      }	
    }
    else
    {
       ret = Read_ALTRO_Reg(0, 0x0, 0x11, reg_data);
       if(ret)
         rcc_error_print(stdout, ret);
    }

  if(testmode)
    printf("\nCycle Nr. %d\n", loop);   
  }
}
else
{
   while(!ncycles)
  {
    tick++;
    if(write)
    {
      ret = Write_ALTRO_Reg(0, 0, 0, 0xabcd);
      if(ret)
        rcc_error_print(stdout, ret);
    }
    else
    {
       ret = Read_ALTRO_Reg(0, 0x0, 0x11, reg_data);
       if(ret)
         rcc_error_print(stdout, ret);
    }
    if(tick == 10000)
    {
      printf("\nLoop of 10000 Cycles completed\n");
      tick = 0;
    }       
  }
}
printf("\n");
ret = U2F_Close();
ret = ts_close(TS_DUMMY);  
exit(0);
}


