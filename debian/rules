#!/usr/bin/make -f
# -*- makefile -*-
# Sample debian/rules that uses debhelper. 
# This file was originally written by Joey Hess and Craig Small.
# As a special exception, when this file is copied by dh-make into a
# dh-make output file, you may use that output file without restriction.
# This special exception was added by Craig Small in version 0.37 of dh-make.
#
# This version is for a hypothetical package that can build a kernel modules
# architecture-dependant package via make-kpkg, as well as an
# architecture-independent module source package, and other packages
# either dep/indep for things like common files or userspace components
# needed for the kernel modules.

# Uncomment this to turn on verbose mode. 
#export DH_VERBOSE=1


# These are used for cross-compiling and for saving the configure script
# from having to guess our platform (since we know it already)
DEB_HOST_GNU_TYPE   ?= $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
DEB_BUILD_GNU_TYPE  ?= $(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)


CFLAGS = -Wall -g

ifneq (,$(findstring noopt,$(DEB_BUILD_OPTIONS)))
	CFLAGS += -O0
else
	CFLAGS += -O2
endif

# some default definitions, important!
# 
# Name of the source package
psource:=altro-source

# The short upstream name, used for the module source directory
sname:=altro

### KERNEL SETUP
### Setup the stuff needed for making kernel module packages
### taken from /usr/share/kernel-package/sample.module.rules

# prefix of the target package name
PACKAGE	=  altro-modules
# modifieable for experiments or debugging m-a
MA_DIR 	?= /usr/share/modass
# load generic variable handling
-include $(MA_DIR)/include/generic.make
# load default rules, including kdist, kdist_image, ...
-include $(MA_DIR)/include/common-rules.make

# module assistant calculates all needed things for us and sets
# following variables:
# KSRC (kernel source directory), KVERS (kernel version string), KDREV
# (revision of the Debian kernel-image package), CC (the correct
# compiler), VERSION (the final package version string), PKGNAME (full
# package name with KVERS included), DEB_DESTDIR (path to store DEBs)

# The kdist_configure target is called by make-kpkg modules_config and
# by kdist* rules by dependency. It should configure the module so it is
# ready for compilation (mostly useful for calling configure).
# prep-deb-files from module-assistant creates the neccessary debian/ files 
kdist_config: prep-deb-files

# the kdist_clean target is called by make-kpkg modules_clean and from
# kdist* rules. It is responsible for cleaning up any changes that have
# been made by the other kdist_commands (except for the .deb files created)
kdist_clean: 
	# $(MAKE) $(MFLAGS) -f debian/rules clean
	rm -rf 	driver/altro_driver*.o		\
		driver/altro_driver*.ko		\
		driver/altro_driver*.mod	\
		driver/altro_driver*.mod.o	\
		driver/altro_driver*.mod.c	\
		driver/.altro_driver*		\
		driver/*~ 			\
		driver/.tmp_versions		\
		debian/control			\
		debian/control.backup		\
		debian/$(PACKAGE)-$(KVERS).postinst.modules.in.ex	\
		debian/tmp

#	rm -f driver/*.o driver/*.ko
#
### end  KERNEL SETUP

config.status: configure
	dh_testdir
	# Add here commands to configure the package.
	CFLAGS="$(CFLAGS) -Wl,-z,defs" \
	./configure --host=$(DEB_HOST_GNU_TYPE) 	\
		--build=$(DEB_BUILD_GNU_TYPE) 		\
		--prefix=/usr 				\
		--mandir=\$${prefix}/share/man 		\
		--infodir=\$${prefix}/share/info	\
		--sysconfdir=/etc			\
		--disable-java				\
		--disable-jni				\
		--disable-labview			\
		--disable-module			\
		--without-kernel-source


build-arch:  config.status build-arch-stamp
build-arch-stamp:
	dh_testdir

	# Add here command to compile/build the package.
	$(MAKE)

	touch build-arch-stamp

k = $(shell echo $(KVERS) | grep -q ^2.6 && echo k)

# the binary-modules rule is invoked by module-assistant while processing the
# kdist* targets. It is called by module-assistant or make-kpkg and *not*
# during a normal build
binary-modules:
	dh_testroot
	dh_clean -k
	dh_installdirs lib/modules/$(KVERS)/misc

	# Build the module
	$(MAKE) -C $(KSRC) V=1 SUBDIRS=$(PWD)/driver \
		MODVERDIR=$(PWD)/driver modules

	# Install the module
	mkdir -p debian/tmp/lib/modules/$(KVERS)/misc
	cp driver/altro_driver.$(k)o 	\
		debian/tmp/lib/modules/$(KVERS)/misc

	dh_installdocs
	dh_installchangelogs
	dh_compress
	dh_fixperms
	dh_installdeb
	dh_gencontrol -- -v$(VERSION)
	dh_md5sums
	dh_builddeb --destdir=$(DEB_DESTDIR)
	dh_clean -k

build-indep: config.status  build-indep-stamp
build-indep-stamp:
	dh_testdir

	# Add here command to compile/build the arch indep package.
	# It's ok not to do anything here, if you don't need to build
	#  anything for this package.
	#docbook-to-man debian/altro.sgml > altro.1

	touch build-indep-stamp

build: build-arch build-indep

clean:
	dh_testdir
	#dh_testroot
	rm -f build-arch-stamp build-indep-stamp 

	# Add here commands to clean up after the build process.
	-$(MAKE) distclean
ifneq "$(wildcard /usr/share/misc/config.sub)" ""
	cp -f /usr/share/misc/config.sub config.sub
endif
ifneq "$(wildcard /usr/share/misc/config.guess)" ""
	cp -f /usr/share/misc/config.guess config.guess
endif


	dh_clean

install: DH_OPTIONS=
install: build
	dh_testdir
	dh_testroot
	dh_clean -k
	dh_installdirs

	# Create the directories to install the source into
	dh_installdirs -p$(psource)  usr/src/modules/$(sname)/debian
	dh_installdirs -p$(psource)  usr/src/modules/$(sname)/driver

	# Copy only the driver source to the proper location
	cp driver/altro_driver_26.c 	\
	  debian/$(psource)/usr/src/modules/$(sname)/driver/altro_driver.c
	cp driver/Makefile 		\
	  debian/$(psource)/usr/src/modules/$(sname)/driver/
	cp altro/altro_common.h 	\
	  debian/$(psource)/usr/src/modules/$(sname)/driver/

	# Copy the needed debian/ pieces to the proper location
	cp debian/*modules.in* \
		debian/$(psource)/usr/src/modules/$(sname)/debian
	cp debian/*_KVERS_* debian/rules debian/changelog debian/copyright \
		debian/$(psource)/usr/src/modules/$(sname)/debian/
	cd debian/$(psource)/usr/src && \
		tar c modules | bzip2 -9 > $(sname).tar.bz2 && rm -rf modules

	# Add here commands to install the package into debian/altro.
	$(MAKE) install DESTDIR=$(CURDIR)/debian/tmp

	dh_install --sourcedir=$(CURDIR)/debian/tmp 

# Build architecture-independent files here.
# Pass -i to all debhelper commands in this target to reduce clutter.
binary-indep: build install
	dh_testdir -i
	dh_testroot -i
	dh_installchangelogs  -i
	dh_installdocs -i
	dh_installexamples -i
#	dh_install -i
#	dh_installmenu -i
#	dh_installdebconf -i
#	dh_installlogrotate -i
#	dh_installemacsen -i
#	dh_installpam -i
#	dh_installmime -i
#	dh_installinit -i
#	dh_installcron -i
#	dh_installinfo -i
	dh_installman -i
	dh_link -i
	dh_compress -i
	dh_fixperms -i
	dh_installdeb -i
#	dh_perl -i
#	dh_python -i
#	dh_makeshlibs -i
	dh_installdeb -i
	dh_shlibdeps -i
	dh_gencontrol -i
	dh_md5sums -i
	dh_builddeb -i

# Build architecture-dependent files here.
binary-arch: build install
	dh_testdir -s
	dh_testroot -s
#	dh_installdebconf -s
	dh_installdocs -s
	dh_installexamples -s
	dh_installmenu -s
#	dh_installlogrotate -s
#	dh_installemacsen -s
#	dh_installpam -s
#	dh_installmime -s
#	dh_installinit -s
	dh_installcron -s
#	dh_installman -s
	dh_installinfo -s
	dh_installchangelogs  -s
	dh_strip -s
	dh_link -s
	dh_compress -s
	dh_fixperms -s
	dh_makeshlibs -s
	dh_installdeb -s
#	dh_perl -s
	dh_shlibdeps -s
	dh_gencontrol -s
	dh_md5sums -s
	dh_builddeb -s

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install  binary-modules kdist kdist_config kdist_image kdist_clean
