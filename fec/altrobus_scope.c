/****************************************************************/
/*                                                              */
/*  file: altrobus_scope.c                                      */
/*                                                              */
/*  Author: Luciano Musa, CERN-EP                               */
/*                                                              */
/*  20. Jul. 05  last  update                                   */
/*                                                              */
/****************************************************************/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/file.h>
#include <signal.h>
#include "fec.h"


/***********/
/* Globals */
/***********/
u_int debug = 0;


/*************/
/* Prototypes*/
/*************/
int mainhelp(void);
int altrobus_menu(void);


/****************/
int mainhelp(void)
/****************/
{
  printf("\nCall Luciano Musa, 7676261, 163119 if you need help\n");
  return(0);
}


/******************/
int debug_mode(void)
/******************/
{
  int tl, pid;

  printf("Package identifiers:\n");
  printf("ALTRO library: %d\n", P_ID_ALTRO);
  printf("U2F library: %d\n", P_ID_U2F);
  printf("FEC library: %d\n", P_ID_FEC);
  printf("\n");
  printf("Enter the debug package: ");
  pid = getdecd(P_ID_FEC);
  printf("Enter the debug level: ");
  tl = getdecd(20);
  rcc_error_set_debug(pid, tl);

  printf("Setting the debugLevel:\n");
  printf("0: No debug\n");
  printf("1: messages from altrobus_scope\n");
  printf("======================================\n");
  printf("Your choice ");
  debug = getdecd(debug);
  return(0);
}


/******************************/
int main(int argc, char *argv[])
/******************************/
{  
  static int ret, fun = 1;
  
  ret = ts_open(1, TS_DUMMY);
  if (ret)
    rcc_error_print(stdout, ret);

  printf("\n\nThis is ALTRO BUS SCOPE\n");
  while(fun != 0)
  {
    printf("\n");
    printf("Select an option:\n");
    printf("  1 Print help\n");
    printf("  2 ALTROBUS menu\n");
    printf("  3 Set debugging parameters\n");
    printf("  0 Quit\n");
    printf("Your choice ");
    fun = getdecd(fun);
    if (fun == 1) mainhelp();
    if (fun == 2) altrobus_menu();
    if (fun == 3) debug_mode();
  }
  
  ts_close(TS_DUMMY);
  exit(0);
}


/*********************/
int altrobus_menu(void)
/*********************/
{  
  static u_int ret, fun, number_fec = 0, act_fec = 0, channel_address = 0, fec_address = 0, fa_enc = 0/*, fec_channel_address = 0*/;
  static u_int reg_address = 0, reg_data = 0, read_data = 0, num_cycles = 1, num_samples = 128;
  static u_int debug =0, rcode=0, rdata=0/*, ccode =0, cdata =0*/;
  static char fname[200] = "/home/rcu/NEW_U2F/altro_usb/log_files/backplane_test.log";
  char data_fname[200] = "/home/rcu/NEW_U2F/altro_usb/data_files/test_data";
  int fec_status_array[MAX_NR_OF_FECS]; u_int /*fec_status,*/ hadd, error = 0, loop, tick;
  int handle, handle2;
  FILE *file;
 
  /* Open FEC_LIB */
  ret = FEC_Open(DEFAULT_NODE, &handle);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return ret;
  }
  
  /* Open U2F_LIB */
  ret = U2F_Open(DEFAULT_NODE, &handle2, HW);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return ret;
  }
 
  fun = 1; 
  while(fun != 0)
  {
    printf("\n");
    printf("Select an option:\n");
    printf("   1 CONFIGURE THE SYSTEM   	2 RESET FECs\n");
    printf("   3 READ_ALTRO_REG         	4 WRITE_ALTRO_REG\n");
    printf("   5 READ_BC_REG            	6 WRITE_BC_REG\n");
    printf("   7 READ_FEC_ADD\n");
    printf("   8 WRITE_READ_CHECK_ALTRO_REG\n");
    printf("   9 WRITE_READ_CHECK_BC\n");
    printf("  10 FEC_READOUT			11 Read FEC Status\n");
    printf("  =========================================================\n");
    printf("   0 Quit\n");
    printf("Your choice ");
    fun = getdecd(fun);
    
    if((fun < 0) || (fun > 11))
    {
      printf ("\n ERROR: your selection is out of range\n");  
      return(-1);
    }

    if (fun == 1)
    {
      printf("\nNr of FECs ON ");
      number_fec = getdecd(number_fec);

      act_fec = 0;
      for(loop = 0; loop < number_fec; loop++)
      {
        printf("Enter Addres of FEC nr. %d ", loop);
        fec_address = getdecd(fec_address); 
	act_fec = (act_fec) | (1 << fec_address);  
      }
      /* Switch on FEC with Address = 0  */
      rcode = O_ACTFEC;
      rdata = act_fec;
      ret = U2F_Reg_Write(handle2, rcode, rdata);
      if (ret)
      {
	rcc_error_print(stdout, ret);
	return ret;
      }

	/* delay 1ms */
        ts_delay(1000);
	
	/* Omer Khalid: Check the status of all FECs */
	ret = FEC_All_Status(handle2, &fec_status_array[0]);
	CHECK_RET(ret)
	   
	/* delay 1s */
      	ts_delay(1000000);
	
	/* Omer Khalid: Reset All FECs */
	ret = FEC_All_Reset(handle2);
	CHECK_RET(ret)
	    
    }
    
    if (fun ==2)
    {    
	/* delay 1s */
      	ts_delay(1000000);
      	 
        /* Reset All FECs */
	ret = FEC_All_Reset(handle2);
	CHECK_RET(ret)

	/* delay 1ms */
        ts_delay(1000);
    }


    if (fun == 3) 
    {
      printf("Enter the FEC Address");
      fec_address = getdecd(fec_address);

      printf("Enter the Channel Address");
      channel_address = getdecd(channel_address);
      
      printf("Enter the Register Address");
      reg_address = gethexd(reg_address);

      printf("Enter the Number of Read Cycles (0 -> infinite loop) ");
      num_cycles = getdecd(num_cycles);

      channel_address = ((fec_address << 7) & 0xf80) | (channel_address & 0x7f);
      if(debug)
        printf("Channel Address = %x\n", channel_address);

      if(num_cycles)
      { 
        for(loop = 0; loop < num_cycles; loop++)
        {      
          ret = FEC_Read_ALTRO_Reg(handle, channel_address, reg_address, &reg_data);
          if(ret)
	  {
	    rcc_error_print(stdout, ret);
	    return ret;
	  }
	  printf("\nReg %x, of FEC Channel %d, of FEC %d, has value: 0x%x\n", reg_address, channel_address, fec_address, reg_data);   
        }
      }
    
      else
      {
        tick = 0;
        while(!num_cycles)
	{
           tick++;
           ret = FEC_Read_ALTRO_Reg(handle, channel_address, reg_address, &reg_data);
           if(ret)
	   {
	     rcc_error_print(stdout, ret);
	     return ret;
	   }
	   if(tick == 10000)
	   {
	     printf("\nLoop of 10000 Cycles Completed");
             printf("\nReg %x, of FEC Channel %d, of FEC %d, has value: 0x%x\n", reg_address, channel_address, fec_address, reg_data);   
	     tick =0;
	   }
	 }
       }  
    }	

    if (fun == 4) 
    {
      printf("Enter the FEC Address");
      fec_address = getdecd(fec_address);

      printf("Enter the Channel Address");
      channel_address = getdecd(channel_address);
      
      printf("Enter the Register Address");
      reg_address = gethexd(reg_address);

      printf("Enter the Register Data");
      reg_data = gethexd(reg_data);
       	
      printf("Enter the Number of Write Cycles (0 -> infinite loop) ");
      num_cycles = getdecd(num_cycles);

      channel_address = ((fec_address << 7) & 0xf80) | (channel_address & 0x7f);
      if(debug)
        printf("Channel Address = %x\n", channel_address);

      if(num_cycles)
      { 
        for(loop = 0; loop < num_cycles; loop++)
        {      
          ret = FEC_Write_ALTRO_Reg(handle, channel_address, reg_address, reg_data);
          if(ret)
	  {
	    rcc_error_print(stdout, ret);
	    return ret;
	  }
        }
      }
    
      else
      {
        tick = 0;
        while(!num_cycles)
	{
          tick++;
          ret = FEC_Write_ALTRO_Reg(handle, channel_address, reg_address, reg_data);
          if(ret)
	  {
	    rcc_error_print(stdout, ret);
	    return ret;
	  }
	  if(tick == 10000)
	  {
	    printf("\nLoop of 10000 Cycles Completed\n");
	    tick =0;
	  }
	}
      }  
    }	

    if (fun == 5) 
    {
      printf("Enter the FEC Address");
      fec_address = getdecd(fec_address);
    
      printf("Enter the BC Register Address");
      reg_address = gethexd(reg_address);

      printf("Enter the Number of Read Cycles (0 -> infinite loop) ");
      num_cycles = getdecd(num_cycles);

      if(num_cycles)
      { 
        for(loop = 0; loop < num_cycles; loop++)
        {      
          ret = FEC_BC_Reg_Read(handle, fec_address, reg_address, &reg_data);
          if(ret)
	  {
	    rcc_error_print(stdout, ret);
	    return ret;
	  }
	  printf("\nBC Reg %x, of FEC %d, has value: 0x%x\n", reg_address, fec_address, reg_data);   
        }
      }
    
      else
      {
        tick = 0;
        while(!num_cycles)
	{
          tick++;
          ret = FEC_BC_Reg_Read(handle, fec_address, reg_address, &reg_data);
          if(ret)
	  {
	    rcc_error_print(stdout, ret);
	    return ret;
	  }
	  if(tick == 10000)
	  {
	    printf("\nLoop of 10000 Cycles Completed");
            printf("\nBC Reg %x, of FEC %d, has value: 0x%x\n", reg_address, fec_address, reg_data);   
	    tick =0;
	  }
	}
      }  
    }	


    if (fun == 6) 
    {
      printf("Enter the FEC Address");
      fec_address = getdecd(fec_address);
      
      printf("Enter the BC Register Address");
      reg_address = gethexd(reg_address);

      printf("Enter the Register Data");
      reg_data = gethexd(reg_data);
       	
      printf("Enter the Number of Write Cycles (0 -> infinite loop) ");
      num_cycles = getdecd(num_cycles);

      if(num_cycles)
      { 
        for(loop = 0; loop < num_cycles; loop++)
        {      
          ret = FEC_BC_Reg_Write(handle, fec_address, reg_address, reg_data);
          if(ret)
	  {
	    rcc_error_print(stdout, ret);
	    return ret;
	  }
        }
      }
    
      else
      {
        tick = 0;
        while(!num_cycles)
	{
          tick++;
          ret = FEC_BC_Reg_Write(handle, fec_address, reg_address, reg_data);
          if(ret)
	  {
	    rcc_error_print(stdout, ret);
	    return ret;
	  }
	  if(tick == 10000)
	  {
	    printf("\nLoop of 10000 Cycles Completed\n");
	    tick =0;
	  }
	}
      }  
    }	


    if (fun == 7) 
    {
      printf("Enter the FEC Address");
      fec_address = getdecd(fec_address);
    
      printf("Enter the Number of Read Cycles (0 -> infinite loop) ");
      num_cycles = getdecd(num_cycles);

      if(num_cycles)
      { 
        for(loop = 0; loop < num_cycles; loop++)
        {      
          ret = FEC_BC_Reg_Read(handle, fec_address, 0x13, &reg_data);
          if(ret)
	  {
	    rcc_error_print(stdout, ret);
	    return ret;
	  }
	  hadd = (reg_data & 0xf800) >> 11;
	  printf("\nAt Address 0x%x respends FEC with HADD 0x%x\n", fec_address, hadd);   
        }
      }
    
      else
      {
        tick = 0;
        while(!num_cycles)
	{
          tick++;
          ret = FEC_BC_Reg_Read(handle, fec_address, 0x13, &reg_data);
          if(ret)
	  {
	    rcc_error_print(stdout, ret);
	    return ret;
	  }
	  if(tick == 10000)
	  {
	    printf("\nLoop of 10000 Cycles Completed");
            hadd = reg_data;
	    printf("\nAt Address 0x%x respends FEC with HADD 0x%x\n", fec_address, hadd);   
	    tick =0;
	  }
	}
      }  
    }	


    if (fun == 8) 
    {
      printf("Enter the FEC Address");
      fec_address = getdecd(fec_address);

      printf("Enter the Channel Address");
      channel_address = getdecd(channel_address);
      
      printf("Enter the Register Address");
      reg_address = gethexd(reg_address);

      printf("Enter the Register Data");
      reg_data = gethexd(reg_data);
       	
      printf("Enter the Number of Write/Read Cycles (0 -> infinite loop) ");
      num_cycles = getdecd(num_cycles);

      channel_address = ((fec_address << 7) & 0xf80) | (channel_address & 0x7f);
      if(debug)
        printf("Channel Address = %x\n", channel_address);

      if(num_cycles)
      { 
        for(loop = 0; loop < num_cycles; loop++)
        {      
          ret = FEC_Write_ALTRO_Reg(handle, channel_address, reg_address, reg_data);
          if(ret)
	  {
	    rcc_error_print(stdout, ret);
	    return ret;
	  }
	    
          ret = FEC_Read_ALTRO_Reg(handle, channel_address, reg_address, &read_data);
          if(ret)
	  {
	    rcc_error_print(stdout, ret);
	    return ret;
	  }

          if(read_data != reg_data)
          {
	    printf("Test error\n");
            file = fopen(fname,"a");
            fprintf(file, "Error in cycle Nr. %d\n", loop);
            fprintf(file, "Written: 0x%x;\t\tRead: %x\n", reg_data, read_data);
	    fclose(file);
          }	
        }
      }
    
      else
      {
        tick = 0;
        while(!num_cycles)
	{
          tick++;

          ret = FEC_Write_ALTRO_Reg(handle, channel_address, reg_address, reg_data);
          if(ret)
	  {
	    rcc_error_print(stdout, ret);
	    return ret;
	  }

          ret = FEC_Read_ALTRO_Reg(handle, channel_address, reg_address, &read_data);
          if(ret)
	  {
	    rcc_error_print(stdout, ret);
	    return ret;
	  }

          if(read_data != reg_data)
          {
      	    printf("Test error\n");
            error++;
            file = fopen(fname,"a");
            fprintf(file, "Error in cycle Nr. %d\n", tick++);
            fprintf(file, "Written: 0x%x;\t\tRead: %x\n", reg_data, read_data);
          }	

	  if(tick == 10000)
	  {
	    printf("\nLoop of 10000 Cycles Completed\n");
	    tick =0;
	  }

	  if(error == 1000)
	  {
	    printf("\n\n THE SYSTEM IS MALFUNCTIONING\n\n");
	    exit(0);
	  } 
	}
      }  
    }  /* end of function */ 	


    if (fun == 10) 
    {
      printf("Enter the FEC Address (0..15)");
      fec_address = getdecd(fec_address);

      printf("Enter the Number of Events");
      num_cycles = getdecd(num_cycles);
      
      printf("Enter the Number of Samples");
      num_samples = getdecd(num_samples); 

      printf("\nCalling FEC_Event_Readout\n");
      /* encoding FEC address */
      fa_enc = 1 << (fec_address - 1);
      ret = FEC_Event_Readout(handle, fa_enc, 1, 0, num_cycles, num_samples, 0, data_fname); 
      if(ret)
      {
		rcc_error_print(stdout, ret);
		return ret;
      }
    }  /* end of function */ 	 
    
    if (fun ==11)
    {
    
      /* Reset All FECs */
	  ret = FEC_All_Status(handle2, &fec_status_array[0]);
	  CHECK_RET(ret)
    }
    
  } /* end of while */

  ret = U2F_Close(handle2);
  if(ret)
  {
    rcc_error_print(stdout, ret);
    return ret;
  }
  
  /* Close FEC_LIB */
  ret = FEC_Close(handle);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return ret;
  }
  return(0);
}
