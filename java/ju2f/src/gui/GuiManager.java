/**
 * 
 */
package gui;

import core.modules.ImplementationManager;
import gui.root.RootFrame;

/**
 * @author okhalid
 *
 */
public class GuiManager {
	
	public ImplementationManager implManager;
	public RootFrame root;
	
	public GuiManager(ImplementationManager implMngr)
	{
		initImplManagerReference(implMngr);
		run();
	}

	public void initImplManagerReference(ImplementationManager implMngr)
	{
		implManager = implMngr;
	}
	public void run()
	{
		initComponents();
	}
	
	private void initComponents()
	{
		initRootFrame();
	}

	
	private void initRootFrame()
	{
//		 This code runs the GUI in a seperate thread from the main thread.
        java.awt.EventQueue.invokeLater
                (
                    new Runnable()
                    {
                        public void run()
                        {
                           root = new RootFrame();
                           root.setVisible(true);
                           root.run(implManager);
                        }
                    }
                );
	}
}
