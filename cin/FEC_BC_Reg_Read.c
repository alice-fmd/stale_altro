/* CIN source file */

#include "extcode.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/file.h>
#include <signal.h>
#include "fec/fec.h"

MgErr CINRun(uInt32 *Handle, uInt32 *fec_addr, uInt32 *reg_addr, uInt32 *data, uInt32 *error);

MgErr CINRun(uInt32 *Handle, uInt32 *fec_addr, uInt32 *reg_addr, uInt32 *data, uInt32 *error)
	{
	*error = FEC_BC_Reg_Read(*Handle, *fec_addr, *reg_addr, data);
	return noErr;
	}
