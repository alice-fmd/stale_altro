/**
 * 
 */
package core.database.data;

import core.global.GlobalVariables;

/**
 * @author okhalid
 *
 */
public class ChannelData {

	public int[] channelRegisters = new int[GlobalVariables.MaxPerAltroChannelRegisters];
	
	public ChannelData() {}

	public int[] getChannels()
	{
		return channelRegisters;
	}
	
	public void setChannels(int[] array)
	{
		channelRegisters = array;
	}
}
