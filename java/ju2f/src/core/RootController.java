/**
 * 
 */
package core;

import gui.GuiManager;
import core.database.DatabaseManager;
import core.modules.ImplementationManager;
import core.global.GlobalVariables;
import java.net.*;
import java.security.*;


/**
 * @author okhalid
 *
 */
public class RootController {

	private ImplementationManager implManager;
	private GuiManager guiManager;
	private DatabaseManager dbManager;
	
	public RootController(){}
	
	public void run()
	{
		initComponents();
	}
	
	public void initComponents()
	{
		/* Set the load path of images relative to the source
		   of this file */
		Class            cls    = this.getClass();
		ProtectionDomain domain = cls.getProtectionDomain();
		CodeSource       source = domain.getCodeSource();
		URL              loc    = source.getLocation();
		String           us     = loc.toString();
		if (us.endsWith(".jar")) 
		    GlobalVariables.imagePath = "jar:" + loc + "!/images/";

		/* Creates an instance of the Implementation Manager
		 * and passes the reference of the Gui Manager */ 
		implManager = new ImplementationManager(guiManager);
		
		/* Creates an instance of the GUIManager and passes
		 * the reference of Implementation Manager */ 
		guiManager  = new GuiManager(implManager);
		
		/* Creates an instance of the database manager, and
		 * runs the thread */ 
		dbManager = new  DatabaseManager(); 
		dbManager.run();

	}
	
}
