/********************************************************/
/*  file : get_input.h					*/
/*							*/
/*  prototypes for get_input functions			*/
/*							*/
/*  970122 J.O.Petersen + M.Joos 			*/
/********************************************************/

#ifndef _GET_INPUT_H
#define _GET_INPUT_H

#ifdef __cplusplus
extern "C" 
{
#endif

int getdec(void);
int getdecd(int defa);
unsigned int gethex(void);
unsigned int gethexd(unsigned int defa);
char getfstchar(void);
void getstrd(char* a, char* defa);
float getfloatd(float defa);

#ifdef __cplusplus
}
#endif

#endif

