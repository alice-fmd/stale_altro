/* CIN source file */

#include "extcode.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/file.h>
#include <signal.h>
#include "fec/fec.h"

MgErr CINRun(uInt32 *Handle, int32 *fec_addr, int32 *reg_addr, int32 *data, int32 *error);

MgErr CINRun(uInt32 *Handle, int32 *fec_addr, int32 *reg_addr, int32 *data, int32 *error)
	{
        *error = FEC_BC_Reg_Write(*Handle, *fec_addr, *reg_addr, *data);
	return noErr;
	}
