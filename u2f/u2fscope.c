/****************************************************************/
/*                                                              */
/*  file: u2fscope.c                                            */
/*                                                              */
/* This program allows to access the resources of a USB2FEC     */
/* card in a user friendly way and includes some test routines  */
/*                                                              */
/*  Author: Markus Joos, CERN-EP                                */
/*                                                              */
/*  16. Mar. 04  MAJO  created                                  */
/*                                                              */
/****************C 2004 - A nickel program worth a dime**********/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/file.h>
#include <signal.h>
#include <tools/get_input.h>
#include <tools/rcc_time_stamp.h>
#include <tools/rcc_error.h>
#include "u2f.h"

/***********/ 
/* Globals */
/***********/ 
int occ = 0;
int emode = HW;

/*************/
/* Prototypes*/
/*************/
int mainhelp(void);
int u2ffuncmenu(void);
int setdebug(void);


/****************/
int setdebug(void)
/****************/
{
  int tl, pid;
  
  printf("Package identifiers:\n");
  printf("ALTRO library: %d\n", P_ID_ALTRO);
  printf("U2F library: %d\n", P_ID_U2F);
  printf("\n");
  printf("Enter the debug package: ");
  pid = getdecd(P_ID_U2F);
  printf("Enter the debug level: ");
  tl = getdecd(20);
  rcc_error_set_debug(pid, tl);
  return(0);
}


/****************/
int mainhelp(void)
/****************/
{
  printf("Call Markus Joos, 72364, 160663 if you need help\n");
  return(0);
}


/******************************/
int main(int argc, char *argv[])
/******************************/
{  
  static int ret, fun = 1;
  int i;
  emode = HW;
  
  for (i = 1; i < argc; i++) {
    if (argv[i][0] == '-') {
      switch (argv[i][1]) {
      case 'h':
	printf("This is u2fscope.\n\n");
	printf("Usage: u2fscope [OPTIONS] [ALTRO occurrence]\n");
	printf("The occurrence is the number of an altro card as assigned by the driver.\n");
	printf("It matches the number of the device node. (e.g. /dev/usb/altro1)\n");
	printf("Legal values are currently 0 to 4\n\n");
	printf("OPTIONS:\n");
	printf("\t-h\tThis help\n");
	printf("\t-e\tEmulation mode\n\n");
	exit(0);
	break;
      case 'e':
	emode = SW;
	break;
      }
    }
    else 
      sscanf(argv[i], "%d", &occ);
  }
	
  ret = ts_open(1, TS_DUMMY);
  if (ret)
    rcc_error_print(stdout, ret);
 
  printf("\n\n\nThis is U2FSCOPE\n");
  while(fun != 0)
  {
    printf("\n");
    printf("Select an option:\n");
    printf("  1 Print help\n");
    printf("  2 U2F menu\n");
    printf("  3 Set debugging parameters\n");
    printf("  0 Quit\n");
    printf("Your choice ");
    fun = getdecd(fun);
    if (fun == 1) mainhelp();
    if (fun == 2) u2ffuncmenu();
    if (fun == 3) setdebug();
  }

  ts_close(TS_DUMMY);
  exit(0);
}


/*******************/
int u2ffuncmenu(void)
/*******************/
{  
  static u_int ret, fun, choice =1, data_mask, rcode = 0, rdata = 0, fdata[2048] = {0}, fsize = 0, ccode = 0, cdata = 0;
  static u_int swtrig = 1, offset = 0, rsize = 10, memdata[2048] = {0}, dsource = 0, mdsize = 0x1000;
  static u_int tofile = 0;
  static int handle = 0;
  static char fname[200] = {0}, dnodename[100] = {0}, nodename[100] = {0};
  static errst_t errst_s;
  static trcfg_t trcfg_s;
  static trcnt_t trcnt_s;
  u_int loop, osize, index;
  FILE  *file = 0;
  int yesFile = 0;
  u_short *rodata, sd1, sd2, ad1, ad2;
  tstamp ts1, ts2;
  float delta_t;

  fun = 1; 
  while(fun != 0)
  {
    printf("\n");
    printf("Select an option:\n");
    printf("   1 U2F_Open               2 U2F_Close\n");
    printf("   3 U2F_IMEM_Read          4 U2F_IMEM_Write\n");
    printf("   5 U2F_PMEM_Read          6 U2F_PMEM_Write\n");
    printf("   7 U2F_RMEM_Read          8 U2F_RMEM_Write\n");
    printf("   9 U2F_ACL_Read          10 U2F_ACL_Write\n");
    printf("  11 U2F_DM1_Read           		\n");
    printf("  13 U2F_DM2_Read          			\n");
    printf("  15 U2F_Reg_Read          16 U2F_Reg_Write\n");
    printf("  17 U2F_File_Read         18 U2F_File_Write\n");
    printf("  19 U2F_ERRST_decode      20 U2F_TRCFG1_decode\n");
    printf("  21 U2F_TRCNT_decode      22 U2F_TRCFG1_encode\n");
    printf("  23 U2F_Exec_Command      24 U2F_Reset\n");
    printf("  25 U2F_ReadOut\n");
    printf("  ============================================\n");
    printf("  30 Read all registers\n");
    printf("   0 Quit\n");
    printf("Your choice ");
    fun = getdecd(fun);
    if (fun == 1) 
    {
      sprintf(dnodename, "%s%d", BASE_NODE, occ);
      printf("Enter the name of the node: ");
      getstrd(nodename, dnodename);
      ret = U2F_Open(nodename, &handle, emode);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
    }
    
    if (fun == 2) 
    {
      ret = U2F_Close(handle);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
    }    
 
    if (fun == 3) 
    {
      printf("Enter the offset were you want to start reading ");
      offset = getdecd(offset);
      printf("How many words do you want to read ");
      rsize = getdecd(rsize);
      ret = U2F_IMEM_Read(handle, rsize, &osize, offset, memdata);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
      printf("\n%d words received\n\n", osize);
      for (loop = 0; loop < osize; loop++)
        printf("Word %d = 0x%08x\n", loop, memdata[loop]);
    }    

    if (fun == 4)
    {
      printf("Enter the offset were you want to start writing ");
      offset = getdecd(offset);
      
      printf("How many words do you want to write ");
      rsize = getdecd(rsize);
      
      printf("Select the data source (0 = Enter by hand  1 = From last U2F_File_Read) ");
      dsource = getdecd(dsource);
 
      if (!dsource)
      {
        for (loop = 0; loop < rsize; loop++)
	{
	  printf("Enter data word%d 0x", loop);
	  memdata[loop] = gethexd(memdata[loop]);
	}
      }
      else
      {
        for (loop = 0; loop < rsize; loop++)
	  memdata[loop] = fdata[loop];
      }

      ret = U2F_IMEM_Write(handle, rsize, offset, memdata);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
    }      

    if (fun == 5)
    {    
      printf("Enter the offset were you want to start reading ");
      offset = getdecd(offset);
      
      printf("How many words do you want to read ");
      rsize = getdecd(rsize);
      ret = U2F_PMEM_Read(handle, rsize, &osize, offset, memdata);
      if (ret)
      {
        printf("\n%d words received\n\n", osize);
        rcc_error_print(stdout, ret);
        return(-1);
      }
      printf("\n%d words received \n\n", osize);
      for (loop = 0; loop < osize; loop++)
        printf("Word %d = 0x%08x\n", loop, memdata[loop]);
    }     

    if (fun == 6)
    {    
      printf("Enter the offset were you want to start writing ");
      offset = getdecd(offset);      
      
      printf("How many words do you want to write ");
      rsize = getdecd(rsize);
      
      printf("Select the data source (0 = Enter by hand  1 = From last U2F_File_Read) ");
      dsource = getdecd(dsource);
 
      if (!dsource)
      {
        for (loop = 0; loop < rsize; loop++)
	{
	  printf("Enter data word %d ", loop);
	  memdata[loop] = gethexd(memdata[loop]);
	}
      }
      else
      {
        for (loop = 0; loop < rsize; loop++)
	  memdata[loop] = fdata[loop];
      }

      ret = U2F_PMEM_Write(handle, rsize, offset, memdata);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
    }     

    if (fun == 7)
    {    
      printf("Enter the offset were you want to start reading ");
      offset = getdecd(offset);      
      
      printf("How many words do you want to read ");
      rsize = getdecd(rsize);
      ret = U2F_RMEM_Read(handle, rsize, &osize, offset, memdata);
      if (ret)
      {
        printf("\n%d words received\n\n", osize);
        rcc_error_print(stdout, ret);
        return(-1);
      }
      printf("\n%d words received\n\n", osize);
      for (loop = 0; loop < osize; loop++)
        printf("Word %d = 0x%08x\n", loop, memdata[loop]);
    }     
 
    if (fun == 8)
    {    
      printf("Enter the offset were you want to start writing ");
      offset = getdecd(offset);
      
      printf("How many words do you want to write ");
      rsize = getdecd(rsize);
      
      printf("Select the data source (0 = Enter by hand  1 = From last U2F_File_Read) ");
      dsource = getdecd(dsource);
 
      if (!dsource)
      {
        for (loop = 0; loop < rsize; loop++)
	{
	  printf("Enter data word %d 0x", loop);
	  memdata[loop] = gethexd(memdata[loop]);
	}
      }
      else
      {
        for (loop = 0; loop < rsize; loop++)
	  memdata[loop] = fdata[loop];
      }

      ret = U2F_RMEM_Write(handle, rsize, offset, memdata);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
    }     
 
    if (fun == 9)
    {    
      printf("Enter the offset were you want to start reading ");
      offset = getdecd(offset);
      
      printf("How many words do you want to read ");
      rsize = getdecd(rsize);
      ret = U2F_ACL_Read(handle, rsize, &osize, offset, memdata);
      if (ret)
      {
        printf("\n%d words received\n\n", osize);
        rcc_error_print(stdout, ret);
        return(-1);
      }
      printf("\n%d words received\n\n", osize);
      for (loop = 0; loop < osize; loop++)
        printf("Word %d = 0x%08x\n", loop, memdata[loop]);
    }     
 
    if (fun == 10)
    {    
      printf("Enter the offset were you want to start writing ");
      offset = getdecd(offset);
      
      printf("How many words do you want to write ");
      rsize = getdecd(rsize);
      
      printf("Select the data source (0 = Enter by hand  1 = From last U2F_File_Read) ");
      dsource = getdecd(dsource);
 
      if (!dsource)
      {
        for (loop = 0; loop < rsize; loop++)
	{
	  printf("Enter data word %d 0x", loop);
	  memdata[loop] = gethexd(memdata[loop]);
	}
      }
      else
      {
        for (loop = 0; loop < rsize; loop++)
	  memdata[loop] = fdata[loop];
      }

      ret = U2F_ACL_Write(handle, rsize, offset, memdata);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
    }     
 
    if (fun == 11)
    {    
      printf("Enter the offset were you want to start reading ");
      offset = getdecd(offset);
      
      printf("How many words do you want to read ");
      rsize = getdecd(rsize);
      ret = U2F_DM1_Read(handle, rsize, &osize, offset, memdata);
      if (ret)
      {
        printf("\n%d words received\n\n", osize);
        rcc_error_print(stdout, ret);
        return(-1);
      }

      yesFile = 0;
      printf("Enter '1' if you want to have a file with data from mem ");
      if (getdecd(yesFile) == 1) 
      {
	printf("Enter the name of file ");
        getstrd(fname, fname);	
	file = fopen(fname,"w");
	yesFile = 1;
      }
      
      printf("\n%d words received\n\n", osize);
      for (loop = 0; loop < osize; loop++) 
      {
        printf("Word %d: data[9..0]   = 0x%03x\n", loop, memdata[loop * 4]);
        printf("Word %d: data[19..10] = 0x%03x\n", loop, memdata[loop * 4 +1]);
        printf("Word %d: data[29..20] = 0x%03x\n", loop, memdata[loop * 4 +2]);
        printf("Word %d: data[39..30] = 0x%03x\n", loop, memdata[loop * 4 +3]);
	
	if(yesFile) 
	{
          fprintf(file, "%d\n", memdata[loop * 4]);
          fprintf(file, "%d\n", memdata[loop * 4 +1]);
          fprintf(file, "%d\n", memdata[loop * 4 +2]);
          fprintf(file, "%d\n", memdata[loop * 4 +3]);
	}
      }
      if(yesFile)	    
        fclose(file);
    }     

    if (fun == 13)
    {    
      printf("Enter the offset were you want to start reading ");
      offset = getdecd(offset);
      
      printf("How many words do you want to read ");
      rsize = getdecd(rsize);
      ret = U2F_DM2_Read(handle, rsize, &osize, offset, memdata);
      if (ret)
      {
        printf("\n%d words received\n\n", osize);
        rcc_error_print(stdout, ret);
        return(-1);
      }
      
      yesFile = 0;
      printf("Enter '1' if you want to have a file with data from mem  ");
      if (getdecd(yesFile) == 1) 
      {
	printf("Enter the name of file  ");
        getstrd(fname, fname);	
	file = fopen(fname,"w");
	yesFile = 1;
      }
      
      printf("\n%d words received\n\n", osize);
      for (loop = 0; loop < osize; loop++) 
      {
        printf("Word %d: data[9..0]   = 0x%03x\n", loop, memdata[loop * 4]);
        printf("Word %d: data[19..10] = 0x%03x\n", loop, memdata[loop * 4 +1]);
        printf("Word %d: data[29..20] = 0x%03x\n", loop, memdata[loop * 4 +2]);
        printf("Word %d: data[39..30] = 0x%03x\n", loop, memdata[loop * 4 +3]);
	
	if(yesFile) 
	{
          fprintf(file, "%d\n", memdata[loop * 4]);
          fprintf(file, "%d\n", memdata[loop * 4 +1]);
          fprintf(file,"%d\n", memdata[loop * 4 +2]);
          fprintf(file, "%d\n", memdata[loop * 4 +3]);
	}
      }
      if(yesFile)	    
        fclose(file);
    }     

    if (fun == 15)
    {    
      printf("\nSelect an option:\n");
      printf("  1) ERRST\n");
      printf("  2) TRCFG1\n");
      printf("  3) TRCNT\n");
      printf("  4) LWADD\n");
      printf("  5) IRADD\n");
      printf("  6) IRDAT\n");
      printf("  7) EVWORD\n");
      printf("  8) ACT_FEC\n");
      printf("  9) FMIREG\n");
      printf(" 10) FMOREG\n");
      printf(" 11) TRCFG2\n");
      printf("Your choice ");
      choice = getdecd(choice);      
      if ((choice < 1) || (choice > 11))
      {
      	printf ("\n ERROR: your selection is out of range\n");  
        return(-1);
      }
      else
      {
      	switch (choice)
        {
	  case 1: 
	    rcode = O_ERRST; 
	    data_mask = 0x0000000f;
	    break;
	  case 2: 
	    rcode = O_TRCFG1; 
	    data_mask = 0xffffffff;
	    break;
	  case 3: 
	    rcode = O_TRCNT; 
	    data_mask = 0xffffffff;
	    break;
	  case 4: 
	    rcode = O_LWADD; 
	    data_mask = 0x0000ffff;
	    break;
	  case 5: 
	    rcode = O_IRADD; 
	    data_mask = 0x000fffff;
	    break;
	  case 6: 
	    rcode = O_IRDAT; 
	    data_mask = 0x000fffff;
	    break;
	  case 7: 
	    rcode = O_EVWORD;
	    data_mask = 0x0000001f;
	    break;
	  case 8: 
	    rcode = O_ACTFEC; 
	    data_mask = 0x00001fff;
	    break;
	  case 9: 
	    rcode = O_FMIREG; 
	    data_mask = 0x0000ffff;
	    break;
	  case 10: 
	    rcode = O_FMOREG; 
	    data_mask = 0x000000ff;
	    break;
	  case 11: 
	    rcode = O_TRCFG2; 
	    data_mask = 0x00000003;
	    break;
	  case 12: 
	    rcode = O_RESREG; 
	    data_mask = 0x0000ffff;
	    break;
	  case 13: 
	    rcode = O_ERRREG; 
	    data_mask = 0x00000003;
	    break;
	  case 14: 
	    rcode = O_INTMOD; 
	    data_mask = 0x00000003;
	    break;
	  case 15: 
	    rcode = O_INTREG; 
	    data_mask = 0x0000ffff;
	    break;
	}    
        ret = U2F_Reg_Read(handle, rcode, &rdata);
        if (ret)
        {
           rcc_error_print(stdout, ret);
           return(-1);
        } 

        printf("The register contains 0x%08x\n", rdata & data_mask);    
      }
    }     

    if (fun == 16)
    {    
      printf("\nSelect an option:\n");
      printf("  1) TRCFG1\n");
      printf("  2) ACT_FEC\n");
      printf("  3) FMIREG\n");
      printf("  4) FMOREG\n");
      printf("  5) TRCFG2\n");
      printf("Your choice ");
      choice = getdecd(choice);      
      if ((choice < 1) || (choice > 5))
      {
      	printf ("\n ERROR: your selection is out of range\n");  
        return(-1);
      }
      else
      {
      	switch (choice)
      	{
	  case 1: rcode = O_TRCFG1; break;
	  case 2: rcode = O_ACTFEC; break;
	  case 3: rcode = O_FMIREG; break;
	  case 4: rcode = O_FMOREG; break;
	  case 5: rcode = O_TRCFG2; break;
	  case 6: rcode = O_RESREG; break;
	  case 7: rcode = O_ERRREG; break;
	  case 8: rcode = O_INTMOD; break;
	  case 9: rcode = O_INTREG; break;
	}
        printf("Enter data that you want to write ");
        rdata = gethexd(rdata);      	
        ret = U2F_Reg_Write(handle, rcode, rdata);
        if (ret)
        {
           rcc_error_print(stdout, ret);
           return(-1);
        }
      }

    }     
 
    if (fun == 17)
    {    
      printf("Enter the path and name of the file to read ");
      getstrd(fname, fname);
      printf("Enter the number of words to read ");
      fsize = getdecd(fsize);

      ret = U2F_File_Read(fname, fsize, fdata);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
      printf("The file has been read into the internal array\n"); 
    }     
 
    if (fun == 18)
    {    
      printf("Enter the path and name of the file to write ");
      getstrd(fname, fname);
      printf("Enter the number of words to write ");
      fsize = getdecd(fsize);

      ret = U2F_File_Write(fname, fsize, fdata);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
      printf("The file has been written with data from the internal array\n");       
    }     
 
    if (fun == 19)
    {   
      printf("Enter the content of the ERRST register: 0x");
      rdata = gethexd(rdata);

      ret = U2F_ERRST_decode(rdata, &errst_s);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
      printf("Pattern error is : %s\n", errst_s.pattern_error ? "True" : "False");     
      printf("Abort is         : %s\n", errst_s.abort ? "True" : "False");
      printf("Time-out is      : %s\n", errst_s.timeout ? "True" : "False");
      printf("Altro error is   : %s\n", errst_s.altro_error ? "True" : "False");
    }     
 
    if (fun == 20)
    {    
      printf("Enter the content of the TRCFG1 register: 0x");
      rdata = gethexd(rdata);
      
      ret = U2F_TRCFG1_decode(rdata, &trcfg_s);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
      
      printf("The system waits %d clock cycles after arrival of L1 trigger and issuing of L2 trigger\n", trcfg_s.tw);
      printf("Buffer mode                  : %s\n", trcfg_s.bmd ? "8 buffers" : "4 buffers");
      printf("Trigger mode                  : ");
      if (trcfg_s.mode == 0) printf("Software trigger\n");      
      if (trcfg_s.mode == 1) printf("Out of range\n");
      if (trcfg_s.mode == 2) printf("External L1 and automatic L2 after TW\n");
      if (trcfg_s.mode == 3) printf("External L1 and L2\n");
      printf("There are %d buffers remaining in the multi event buffer\n",trcfg_s.remb);
      printf("Multi event buffer empty     : %s\n",trcfg_s.empty ? "True" : "False");
      printf("Multi event buffer full      : %s\n",trcfg_s.full ? "True" : "False");
      printf("The read pointer position is : %d\n",trcfg_s.rd_pt); 
      printf("The write pointer position is: %d\n",trcfg_s.wr_pt); 
    }     
 
    if (fun == 21)
    {    
      printf("Enter the content of the TRCNT register: 0x");
      rdata = gethexd(rdata);
      
      ret = U2F_TRCNT_decode(rdata, &trcnt_s);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
      printf("Number of L1 triggers received: %d\n", trcnt_s.ntr); 
      printf("Number of L1 triggers accepted: %d\n", trcnt_s.nta); 
    }     
 
    if (fun == 22)
    {    
      printf("Enter the value for Trigger Wait: 0x");
      trcfg_s.tw = gethexd(trcfg_s.tw);
      
      printf("Enter the value for Buffer Mode : ");
      trcfg_s.bmd = gethexd(trcfg_s.bmd);
      
      printf("Enter the value for Trigger Mode: ");
      trcfg_s.mode = getdecd(trcfg_s.mode);
    
      ret = U2F_TRCFG1_encode(&rdata, trcfg_s);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }      
      printf("The encoded data is: 0x%08x\n", rdata);
    }

    if (fun == 23)
    {    
      printf("\nSelect an option:\n");
      printf("  1) RS_STATUS\n" );
      printf("  2) RS_TRCFG \n" );
      printf("  3) RS_TRCNT \n" );
      printf("  4) EXEC     \n" );
      printf("  5) ABORT    \n" );
      printf("  6) FECRST   \n" );
      printf("  7) SWTRG    \n" );
      printf("  8) TRG_CLR  \n" );
      printf("  9) RS_DMEM1 \n" );
      printf(" 10) RS_DMEM2 \n" );
      printf(" 11) WRFM     \n" );
      printf(" 12) RDFM     \n" );
      printf(" 13) RDABORT  \n" );
      printf("Your choice ");

      choice = getdecd(choice); 
      if (choice == 4)
      {
        printf("Enter the execution offset 0x");
	cdata = gethexd(cdata);
      }      
      if ((choice < 1) || (choice > 13))
      {
      	printf ("\n ERROR: your selection is out of range\n");  
        return(-1);
      }
      else
      {
	switch (choice)
	{
	  case 1: 
	    ccode = C_RS_STATUS; 
	    break;
	  case 2: 
	    ccode = C_RS_TRCFG; 
	    break;
	  case 3: 
	    ccode = C_RS_TRCNT; 
	    break;
	  case 4: 
	    ccode = C_EXEC; 
	    break;
	  case 5: 
	    ccode = C_ABORT; 
	    break;
	  case 6: 
	    ccode = C_FECRST; 
	    break;
	  case 7: 
	    ccode = C_SWTRG; 
	    break;
	  case 8: 
	    ccode = C_TRG_CLR; 
	    break;
	  case 9: 
	    ccode = C_RS_DMEM1; 
	    break;
	  case 10: 
	    ccode = C_RS_DMEM2; 
	    break;			
	  case 11: 
	    ccode = C_WRFM; 
	    break;			
	  case 12: 
	    ccode = C_RDFM; 
	    break;			
	  case 13: 
	    ccode = C_RDABORT; 
	    break;			
	  case 14: 
	    ccode = C_CLR_EVTAG; 
	    break;			
	  case 15: 
	    ccode = C_SCCOMMAND; 
	    break;			
	}    
        ret = U2F_Exec_Command(handle, ccode, cdata);
        if (ret)
        {
           rcc_error_print(stdout, ret);
           return(-1);
        }
      }
    }

    if (fun == 24)
    {
      ret = U2F_Reset(handle);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }    
    }

    if (fun == 25)
    {
      printf("Enter the max. data size in bytes: ");
      mdsize = gethexd(mdsize);
     
      printf("Send S/W trigger (1=yes 0=no): ");
      swtrig = getdecd(swtrig);
      
      printf("Do you want to write the data to file (1=yes 0=no): ");
      tofile = getdecd(tofile);

      if (tofile)
      {
        printf("Enter the path and name of the file to write: ");
        getstrd(fname, fname);
      }

      rodata = (u_short *)malloc(mdsize);
      if (rodata == 0)
      {
        printf("Error from malloc()\n");
	return(-1);
      }

      ts_clock(&ts1);
      if (swtrig)   
        ret = U2F_ReadOut(handle, mdsize, &osize, (u_char *)rodata, M_FIRST | M_LAST | M_TRIGGER);
      else
        ret = U2F_ReadOut(handle, mdsize, &osize, (u_char *)rodata, M_FIRST | M_LAST);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      } 
      ts_clock(&ts2);

      delta_t = ts_duration(ts1, ts2);
      printf("%d bytes received in %f seconds\n", osize, delta_t); 

      if (tofile)
      {
        printf("Dumping data to file\n");

        file = fopen(fname, "w");
	if (file == 0) 
	{
	  printf("Can't open output file\n");
	  return(-1);
	}
  
        index = 0;
        for (loop = 0; loop < (osize >> 2); loop++)
	{
	  sd1 = rodata[index++];
	  sd2 = rodata[index++];
          ad1 = sd1 & 0x3ff;
	  ad2 = (sd1 >> 10) | ((sd2 & 0xf) << 6);
	  if (loop < 10)
	    printf("sd1=0x%04x,  sd2=0x%04x,  ad1=0x%04x,  ad2=0x%04x\n", sd1, sd2, ad1, ad2);
	  else if (loop == 10)
	    printf("........\n");  
	  fprintf(file,"%d\n",ad1);
	  fprintf(file,"%d\n",ad2);
        }
	
        fclose(file);
      }
      
      free((void *)rodata);
    }
        
    if (fun == 30)
    {    
      printf("===========================================\n");
      ret = U2F_Reg_Read(handle, O_ERRST, &rdata);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      } 

      ret = U2F_ERRST_decode(rdata, &errst_s);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
      printf("\nDecoding ERRST register\n");
      printf("Pattern error is : %s\n", errst_s.pattern_error ? "True" : "False");     
      printf("Abort is         : %s\n", errst_s.abort ? "True" : "False");
      printf("Time-out is      : %s\n", errst_s.timeout ? "True" : "False");
      printf("Altro error is   : %s\n", errst_s.altro_error ? "True" : "False");
      
      ret = U2F_Reg_Read(handle, O_TRCFG1, &rdata);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      } 

      ret = U2F_TRCFG1_decode(rdata, &trcfg_s);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
      
      printf("\nDecoding TRCFG1 register\n");
      printf("The system waits %d clock cycles after arrival of L1 trigger and issuing of L2 trigger\n", trcfg_s.tw);
      printf("Buffer mode                  : %s\n", trcfg_s.bmd ? "8 buffers" : "4 buffers");
      printf("Trigger mode                 : ");
      if (trcfg_s.mode == 0) printf("Software trigger\n");      
      if (trcfg_s.mode == 1) printf("Out of range\n");
      if (trcfg_s.mode == 2) printf("External L1 and automatic L2 after TW\n");
      if (trcfg_s.mode == 3) printf("External L1 and L2\n");
      printf("There are %d buffers remaining in the multi event buffer\n",trcfg_s.remb);
      printf("Multi event buffer empty     : %s\n",trcfg_s.empty ? "True" : "False");
      printf("Multi event buffer full      : %s\n",trcfg_s.full ? "True" : "False");
      printf("The read pointer position is : %d\n",trcfg_s.rd_pt); 
      printf("The write pointer position is: %d\n",trcfg_s.wr_pt); 
    
      ret = U2F_Reg_Read(handle, O_TRCNT, &rdata);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      } 
      
      ret = U2F_TRCNT_decode(rdata, &trcnt_s);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
      printf("\nDecoding TRCNT register\n");
      printf("Number of L1 triggers received: %d\n", trcnt_s.ntr); 
      printf("Number of L1 triggers accepted: %d\n", trcnt_s.nta); 

      ret = U2F_Reg_Read(handle, O_IRADD, &rdata);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      } 
      printf("\nThe IRADD register contains 0x%08x\n", rdata);

      ret = U2F_Reg_Read(handle, O_IRDAT, &rdata);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      } 
      printf("\nThe IRDAT register contains 0x%08x\n", rdata);
      printf("===========================================\n");
    }
  }
  return(0);
}





