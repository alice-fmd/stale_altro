#include <jni.h>
#include <stdio.h>
#include <tools/get_input.h>
#include <tools/rcc_time_stamp.h>

/***********************************/ 
/*Official functions of the library*/
/***********************************/ 
u_int Read_ALTRO_Reg(int handle,u_int fec_addr, u_int ch_addr, u_int reg_addr, u_int *reg_data);
u_int Write_ALTRO_Reg(int handle, u_int fec_addr, u_int ch_addr, u_int reg_addr, u_int reg_data) ;
u_int fecAddressToChannelAddressMapping(u_int fec_Add, u_int *ch_addr); 
