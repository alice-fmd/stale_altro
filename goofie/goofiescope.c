/****************************************************************/
/*                                                              */
/*  file: goofiescope.c                                         */
/*                                                              */
/* This program allows to access the resources of a GOOFIE      */
/* card in a user friendly way and includes some test routines  */
/*                                                              */
/*  Author: Markus Joos, CERN-EP                                */
/*                                                              */
/*  16. Nov. 04  MAJO  created                                  */
/*                                                              */
/****************C 2004 - A nickel program worth a dime**********/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>
#include <tools/get_input.h>
#include <tools/rcc_error.h>
#include "goofie.h"


/*************/
/* Prototypes*/
/*************/
int mainhelp(void);
int altromenu(void);
int setdebug(void);


/******************************/
int main(int argc, char *argv[])
/******************************/
{  
  static int ret, rsize = 1, rcode = 0, fun = 1, wfile = 0, cmd = 0;
  static u_int rdata = 0, channel = 1, offset = 0, fdata[2048] = {0}, fsize = 0;
  static char fname[200] = {0};
  static u_short memdata = 0;
  u_int osize, loop, dmemdata[S_DMEM * 2], dcount;
  
  ret = Goofie_Open(DEFAULT_NODE);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    exit(-1);
  }

  printf("\n\n\nThis is RUCSCOPE\n");
  while(fun != 0)
  {
    printf("\n");
    printf("Select an option:\n");    
    printf("   1 Goofie_CSR_Read	  2 Goofie_CSR_Write        \n");
    printf("   3 Goofie_PMEM_Read	  4 Goofie_PMEM_Write       \n");
    printf("   5 Goofie_DMEM_Read	  6 Goofie_EVLEN_Read       \n");
    printf("   7 Goofie_Exec_Command                                \n");
    printf("========================================================\n");
    printf("   9 File_Read		 10 File_Write              \n");
    printf("  11 Print help              12 Set debugging parameters\n");
    printf("  0 Quit\n");
    printf("Your choice ");
    fun = getdecd(fun);

    if (fun == 1)
    {    
      printf("Register | code\n");
      printf("=========|=====\n");
      printf("BSLCT    | %d\n", O_BSLCT);
      printf("TMASK    | %d\n", O_TMASK);
      printf("ACTCH    | %d\n", O_ACTCH);
      printf("ZSCTL    | %d\n", O_ZSCTL);
      printf("ZSTHD    | %d\n", O_ZSTHD);
      printf("ZSPED    | %d\n", O_ZSPED);
      printf("TATHD    | %d\n", O_TATHD);
      printf("TBTHD    | %d\n", O_TBTHD);
      printf("NSEVT    | %d\n", O_NSEVT);
      printf("TRGEV    | %d\n", O_TRGEV);
      printf("PRTRG    | %d\n", O_PRTRG);
      printf("Enter the code of the register you want to read: ");
      rcode = getdecd(rcode);      
    
      ret = Goofie_CSR_Read(rcode, &rdata);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      } 
      
      printf("The register contains 0x%08x\n", rdata);    
    }
    
    if (fun == 2)
    {    
      printf("Register | code\n");
      printf("=========|=====\n");
      printf("BSLCT    | %d\n", O_BSLCT);
      printf("TMASK    | %d\n", O_TMASK);
      printf("ACTCH    | %d\n", O_ACTCH);
      printf("ZSCTL    | %d\n", O_ZSCTL);
      printf("ZSTHD    | %d\n", O_ZSTHD);
      printf("ZSPED    | %d\n", O_ZSPED);
      printf("TATHD    | %d\n", O_TATHD);
      printf("TBTHD    | %d\n", O_TBTHD);
      printf("NSEVT    | %d\n", O_NSEVT);
      printf("PRTRG    | %d\n", O_PRTRG);
      printf("Enter the code of the register you want to modify: ");
      rcode = getdecd(rcode);      
      printf("Enter data that you want to write: 0x");
      rdata = gethexd(rdata);
  
      ret = Goofie_CSR_Write(rcode, rdata);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
    } 
    
    if (fun == 3) 
    {
      printf("Enter the channel number ( 1, 2 or 3 ) you want to read:");
      channel = getdecd(channel);
      printf("Enter the offset relative to the channel:");
      offset = getdecd(offset);
      	
      ret = Goofie_PMEM_Read(channel, offset, &memdata);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
      
      printf("The PMEM of channel %d, offset %d contains the data 0x%04x\n", channel, offset, memdata);
    } 
    
    if (fun == 4)
    {
      printf("Enter the channel number ( 1, 2 or 3 ) you want to write: ");
      channel = getdecd(channel);
      
      printf("Enter the offset relative to the channel");
      offset = getdecd(offset);
      
      printf("Enter data word: ");
      memdata = gethexd(memdata);

      ret = Goofie_PMEM_Write(channel, offset, memdata);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
    }      
  
    if (fun == 5)
    {    
      printf("Enter the channel number ( 1, 2 or 3 ) you want to read: ");
      channel = getdecd(channel);
	
      printf("How many words do you want to read: ");
      rsize = getdecd(rsize);

      ret = Goofie_DMEM_Read(channel, rsize, &osize, dmemdata);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
      printf("%d words received. Dumping data\n", osize);
      dcount = 0;
      for (loop = 0; loop < osize; loop++) 
      {
	int i1 = dcount;
	int i2 = dcount+1;
        printf("Word %d = 0x%02x %08x\n", loop, dmemdata[i1], dmemdata[i2]);
	dcount += 2;
      }
      
      /* write data to file */
      printf("Do you want to write this data to file (1=yes  0=no): ");
      wfile = getdecd(wfile);
      if (wfile)
      {
	printf("Enter the path and name of the file to write: ");
	getstrd(fname, fname);

	ret = Goofie_File_Write(fname, osize * 2, dmemdata);
	if (ret)
	{
          rcc_error_print(stdout, ret);
          return(-1);
	}
	printf("The file has been written with data from the internal array\n"); 
      }      
    }     

    if (fun == 6)
    {    
      printf("Enter the number of the channel (1, 2 or 3) you want to read: ");
      channel = getdecd(channel);      

      ret = Goofie_EVLEN_Read(channel, &memdata);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      } 
      
      printf("The event length of channel %d is %d\n", channel, memdata);    
    }     


    if (fun == 7)
    {    
      printf("==========|=====\n");
      printf("SWTRG     |  %d\n", I_SWTRG ); 
      printf("SWRST     |  %d\n", I_SWRST ); 
      printf("TWRST     |  %d\n", I_TWRST ); 
      printf("Enter the code of the command command:");
      cmd = getdecd(cmd);
      
      ret = Goofie_Exec_Command(cmd);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      } 
    }
    
    if (fun == 9)
    {    
      printf("Enter the path and name of the file to read: ");
      getstrd(fname, fname);
      printf("Enter the number of words to read: ");
      fsize = getdecd(fsize);

      ret = Goofie_File_Read(fname, fsize, fdata);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
      printf("The file has been read into the internal array\n"); 
    }     
 
    if (fun == 10)
    {    
      printf("Enter the path and name of the file to write: ");
      getstrd(fname, fname);
      printf("Enter the number of words to write: ");
      fsize = getdecd(fsize);

      ret = Goofie_File_Write(fname, fsize, fdata);
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
      printf("The file has been written with data from the internal array\n");       
    }     
    
    if (fun == 11) 
      mainhelp();

    if (fun == 12) 
      setdebug();
  }

  ret = Goofie_Close();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    exit(-1);
  }
  
  exit(0);
}


/****************/
int setdebug(void)
/****************/
{
  int tl, pid;
  
  printf("Package identifiers:\n");
  printf("ALTRO library: %d\n", P_ID_ALTRO);
  printf("Goofie library: %d\n", P_ID_GOOFIE);
  printf("\n");
  printf("Enter the debug package: ");
  pid = getdecd(P_ID_GOOFIE);
  printf("Enter the debug level: ");
  tl = getdecd(20);
  rcc_error_set_debug(pid, tl);
  return(0);
}


/****************/
int mainhelp(void)
/****************/
{
  printf("Call Markus Joos, 72364, 160663 if you need help\n");
  return(0);
}
