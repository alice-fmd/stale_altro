/*********************************************************************
 *
 * Prototypes 
 */
static void rcu_irq(struct urb *urb);
static void rcu_write_irq(struct urb *urb);
static void rcu_read_irq(struct urb *urb);

/*********************************************************************
 * 
 * Handler for data sent in by the device. The function is called by
 * the USB kernel subsystem whenever a device spits out new data 
 */
static void rcu_irq(struct urb *urb)
{
  struct rcu_device *rcu = urb->context;
  char *pos = rcu->output;
  int i;
  
  kdebug(("rcu_drv(rcu_irq): function called\n"));
  
  if (urb->status != USB_ST_NOERROR) { 
    kdebug(("rcu_drv(rcu_irq): ERROR urb->status != USB_ST_NOERROR\n"));
    return;
  }

  pos += sprintf(pos, "rcu: data from %-8s =", rcu->name);
  for (i=0; i<rcu->maxp; i++) 
    pos += sprintf(pos, " %02x", rcu->data[i]);
  
  kdebug(("rcu_drv(rcu_irq): %s\n", rcu->output));
}


/*********************************************************************
 * 
 * Handler for error checking after data das been sent out to the device 
 */
static void rcu_write_irq(struct urb *urb)
{
  struct rcu_device *rcu = (struct rcu_device *)urb->context;

  kdebug(("rcu_drv(rcu_write_irq): function called\n"));

  //usb-skeleton: if ((urb->status != -ENOENT) && (urb->status != -ECONNRESET)) 
  if (urb->status) {
    kdebug(("rcu_drv(rcu_write_irq): nonzero write bulk status received: %d\n",  urb->status));
    return;
  }

  urbdone = 1;
  kdebug(("rcu_drv(rcu_write_irq): function done\n"));
  return;
}


/*********************************************************************
 * 
 * Handler for error checking after data das been sent out to the
 * device 
 */
static void rcu_read_irq(struct urb *urb)
{
  struct rcu_device *rcu = (struct rcu_device *)urb->context;

  kdebug(("rcu_drv(rcu_read_irq): function called\n"));

  if (urb->status) {
    kdebug(("rcu_drv(rcu_read_irq): nonzero write bulk status received: %d\n",  urb->status));
    return;
  }

  urbdone = 1;
  kdebug(("rcu_drv(rcu_read_irq): function done\n"));
  return;
}

/*********************************************************************
 * 
 * These two callbacks are invoked when an USB device is detached or
 * attached to the bus
 */ 
static void *rcu_probe(struct usb_device *udev, unsigned int ifnum, 
		       const struct usb_device_id *id)
{
  /* The probe procedure is pretty standard. Device matching has already */
  /* been performed based on the id_table structure */

  struct usb_interface *iface;
  struct usb_interface_descriptor *interface;
  struct usb_endpoint_descriptor *endpoint;
  struct rcu_device *rcu;
  char name[10];
  int ecode, i, buffer_size, inok, outok;

  kdebug(("rcu_drv(rcu_probe): probe called for %s device\n", (char *)id->driver_info));
  kdebug(("rcu_drv(rcu_probe): ifnum = %d\n", ifnum));

  /* allocate and zero a new data structure for the new device */
  rcu = kmalloc(sizeof(struct rcu_device), GFP_KERNEL);
  if (!rcu) { 
    kdebug(("rcu_drv(rcu_probe): ERROR failed to kmalloc rcu_device\n"));
    return NULL; /* failure */
  }
  
  memset(rcu, 0, sizeof(*rcu));
  rcu->name = (char *)id->driver_info;
  init_MUTEX(&rcu->sem);

  iface = &udev->actconfig->interface[ifnum];
  interface = &iface->altsetting[iface->act_altsetting];

  if (interface->bNumEndpoints != 2) {
    kdebug(("rcu_drv(rcu_probe): ERROR interface->bNumEndpoints "
	    "!= 3 (number = %d)\n", interface->bNumEndpoints));
    return NULL;
  }
  
  inok = 0;
  outok = 0;
    
  for (i = 0; i < interface->bNumEndpoints; ++i) {
    kdebug(("rcu_drv(rcu_probe): Processing endpoint %d ...\n", i));
    endpoint = &interface->endpoint[i];
    
    if ((endpoint->bEndpointAddress & 0x80) && 
	((endpoint->bmAttributes & 3) == 0x02)) {
      if (!inok) {
        kdebug(("rcu_drv(rcu_probe): we found the first bulk in endpoint\n"));
	buffer_size = endpoint->wMaxPacketSize;
	kdebug(("rcu_drv(rcu_probe): buffer_size = %d\n", buffer_size));
	rcu->bulk_in_size = buffer_size;
	rcu->bulk_in_endpointAddr = endpoint->bEndpointAddress;
	kdebug(("rcu_drv(rcu_probe): bulk_in_endpointAddr = %d\n", rcu->bulk_in_endpointAddr));
	rcu->bulk_in_buffer = kmalloc(buffer_size, GFP_KERNEL);
	if (!rcu->bulk_in_buffer) {
          kdebug(("rcu_drv(rcu_probe): Couldn't allocate bulk_in_buffer"));
          return NULL;
	}
	inok = 1;
      }
      else
        kdebug(("rcu_drv(rcu_probe): we found another bulk in endpoint\n"));
    }
    
    else if (((endpoint->bEndpointAddress & 0x80) == 0x00) && 
	     ((endpoint->bmAttributes & 3) == 0x02)) {
      if (!outok) {
	kdebug(("rcu_drv(rcu_probe): we found the first bulk out endpoint\n"));
	rcu->write_urb = usb_alloc_urb(0);
	if (!rcu->write_urb) {
          kdebug(("rcu_drv(rcu_probe): No free urbs available"));
          return NULL;
	}
	buffer_size = endpoint->wMaxPacketSize;
	kdebug(("rcu_drv(rcu_probe): buffer_size = %d\n", buffer_size));
	rcu->bulk_out_size = buffer_size;
	rcu->bulk_out_endpointAddr = endpoint->bEndpointAddress;
	kdebug(("rcu_drv(rcu_probe): bulk_out_endpointAddr = %d\n", rcu->bulk_out_endpointAddr));
	rcu->bulk_out_buffer = kmalloc (buffer_size, GFP_KERNEL);
	if (!rcu->bulk_out_buffer) {
          kdebug(("rcu_drv(rcu_probe): Couldn't allocate bulk_out_buffer"));
          return NULL;
	}
	outok = 1;
      }	

      else
        kdebug(("rcu_drv(rcu_probe): we found another bulk out endpoint\n"));
    }
    else
      {
	kdebug(("rcu_drv(rcu_probe): we found an unidentifiable endpoint\n"));
	return NULL;
    }
  }
  
  kdebug(("rcu_drv(rcu_probe): Setting protocol\n"));
  usb_set_protocol(udev, interface->bInterfaceNumber, 0);
  kdebug(("rcu_drv(rcu_probe): Setting idle\n"));
  usb_set_idle(udev, interface->bInterfaceNumber, 0, 0);
  
  kdebug(("rcu_drv(rcu_probe): Preparing URB\n"));
  /* fill the URB data structure using the FILL_INT_URB macro */
  {
    int pipe = usb_rcvintpipe(udev, endpoint->bEndpointAddress);
    int maxp = usb_maxpacket(udev, pipe, usb_pipeout(pipe));
    kdebug(("rcu_drv(rcu_probe): pipe = %d\n", pipe));
    kdebug(("rcu_drv(rcu_probe): maxp = %d\n", maxp));
    
    if (maxp > 512) maxp = 512; 
    rcu->maxp = maxp; /* remember for later */
    kdebug(("rcu_drv(rcu_probe): maxp(2) = %d\n", maxp));
    kdebug(("rcu_drv(rcu_probe): endpoint->bInterval = %d\n", endpoint->bInterval));
    FILL_INT_URB(&rcu->urb, udev, pipe, rcu->data, maxp, rcu_irq, rcu, endpoint->bInterval);
  }
  
  kdebug(("rcu_drv(rcu_probe): Registering URB\n"));
  /* register the URB within the USB subsystem */
  ecode = usb_submit_urb(&rcu->urb);
  if (ecode) {
    kfree(rcu);
    kdebug(("rcu_drv(rcu_probe): ERROR  failed to register URB (ecode = %d)\n", ecode));
    return NULL;
  }
  kdebug(("rcu_drv(rcu_probe): Probe successful for %s (maxp is %i)\n", rcu->name, rcu->maxp));
  
  rcu->udev = udev;


  //  This code is from the usb-skeleton.c template. It seems however,
  //  that CERN Linux kernels do not offer the CONFIG_DEVFS_FS
  //  option. Therefore we cannot use it

  /* initialize the devfs node for this device and register it */
  sprintf(name, "rcu");
  /* MJ: So far we can only support one RCU card. For multiple cards
     the node name has to have an occurence number */
  kdebug(("rcu_drv(rcu_probe): calling devfs_register\n"));
  rcu->devfs = devfs_register (usb_devfs_handle, name, 
			       DEVFS_FL_DEFAULT, USB_MAJOR, 
			       RCU_MINOR_BASE + rcu->minor,
			       S_IFCHR | S_IRUSR | S_IWUSR | 
			       S_IRGRP | S_IWGRP | S_IROTH, 
			       &rcu_fops, NULL);
  kdebug(("rcu_drv(rcu_probe): devfs_register done\n"));
  /* let the user know what node this device is now attached to */
  kdebug(("rcu_drv(rcu_probe): RCU device now attached to "
	  "major number=%d, minor number=%d\n", 
	  USB_MAJOR, RCU_MINOR_BASE + rcu->minor));

  rcu->test = 1; 
  rcup = rcu;
  kdebug(("rcu_drv(rcu_probe): End of function\n"));
  
  return rcu;
}

/*********************************************************************
 * 
 */ 
static int rcu_ioctl (struct inode *inode, struct file *file, 
		      unsigned int cmd, unsigned long arg)
{
  struct rcu_device *rcu;
  int pipe, maxp, retval;
  
  kdebug(("rcu_drv(rcu_ioctl): function called\n")); 
  kdebug(("rcu_drv(rcu_ioctl): minor number = %d\n", MINOR (inode->i_rdev)));

  rcu = (struct rcu_device *)file->private_data;
  kdebug(("rcu_drv(rcu_ioctl): rcu->test = %d\n", rcu->test)); 

  /* lock this object */
  down (&rcu->sem);
  kdebug(("rcu_drv(rcu_ioctl): semaphore locked\n")); 

  /* verify that the device wasn't unplugged */
  if (rcu->udev == NULL) 
  {
    up (&rcu->sem);
    kdebug(("rcu_drv(rcu_ioctl): ERROR device was unplugged\n"));
    return -ENODEV;
  }
  kdebug(("rcu_drv(rcu_ioctl): The device is still connected\n")); 

  switch (cmd)
  {    
  case SEND_DATA: {
    rcu_bulk_out_t params;
    
    kdebug(("rcu_drv(ioctl:SEND_DATA): start of function\n"));
    kdebug(("rcu_drv(ioctl:SEND_DATA): function called for endpoint "
	    "0x%08x\n", rcu->bulk_out_endpointAddr));
    
     /* see if we are already in the middle of a write */
     if (rcu->write_urb->status == -EINPROGRESS) {
       kdebug(("rcu_drv(ioctl:SEND_DATA): the BULK out URB is in use\n"));
       up (&rcu->sem);
       return(-RCU_EFAULT);
     }

     if (copy_from_user(&params, (void *)arg, 
			sizeof(rcu_bulk_out_t)) !=0 ) {
       kdebug(("rcu_drv(ioctl:SEND_DATA): error from " "copy_from_user\n"));
       up (&rcu->sem);
       return(-RCU_EFAULT);
     }

     /* set up our urb */
     pipe = usb_sndbulkpipe(rcu->udev, rcu->bulk_out_endpointAddr);
     maxp = usb_maxpacket(rcu->udev, pipe, usb_pipeout(pipe));
     
     if (maxp > rcu->bulk_out_size) maxp = rcu->bulk_out_size; 
     rcu->maxp = maxp; /* remember for later */
     kdebug(("rcu_drv(ioctl:SEND_DATA): it is possible to send %d bytes\n", 
	     maxp));
    
     /* check if we would write more data than possible */
     if (params.nbytes > maxp) {	
       kdebug(("rcu_drv(ioctl:SEND_DATA): Cannot write %d bytes to "
	       "pipe of %d bytes\n", params.nbytes, maxp));
       up (&rcu->sem);
       return(-RCU_EFAULT);
     }
     kdebug(("rcu_drv(ioctl:SEND_DATA): trying to send %d bytes\n", 
	     params.nbytes));
     kdebug(("rcu_drv(ioctl:SEND_DATA): dumping first four bytes\n"));
     kdebug(("rcu_drv(ioctl:SEND_DATA): 1. byte = 0x%02x\n", params.data[0]));
     kdebug(("rcu_drv(ioctl:SEND_DATA): 2. byte = 0x%02x\n", params.data[1]));
     kdebug(("rcu_drv(ioctl:SEND_DATA): 3. byte = 0x%02x\n", params.data[2]));
     kdebug(("rcu_drv(ioctl:SEND_DATA): 4. byte = 0x%02x\n", params.data[3]));
     
     usb_fill_bulk_urb(&rcu->urb, rcu->udev, pipe, &params.data[0], 
		       params.nbytes, rcu_write_irq, rcu);
     kdebug(("rcu_drv(ioctl:SEND_DATA): URB has beed filled\n"));
     
     /* register the URB within the USB subsystem */
     retval = usb_submit_urb(&rcu->urb); 
     if (retval) {
       kdebug(("rcu_drv(ioctl:SEND_DATA): ERROR  failed to register URB. "
	       "Error code = %d\n", retval));
       up (&rcu->sem);
       return(-RCU_EFAULT);
     }
     kdebug(("rcu_drv(ioctl:SEND_DATA): URB has beed submitted\n"));
     break;
  }
  case GET_DATA: {   
    rcu_bulk_in_t params;
    
    kdebug(("rcu_drv(ioctl:GET_DATA): start of function\n"));
    kdebug(("rcu_drv(ioctl:GET_DATA): function called for endpoint "
	    "0x%08x\n", rcu->bulk_in_endpointAddr));
    
    /* see if we are already in the middle of a write */
    if (rcu->read_urb->status == -EINPROGRESS)  {
      kdebug(("rcu_drv(ioctl:GET_DATA): the BULK in URB is in use\n"));
      up (&rcu->sem); 
      return(-RCU_EFAULT);
    }

    /* set up our urb */
    pipe = usb_rcvbulkpipe(rcu->udev, rcu->bulk_in_endpointAddr);
    maxp = usb_maxpacket(rcu->udev, pipe, usb_pipeout(pipe));
    
    if (maxp > rcu->bulk_in_size) 
      maxp = rcu->bulk_in_size; 
    rcu->maxp = maxp; /* remember for later */
    kdebug(("rcu_drv(ioctl:GET_DATA): it is possible to receive %d bytes\n", 
	    maxp));

    usb_fill_bulk_urb(&rcu->urb, rcu->udev, pipe, &params.data[0], 
		      BULK_IN_SIZE, rcu_read_irq, rcu);
    kdebug(("rcu_drv(ioctl:GET_DATA): URB has beed filled\n"));
     
    /* register the URB within the USB subsystem */
    retval = usb_submit_urb(&rcu->urb); 
    if (retval) {
      kdebug(("rcu_drv(ioctl:GET_DATA): ERROR  failed to register "
	      "URB. Error code = %d\n", retval));
      params.nbytes = 0;
      up (&rcu->sem);
      return(-RCU_EFAULT);
    }
    kdebug(("rcu_drv(ioctl:GET_DATA): URB has beed submitted\n"));

    /// MJ: How does one best detect the end of a transfer?
    kdebug(("rcu_drv(ioctl:GET_DATA): URB has beed completed\n"));
    if (copy_to_user ((void *)arg, &params, 
		      sizeof(rcu_bulk_in_t)) != 0)  {
       kdebug(("rcu_drv(ioctl:GET_DATA): error from copy_to_user\n"));
       up (&rcu->sem);
       return(-RCU_EFAULT);
    }
    kdebug(("rcu_drv(ioctl:GET_DATA): data has been copied\n"));
    
    break;          
  }    
  case CONTROL: {
    rcu_control_t params;
    
    kdebug(("rcu_drv(ioctl:CONTROL): function called\n"));
    
    if (copy_from_user(&params, (void *)arg, sizeof(rcu_control_t)) !=0 )  {
      kdebug(("rcu_drv(ioctl:CONTROL): error from copy_from_user\n"));
      up (&rcu->sem);
      return(-RCU_EFAULT);
    }

    kdebug(("rcu_drv(ioctl:CONTROL): params.bRequest      = %d\n", 
	    params.bRequest));
    kdebug(("rcu_drv(ioctl:CONTROL): params.bmRequestType = %d\n", 
	    params.bmRequestType));
    kdebug(("rcu_drv(ioctl:CONTROL): params.wValue        = %d\n", 
	    params.wValue));
    kdebug(("rcu_drv(ioctl:CONTROL): params.wIndex        = %d\n", 
	    params.wIndex));
    kdebug(("rcu_drv(ioctl:CONTROL): params.data[0]       = %d\n", 
	    params.data[0]));
    kdebug(("rcu_drv(ioctl:CONTROL): params.wLength       = %d\n", 
	    params.wLength));
    
    retval = usb_control_msg(rcu->udev, usb_rcvctrlpipe(rcu->udev,0), 
			     params.bRequest, 
			     params.bmRequestType, 
			     params.wValue, 
			     params.wIndex, 
			     &params.data[0], 
			     params.wLength, HZ);
    if (retval) {
      kdebug(("rcu_drv(ioctl:CONTROL): ERROR  failed to register control URB. "
	      "Error code = %d\n", retval));
      up (&rcu->sem);
      return(-RCU_EFAULT);
    }
    break;
  }
  }
  
  /* unlock the device */
  up (&rcu->sem);
  return(0);
}
