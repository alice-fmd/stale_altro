package core.modules.fec.bcontrol;

import javax.swing.JButton;
import javax.swing.JTextField;

import core.global.GlobalVariables;
import core.jni.fec.BCReadWrite;
import core.modules.fec.altro.Altro;
import core.utilities.SwingWorker;

public class BCRegisterManager 
{
	private int fecAdd;
	
	private BCRegister[] register, writeOnlyRegister;
	private JTextField[] registerGuiField,registerGuiField1,registerGuiField2;
	private JButton[]    registerButtons;
	private BCReadWrite  bcReadWriteObj;
	
	public BCRegisterManager(int fec)
	{
		setFecAdd(fec);
	}
	
	public void setBCHealthMonComponents(JTextField[] textArray, JButton[] buttonArray)
	{
		registerGuiField = textArray;
		registerButtons  = buttonArray;
	}
        
        public void setBCOtherRegComponents(JTextField[] textArray)
	{
		registerGuiField1 = textArray;
	}
        
        public void setBCConfigStatusComponents(JTextField[] textArray)
	{
		registerGuiField2 = textArray;
	}
     
	public void initComponents()
	{
		/* Create an instance of the BC ReadWrite Object so that BC registers could be accessed */
		bcReadWriteObj = new BCReadWrite();
		
		int index=0;
		
		register = new BCRegister[BoardControl.MaxBCRegisters];
		
                /* Here 10 Health Monitoring registers are added. */
		register[index] = new BCRegister(BoardControl.T_TH, BoardControl.T_TH_Access,BoardControl.T_TH_Length,registerGuiField[index]);
		index++;
		register[index] = new BCRegister(BoardControl.AV_TH,BoardControl.AV_TH_Access,BoardControl.AV_TH_Length,registerGuiField[index]);
		index++;
		register[index] = new BCRegister(BoardControl.AC_TH,BoardControl.AC_TH_Access,BoardControl.AC_TH_Length,registerGuiField[index]);
		index++;
		register[index] = new BCRegister(BoardControl.DV_TH,BoardControl.DV_TH_Access,BoardControl.DV_TH_Length,registerGuiField[index]);
		index++;
		register[index] = new BCRegister(BoardControl.DC_TH,BoardControl.DC_TH_Access,BoardControl.DC_TH_Length,registerGuiField[index]);
		index++;
		register[index] = new BCRegister(BoardControl.TEMP,BoardControl.TEMP_Access,BoardControl.TEMP_Length,registerGuiField[index]);
		index++;
		register[index] = new BCRegister(BoardControl.AV,BoardControl.AV_Access,BoardControl.AV_Length,registerGuiField[index]);
		index++;
		register[index] = new BCRegister(BoardControl.AC,BoardControl.AC_Access,BoardControl.AC_Length,registerGuiField[index]);
		index++;
		register[index] = new BCRegister(BoardControl.DV,BoardControl.DV_Access,BoardControl.DV_Length,registerGuiField[index]);
		index++;
		register[index] = new BCRegister(BoardControl.DC,BoardControl.DC_Access,BoardControl.DC_Length,registerGuiField[index]); 
                
         /* Here are the rest of registers added */
        index++;
        register[index] = new BCRegister(BoardControl.L1CNT, BoardControl.L1CNT_Access,BoardControl.L1CNT_Length,registerGuiField1[0]);
        index++;
        register[index] = new BCRegister(BoardControl.L2CNT, BoardControl.L2CNT_Access,BoardControl.L2CNT_Length,registerGuiField1[1]);
        index++;
        register[index] = new BCRegister(BoardControl.SCLKCNT, BoardControl.SCLKCNT_Access,BoardControl.SCLKCNT_Length,registerGuiField1[2]);
        index++;
        register[index] = new BCRegister(BoardControl.DSTBCNT, BoardControl.DSTBCNT_Access,BoardControl.DSTBCNT_Length,registerGuiField1[3]);
        index++;
        register[index] = new BCRegister(BoardControl.TSMWORD, BoardControl.TSMWORD_Access,BoardControl.TSMWORD_Length,registerGuiField1[4]);
        index++;
        register[index] = new BCRegister(BoardControl.USRATIO, BoardControl.USRATIO_Access,BoardControl.USRATIO_Length,registerGuiField1[5]);
        
        /* Here the configuration registers are added */
        index++;
        register[index] = new BCRegister(BoardControl.CSR0, BoardControl.CSR0_Access,BoardControl.CSR0_Length,registerGuiField2[0]);
        index++;
        register[index] = new BCRegister(BoardControl.CSR1, BoardControl.CSR1_Access,BoardControl.CSR1_Length,registerGuiField2[1]);
        index++;
        register[index] = new BCRegister(BoardControl.CSR2, BoardControl.CSR2_Access,BoardControl.CSR2_Length,registerGuiField2[2]);
        index++;
        register[index] = new BCRegister(BoardControl.CSR3, BoardControl.CSR3_Access,BoardControl.CSR3_Length,registerGuiField2[3]);
       
        /* Here goes the code for initalisation of the write only BC registers to set counters, reset Altro's */
        writeOnlyRegister = new BCRegister[BoardControl.MaxMiscBCRegisters];
        
        /* As this is a new array, so index counter value is reset */
        index=0;

        writeOnlyRegister[index] = new BCRegister(BoardControl.CNTLAT, BoardControl.CNTLAT_Access,BoardControl.CNTLAT_Length);
        index++;
        writeOnlyRegister[index] = new BCRegister(BoardControl.CNTCLR, BoardControl.CNTCLR_Access,BoardControl.CNTCLR_Length);
        index++;
        writeOnlyRegister[index] = new BCRegister(BoardControl.CSR1CLR, BoardControl.CSR1CLR_Access,BoardControl.CSR1CLR_Length);
        index++;
        writeOnlyRegister[index] = new BCRegister(BoardControl.ALRST, BoardControl.ALRST_Access,BoardControl.ALRST_Length);
        index++;
        writeOnlyRegister[index] = new BCRegister(BoardControl.BCRST, BoardControl.BCRST_Access,BoardControl.BCRST_Length);
        index++;
        writeOnlyRegister[index] = new BCRegister(BoardControl.STCNV, BoardControl.STCNV_Access,BoardControl.STCNV_Length);
	}
	
	public void setFecAdd(int fec)
	{
		fecAdd = fec;
	}
	
	public int getFecAdd()
	{
		return fecAdd;
	}
	
	public void writeToWriteOnlyBCRegisters(int index)
	{
		/* This set the global parameters */
		setBCRegReadWriteMiscParameters(index);
		
		/* Foe debugging purposes */
		System.out.println("Fec:BCManager:writeToWriteOnlyBCRegisters -> Index value= "+ index );
		/* Write to specific BC register */
		/* SwingWorker is a generic thread, which executes the given function in a seperate thread */
		SwingWorker worker = new SwingWorker() 
		 {
			public Object construct() 
				{	
					/* Read the registers */
					bcReadWriteObj.write_BC_Reg();
					
					return null;
				}
		 };
		
		 worker.start();
		
		/* Re-read BC registers again to update the gui */
		readHealthMonRegisters();
	}
	
	public void writeToBCRegister(int index)
	{
		/* This set the global parameters */
		setBCRegReadWriteParameters(index);
		
		/* Foe debugging purposes */
		System.out.println("Fec:BCManager:writeToBCRegister -> Index value= "+ index );
		/* Write to specific BC register */
		/* SwingWorker is a generic thread, which executes the given function in a seperate thread */
		SwingWorker worker = new SwingWorker() 
		 {
			public Object construct() 
				{	
					/* Read the registers */
					bcReadWriteObj.write_BC_Reg();
					
					return null;
				}
		 };
		
		 worker.start();
		
		/* Re-read BC registers again to update the gui */
		readHealthMonRegisters();
	}
	
	public void readHealthMonRegisters()
	{
		/* SwingWorker is a generic thread, which executes the given function in a seperate thread */
		SwingWorker worker = new SwingWorker() 
		 {
			public Object construct() 
				{	
				
					/* This set the global parameters */
					setAllBCRegReadWriteParameters();
				    
					/* Copy the register addresses from Register objects into Global register address array */
					for(int index=0; index < register.length; index++)
					{
						GlobalVariables.RegAddrArray[index] = register[index].getAddress();
					}
				    
					/* Read the registers */
					bcReadWriteObj.read_All_BC_Reg();
					
					/* Update the values into the respective register text fields. */
					for(int index=0; index < register.length; index++)
					{
						register[index].setValue(GlobalVariables.RegDataArray[index]);
					}
					
					return null;
				}
		 };
		 
		 worker.start();
   }
	
	private void setBCRegReadWriteMiscParameters(int index)
    {
    	/* Set the Global Parameters */
    	GlobalVariables.FecAddr = fecAdd;
    	
    	/* Set the address of register to which dummy data would be written */
    	GlobalVariables.RegAddr = writeOnlyRegister[index].getAddress();
    	
    	/* Write just any thing so that write line is asserted on the Altro */
    	GlobalVariables.RegData = Integer.decode("0x1").intValue();
    	
    }
	
	private void setBCRegReadWriteParameters(int index)
    {
    	/* Set the Global Parameters */
    	GlobalVariables.FecAddr = fecAdd;
    	
    	GlobalVariables.RegAddr= register[index].getAddress();

    	/* Write just any thing so that write line is asserted on the Altro */
    	GlobalVariables.RegData = register[index].getValueToWriteToHardware();
    }
	
	private void setAllBCRegReadWriteParameters()
    {
    	/* Set the Global Parameters */
    	GlobalVariables.FecAddr = fecAdd;
    	
    	GlobalVariables.RegAddrArray = new int[register.length];
    	
    	GlobalVariables.RegLoopCount= GlobalVariables.RegAddrArray.length;
    }
	
}
