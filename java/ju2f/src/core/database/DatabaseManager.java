/**
 * 
 */
package core.database;

/**
 * @author okhalid
 *
 */
public class DatabaseManager implements Runnable{

	private ReadDataFromDatabase dataFromDB; 
	
	public DatabaseManager()
	{
		dataFromDB = new ReadDataFromDatabase();
	}
	
	public void run()
	{
		dataFromDB.getConfiguration();
	}
}
