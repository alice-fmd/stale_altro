//____________________________________________________________________
/* local function prototypes */
static ssize_t alice_rcu_read(struct file *file, char *buffer, 
			      size_t count, loff_t *ppos);
static ssize_t alice_rcu_write(struct file *file, const char *buffer, 
			       size_t count, loff_t *ppos);
static void alice_rcu_write_bulk_callback(struct urb *urb, 
					  struct pt_regs *regs);


//____________________________________________________________________
/*
 * File operations needed when we register this driver.
 * This assumes that this driver NEEDS file operations,
 * of course, which means that the driver is expected
 * to have a node in the /dev directory. If the USB
 * device were for a network interface then the driver
 * would use "struct net_driver" instead, and a serial
 * device would use "struct tty_driver".
 */
static struct file_operations alice_rcu_fops = {
  /*
   * The owner field is part of the module-locking
   * mechanism. The idea is that the kernel knows
   * which module to increment the use-counter of
   * BEFORE it calls the device's open() function.
   * This also means that the kernel can decrement
   * the use-counter again before calling release()
   * or should the open() function fail.
   */
  .read =	alice_rcu_read,
  .write =	alice_rcu_write,
};

//____________________________________________________________________
/**
 * usb_alice_rcu_debug_data
 */
static inline void 
usb_alice_rcu_debug_data(const char *function, int size, 
			 const unsigned char *data)
{
  int i;
  if (!debug) return;
  printk(KERN_DEBUG __FILE__": %s - length = %d, data = ", function, size);
  for (i = 0; i < size; ++i) printk ("%.2x ", data[i]);
  printk ("\n");
}


//____________________________________________________________________
/**
 * alice_rcu_read
 */
static ssize_t 
alice_rcu_read(struct file *file, char *buffer, size_t count, loff_t *ppos)
{
  struct usb_alice_rcu *dev;
  int retval = 0;

  dev = (struct usb_alice_rcu *)file->private_data;
  kdebug(("minor %d, count = %d", dev->minor, count));

  /* lock this object */
  down (&dev->sem);

  /* verify that the device wasn't unplugged */
  if (!dev->present) {
    kdebug(("Error: Device was unplugged!\n"));
    up (&dev->sem);
    return -ENODEV;
  }

  /* do a blocking bulk read to get data from the device */
  retval = usb_bulk_msg(dev->udev,
			usb_rcvbulkpipe (dev->udev, dev->bulk_in_endpointAddr),
			dev->bulk_in_buffer,
			min (dev->bulk_in_size, count),
			&count, HZ*10);

  /* if the read was successful, copy the data to userspace */
  if (!retval) {
    if (copy_to_user (buffer, dev->bulk_in_buffer, count))
      retval = -EFAULT;
    else
      retval = count;
  }

  /* unlock the device */
  up (&dev->sem);
  return retval;
}


//____________________________________________________________________
/**
 *	alice_rcu_write
 *
 *	A device driver has to decide how to report I/O errors back to
 *	the user.  The safest course is to wait for the transfer to
 *	finish before returning so that any errors will be reported
 *	reliably.  alice_rcu_read() works like this.  But waiting for
 *	I/O is slow, so many drivers only check for errors during I/O
 *	initiation and do not report problems that occur during the
 *	actual transfer.  That's what we will do here.
 *
 *	A driver concerned with maximum I/O throughput would use
 *	double- buffering: Two urbs would be devoted to write
 *	transfers, so that one urb could always be active while the
 *	other was waiting for the user to send more data.
 */
static ssize_t 
alice_rcu_write(struct file *file, const char *buffer, 
		size_t count, loff_t *ppos)
{
  struct usb_alice_rcu *dev;
  ssize_t bytes_written = 0;
  int retval = 0;

  dev = (struct usb_alice_rcu *)file->private_data;
  kdebug(("minor %d, count = %d\n", dev->minor, count));

  /* lock this object */
  down (&dev->sem);

  /* verify that the device wasn't unplugged */
  if (!dev->present) {
    kdebug(("Error: Device was unplugged!\n"));
    retval = -ENODEV;
    goto exit;
  }

  /* verify that we actually have some data to write */
  if (count == 0) {
    kdebug(("write request of 0 bytes\n"));
    goto exit;
  }

  /* wait for a previous write to finish up; we don't use a timeout
   * and so a nonresponsive device can delay us indefinitely.
   */
  if (atomic_read (&dev->write_busy))
    wait_for_completion (&dev->write_finished);

  /* we can only write as much as our buffer will hold */
  bytes_written = min (dev->bulk_out_size, count);

  /* copy the data from userspace into our transfer buffer;
   * this is the only copy required.
   */
  if (copy_from_user(dev->write_urb->transfer_buffer, buffer,
		     bytes_written)) {
    retval = -EFAULT;
    goto exit;
  }

  usb_alice_rcu_debug_data (__FUNCTION__, bytes_written,
			    dev->write_urb->transfer_buffer);

  /* this urb was already set up, except for this write size */
  dev->write_urb->transfer_buffer_length = bytes_written;

  /* send the data out the bulk port */
  /* a character device write uses GFP_KERNEL,
     unless a spinlock is held */
  init_completion (&dev->write_finished);
  atomic_set (&dev->write_busy, 1);
  retval = usb_submit_urb(dev->write_urb, GFP_KERNEL);
  if (retval) {
    atomic_set (&dev->write_busy, 0);
    err("%s - failed submitting write urb, error %d",
	__FUNCTION__, retval);
  } else {
    retval = bytes_written;
  }

 exit:
  /* unlock the device */
  up (&dev->sem);

  return retval;
}

//____________________________________________________________________
/**
 *	alice_rcu_write_bulk_callback
 */
static void 
alice_rcu_write_bulk_callback (struct urb *urb, struct pt_regs *regs)
{
  struct usb_alice_rcu *dev = (struct usb_alice_rcu *)urb->context;

  kdebug(("minor %d\n", dev->minor));

  /* sync/async unlink faults aren't errors */
  if (urb->status && !(urb->status == -ENOENT ||
		       urb->status == -ECONNRESET)) {
    kdebug(("nonzero write bulk status received: %d", urb->status));
  }
  
  /* notify anyone waiting that the write has finished */
  atomic_set (&dev->write_busy, 0);
  complete (&dev->write_finished);
}

//____________________________________________________________________
/** Handler for data sent in by the device. The function is called by  
    the USB kernel subsystem whenever a device spits out new data
    @param urb 
*/
static void rcu_irq(struct urb *urb)
{
  struct rcu_device *rcu = urb->context;
  char *pos = rcu->output;
  int i;
  
  kdebug(("rcu_drv(rcu_irq): function called\n"));

  if (urb->status != USB_ST_NOERROR) { 
    kdebug(("rcu_drv(rcu_irq): ERROR urb->status != USB_ST_NOERROR\n"));
    return;
  }

  pos += sprintf(pos, "rcu: data from %-8s =", rcu->name);
  for (i=0; i<rcu->maxp; i++) 
    pos += sprintf(pos, " %02x", rcu->data[i]);
  
  kdebug(("rcu_drv(rcu_irq): %s\n", rcu->output));
}

//____________________________________________________________________
/** Handler for error checking after data das been sent out to the
    device  
    @param urb 
*/
static void rcu_write_irq(struct urb *urb)
{
  struct rcu_device *rcu = (struct rcu_device *)urb->context;

  kdebug(("rcu_drv(rcu_write_irq): function called\n"));

  //usb-skeleton: 
  //   if ((urb->status != -ENOENT) && (urb->status != -ECONNRESET)) 
  if (urb->status) {
    kdebug(("rcu_drv(rcu_write_irq): nonzero write bulk status received: %d\n",
	    urb->status));
    return;
  }
  
  urbdone = 1;
  kdebug(("rcu_drv(rcu_write_irq): function done\n"));
  return;
}


//____________________________________________________________________
/** Handler for error checking after data das been sent out to the
    device  
    @param urb  */
static void rcu_read_irq(struct urb *urb)
{
  struct rcu_device *rcu = (struct rcu_device *)urb->context;
  
  kdebug(("rcu_drv(rcu_read_irq): function called\n"));
  
  if (urb->status) {
    kdebug(("rcu_drv(rcu_read_irq): nonzero write bulk status received: %d\n",
	    urb->status));
    return;
  }
  
  urbdone = 1;
  kdebug(("rcu_drv(rcu_read_irq): function done\n"));
  return;
}
