/************************************************************************/
/*									*/
/*  This is the header file for the ALTRO  library			*/
/*									*/
/*  7. Sep. 04  MAJO  created						*/
/*									*/
/*******C 2004 - The software with that certain something****************/ 

#ifndef _ALTRO_H
#define _ALTRO_H

#include <stdio.h>
#include <altro/altro_common.h>

#define ALTROISOPEN {if(!altro_is_open) return(ALTRO_NOTOPEN);}
#define DEFAULT_NODE "/dev/usb/altro"
#define BASE_NODE    "/dev/usb/altro"

#ifdef ALTRODEBUG
#define DEBUG_TEXT(my_package, level, text)\
  {\
    if ((my_package == packageId) || (packageId == 0))\
      if (traceLevel >= level)\
        printf text;\
  }
#else
  #define DEBUG_TEXT(my_package, level, text)
#endif

#ifdef __cplusplus
extern "C" {
#endif

u_int ALTRO_Open(char *node, int *handle);
u_int ALTRO_Close(int handle);
u_int ALTRO_Send(int handle, altro_bulk_out_t *out);
u_int ALTRO_Get(int handle, altro_bulk_in_t *in);
u_int ALTRO_Control(int handle, altro_control_t *ctrl);
u_int ALTRO_err_get(err_pack err, err_str pid, err_str code);

#ifdef __cplusplus
}
#endif

#endif
