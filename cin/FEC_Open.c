/* CIN source file */

#include "extcode.h"
#include "fec/fec.h"

MgErr CINRun(uInt16 *Error, uInt32 *Handle);

MgErr CINRun(uInt16 *Error, uInt32 *Handle)
	{
	int h;
	*Error = FEC_Open(DEFAULT_NODE, &h);
	*Handle = h;
	return *Error;
	}
