/**
 * 
 */
package core.global;

import core.database.data.U2FData;

/**
 * @author okhalid
 *
 */
public class GlobalVariables {

	public static final int MaxU2FRegisters = 11;
	/* * READ_WRITE varaible is used to set read/write access level of the register. */
	public static int[] U2FRegisterData = new int[MaxU2FRegisters];
	/* * This object holds the database configurations */
	public static U2FData u2fDatafromDB = new U2FData();
	
	
	/* * Each U2F / RCU can be connected to 16 fec's */
	public static int MaxPerU2Ffecs = 16;
	/* * This object holds the database configurations */
	public static int[] FEC_STATUS = new int[MaxPerU2Ffecs];
	/* * This interger holds the fec's to be switched ON */
	public static int FecAddress = 0;

	
	/* * Each Altro chip on the FEC has 16 channels	 */
	public static final int MaxPerAltroChannels = 16;
	/*  * Each Altro chip has 16 global registers */
	public static final int MaxPerAltroGlobalRegisters = 19;
	/* * Each Altro Channel has 7 registers */
	public static final int MaxPerAltroChannelRegisters = 7;
	/* * Each FEC has 8 ALtro chips */
	public static final int MaxPerFecAltros = 8;
	/* * Eacht Altro has 11 global registers */
	public static final int MaxAltroGlobalRegisters = 11;
	
	
	
	/*  * READ_ONLY variable is used to set read only access level of the register. */
	public static final int READ_ONLY  = 0;
	/* * READ_WRITE varaible is used to set read/write access level of the register. */
	public static final int READ_WRITE = 1;
	/* * WRITE_ONLY variable is used only to write to a register */
	/****************** Altro Function Execution Data Holders **********************/
	
	/* The following variables are needed for the execution of Altro related functions through AltroReadWrite Class */
	/* AltroFecAddr variable is only for Altro communicatio, and should not be confused with FecAddress */
	public static int FecAddr=0;
	public static int ChannelAddr=0;
	public static int RegAddr=0; 
	public static int RegData=0;
	public static int[] RegDataArray;
	public static int[] RegAddrArray;
	public static int RegLoopCount=1;
	public static int[] AltroGlobalRegDataArray = new int[MaxAltroGlobalRegisters];
	/******************************************************************************/
	
	/* * Following variables are needed for the database connections to the mySQL database */
	public static final String mySQLUrl = "jdbc:mysql://localhost/u2f";
	public static final String mySQLUser = "u2f";
	public static final String mySQLPass = "u2f";
	
	/* Path to be used for Image loading */
	public static String imagePath = "/afs/cern.ch/user/o/okhalid/public/2005-2006/altro_usb/images/";
}
