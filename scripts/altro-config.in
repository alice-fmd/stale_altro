#!/bin/sh
#
#  Copyright (c) 2005 Christian Holm <cholm@nbi.dk>
#
#  This library is free software; you can redistribute it and/or 
#  modify it under the terms of the GNU Lesser General Public License 
#  as published by the Free Software Foundation; either version 2.1 
#  of the License, or (at your option) any later version. 
#
#  This library is distributed in the hope that it will be useful, 
#  but WITHOUT ANY WARRANTY; without even the implied warranty of 
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
#  Lesser General Public License for more details. 
# 
#  You should have received a copy of the GNU Lesser General Public 
#  License along with this library; if not, write to the Free 
#  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
#  02111-1307 USA 
#

prefix=@prefix@
exec_prefix=@exec_prefix@
includedir=@includedir@ 
pkgincludedir=@includedir@
libdir=@libdir@
pkglibdir=@libdir@
package="@PACKAGE_TARNAME@"
string="@PACKAGE_STRING@"
email="@PACKAGE_BUGREPORT@"
version="@VERSION@"

prog=`basename $0`
lib=0
needaltro=0
needfec=0  
needgoofie=0 
needu2f=0    

if test $# -eq 0 ; then 
    $0 --help
    exit 0
fi

while test $# -gt 0 ; do 
    case $1 in 
	--*=*)    
	    opt=`echo $1 | sed 's/=.*//'`
	    arg=`echo $1 | sed 's/[^=]*=//'` 
	    ;;
	--*)
	    opt=$1
	    arg=
	    ;;
	-*)
	    opt=$1
	    arg=$2
	    ;;
	*)
	    opt=$1
	    ;;
    esac

    case $opt in
	--help|-h) 
	    cat <<EOF
Usage: $prog [OPTIONS] INTERFACE

  --prefix          Gives the installation prefix
  --libdir          Gives path to libraries
  --includedir      Gives path to headers 
  --libs            Gives libraries information 
  --ldflags         Gives linkage information 
  --cppflags        Gives preprocessor information
  --version         Gives version information
  --help            Gives this help

INTERFACE is one of 

  altro             USB low-level interface 
  u2f		    USB 2 FEC card (RCU test board)
  goofie            Goofie card 
  fec               Front end card abstraction layer

EOF
	    ;;
	--version|-v)
	    cat <<EOF
$string 

Copyright 2002 Christian Holm Christensen <$email>
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
EOF
	    exit 0
	    ;;
	-V)
	    echo $version 
	    exit 0
	    ;;
	--prefix|-p)
	    out="$out $prefix" 
	    ;;
	--libdir|-L)
	    out="$out $pkglibdir"
	    ;;
	--includedir|-I)
	    out="$out $includedir" 
	    ;;
	--cppflags|-D)
	    if test ! "x$pkgincludedir" = "x/usr/include" ; then 
		out="$out -I$pkgincludedir" 
	    fi
	    ;;
	--ldflags) 
	    if test ! "x$pkglibdir" = "x/usr/lib" ; then 
		out="$out -L$pkglibdir" 
	    fi
	    lib=1
	    ;;
	--libs|-l)
	    lib=1
	    ;;
	*)
	    opt=`echo $opt | tr '[A-Z]' '[a-z]'`
	    case $opt in 
		altro)  needaltro=1  ;; 
		fec)    needfec=1    ; needaltro=1 ; needu2f=1 ;;
		goofie) needgoofie=1 ; needaltro=1 ;;
		u2f)    needu2f=1    ; needaltro=1 ;;
		*)
		    echo "$prog: unknown option '$opt' - try '$prog --help'"
		    exit 1
		    ;;
	    esac
	    ;;
    esac
    shift
done

if test $lib -gt 0 ; then 
    if test $needaltro  -gt 0 ; then out="$out -laltro"  ; fi
    if test $needgoofie -gt 0 ; then out="$out -lgoofie" ; fi
    if test $needu2f    -gt 0 ; then out="$out -lu2f"    ; fi
    if test $needfec    -gt 0 ; then out="$out -laltrofec"; fi
fi
echo $out

		
	
