package core.modules.u2f;

import core.global.GlobalVariables;
import javax.swing.JTextField;
import javax.swing.JButton;

public class U2fManager implements Runnable {


public U2fGui u2fGui;	
private Register[] u2fRegisters;
private JTextField[] u2fRegisterGuiDestination;
private JButton[] u2fRegTextBtn;

public U2fManager()
{
	run();
};

public void run()
{
	initComponents();
}
/*
 *  This method allows to initialise the components of U2F Object.
 */
private void initComponents()
{
	//initialise U2F Registers
	initGui();
	initU2FRegisters();

}

private void initGui()
{
	/*
	 * This creates an instance of the U2FGUI Object
	 */
	u2fGui = new U2fGui();
	/*
	 * Here the U2FGUI Textfields are returned as an array, whose references are passed to the local u2fRegisterGuiDestination
	 * array and then set to the local u2fRegister array for connecting it the individual register.
	 */
	u2fRegisterGuiDestination = u2fGui.getU2FRegisterTextFields();
	u2fRegTextBtn			  = u2fGui.getRegisterButtons();
}

public void initU2FRegisters()
{
	/*
	 * U2 has 11 registers which would be initialised from here. List of registers could be obtained
	 * from U2F documentation written by Luciano Musa @CERN
	 */
	u2fRegisters 				= new Register 		[GlobalVariables.MaxU2FRegisters];

	/* 11 statements to initialised the U2F register array 
	 * 
	 * */
	   int index=0;
	   u2fRegisters[index] = new Register("ERRST"	,GlobalVariables.READ_ONLY	,4	,"0x7800",u2fRegisterGuiDestination[index], 
			   u2fRegTextBtn[(index)+1],u2fRegTextBtn[(index)+1] );
	   index++;
	   u2fRegisters[index] = new Register("TRCFG1",GlobalVariables.READ_WRITE	,29	,"0x7801",u2fRegisterGuiDestination[index]); 
	   index++;
	   u2fRegisters[index] = new Register("TRCNT"	,GlobalVariables.READ_ONLY	,32	,"0x7802",u2fRegisterGuiDestination[index]); 
	   index++;
	   u2fRegisters[index] = new Register("LWADD"	,GlobalVariables.READ_ONLY	,16	,"0x7803",u2fRegisterGuiDestination[index]); 
	   index++;
	   u2fRegisters[index] = new Register("IRADD"	,GlobalVariables.READ_ONLY	,20	,"0x7804",u2fRegisterGuiDestination[index]); 
	   index++;
	   u2fRegisters[index] = new Register("IRDAT"	,GlobalVariables.READ_ONLY	,20	,"0x7805",u2fRegisterGuiDestination[index]); 
	   index++;
	   u2fRegisters[index] = new Register("EVWORD",GlobalVariables.READ_ONLY	,5	,"0x7806",u2fRegisterGuiDestination[index]); 
	   index++;
	   u2fRegisters[index] = new Register("ACTFEC",GlobalVariables.READ_WRITE	,16	,"0x7807",u2fRegisterGuiDestination[index]); 
	   index++;
	   u2fRegisters[index] = new Register("FMIREG",GlobalVariables.READ_WRITE	,16	,"0x7808",u2fRegisterGuiDestination[index]); 
	   index++;
	   u2fRegisters[index] = new Register("FMOREG",GlobalVariables.READ_WRITE	,8	,"0x7809",u2fRegisterGuiDestination[index]); 
	   index++;
	   u2fRegisters[index] 	 = new Register("TRCFG2",GlobalVariables.READ_WRITE	,2	,"0x780A",u2fRegisterGuiDestination[index]); 
	   
	   for (index=0; index < GlobalVariables.MaxU2FRegisters; index++)
	   {
		   /* This reads the values of U2FRegisters from the GlobalVariables Class and that data is 
		   *  set in the u2fRegisters by calling the setData method. The setData method takes in
		   *  a string, so the int values are converted to String and then passed to Register.setData(String).
		   *  This automatically updates the values in the connected GUI components.
		   */
		   u2fRegisters[index].setData(GlobalVariables.U2FRegisterData[index]);
	   }
 }



public U2fGui getU2FGui()
{
	return u2fGui;
}
}
/*
 * 
 *

private float temperature;
private float digitalVoltage;
private float digitalCurrent;
private float analogueCurrent;
private float analogueVoltage;

private JTextField[] u2fStatusField;
private final int numberOfU2fStatusField = 5;
private int numberOfU2fStatusFieldIndex = 0;
private boolean isGuiComponentAttached = false;

public void setGlobalValues(float temp, float digVoltage, float digCurrent, float aVoltage, float aCurrent)
{
	setTemperature(temp);
	setDigitalVoltage(digVoltage);
	setDigitalCurrent(digCurrent);
	setAnalogueVoltage(aVoltage);
	setAnalogueCurrent(aCurrent);
}

public void attachGUIComponents(JTextField tempTxt, JTextField digVTxt, JTextField digCTxt, JTextField aVTxt, JTextField aCTxt )
{
	if(!isGuiComponentAttached)
	{
	//create an array of the local text fields
	u2fStatusField  = new JTextField[numberOfU2fStatusField]; 
	
	u2fStatusField[numberOfU2fStatusFieldIndex++]  = tempTxt;
	u2fStatusField[numberOfU2fStatusFieldIndex++]  = digVTxt;
	u2fStatusField[numberOfU2fStatusFieldIndex++]  = digCTxt;
	u2fStatusField[numberOfU2fStatusFieldIndex++]  = aVTxt;
	u2fStatusField[numberOfU2fStatusFieldIndex++]  = aCTxt;
	isGuiComponentAttached = true;
	}
 }

public void getGlobalData()
{
	u2fStatusField[0].setText(String.valueOf(getTemperature()));
	u2fStatusField[0].repaint();
	
	u2fStatusField[1].setText(String.valueOf(getDigitalVoltage()));
	u2fStatusField[1].repaint();
	
	u2fStatusField[2].setText(String.valueOf(getDigitalCurrent()));
	u2fStatusField[2].repaint();
	
	u2fStatusField[3].setText(String.valueOf(getAnalogueVoltage()));
	u2fStatusField[3].repaint();
	
	u2fStatusField[4].setText(String.valueOf(getAnalogueCurrent()));
	u2fStatusField[4].repaint();
}

private float getTemperature()
{
	return temperature;
}

private void setTemperature (float newTemperature)
{
	temperature =  newTemperature;
}

private float getDigitalVoltage()
{
	return digitalVoltage;
}

private void setDigitalVoltage(float newDigVoltage)
{
	digitalVoltage = newDigVoltage;
}

private float getDigitalCurrent()
{
	return digitalCurrent;
}

private void setDigitalCurrent(float newDigCurrent)
{
	digitalCurrent = newDigCurrent;
}

private float getAnalogueVoltage()
{
	return analogueVoltage;
}

private void setAnalogueVoltage(float newAVoltage)
{
	analogueVoltage = newAVoltage;		
}

private float getAnalogueCurrent()
{
	return analogueCurrent;
}

private void setAnalogueCurrent(float newACurrent)
{
	analogueCurrent = newACurrent;
}
*/


