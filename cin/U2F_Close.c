/* CIN source file */

#include "extcode.h"
#include "u2f/u2f.h"

MgErr CINRun(uInt32 *Handle, uInt16 *Error);

MgErr CINRun(uInt32 *Handle, uInt16 *Error)
	{
        *Error = U2F_Close(*Handle);
	return noErr;
	}
