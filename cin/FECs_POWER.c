/* CIN source file */

#include "extcode.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/file.h>
#include <signal.h>
#include "fec/fec.h"

MgErr CINRun(uInt16 *Handle, uInt16 *FECsAddr, uInt16 *Error);

MgErr CINRun(uInt16 *Handle, uInt16 *FECsAddr, uInt16 *Error)
	{
	*Error = FEC_Power_Reset(*Handle, *FECsAddr);
	return noErr;
	}
