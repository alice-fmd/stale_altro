/* CIN source file */

#include "extcode.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/file.h>
#include <signal.h>
#include "fec/fec.h"

MgErr CINRun(uInt32 *Handle, uInt32 *ChannelAddress, uInt32 *NWords, uInt32 *Offset, LStrHandle Fname, int32 *arg1, uInt16 *Error);

MgErr CINRun(uInt32 *Handle, uInt32 *ChannelAddress, uInt32 *NWords, uInt32 *Offset, LStrHandle Fname, int32 *arg1, uInt16 *Error)
	{
	char *new_file_name;

        new_file_name = (char*)LStrBuf(*Fname);
	new_file_name[*arg1] = '\0';

        *Error = FEC_Read_ALTRO_PMEM(*Handle, *ChannelAddress, *NWords, new_file_name); 
	return noErr;
	}

