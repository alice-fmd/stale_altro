#!/bin/sh

# set -e 
export PATH=$PATH:/usr/sbin:/sbin

if `grep -q altro_driver /proc/modules > /dev/null 2>&1` ; then 
    echo -n "Removing driver ... "
    rmmod -f altro_driver
    sleep 1
    echo "done"
fi

echo -n "Installing driver ... "
insmod driver/altro_driver.ko debug=1
if test $? -ne 0 ; then 
	exit 1
fi
sleep 1
echo "done"

echo -n "Making devices ... "
major=`grep usb /proc/devices | awk '{print $1}'`
minors=`grep Device: /proc/altro | awk '{print $3}' | tr -d '()'`
i=0
for minor in $minors ; do 
    if test ! -c /dev/altro$i ; then 
	mknod /dev/altro$i c $major $minor 
    fi
    let i=$i+1
done
echo "done (total of $i devices)"

echo -n "Changing permissions on device ... "
for dev in /dev/altro? ; do 
    if test -c $dev ; then
	chmod 666 $dev 
    fi
done
echo "done"

#
# EOF
#
