/********************************************************/
/*  This is the driver for the ALICE ALTRO USB core	*/
/*							*/
/*  Markus Joos, CERN-PH-ESS 15.3.2004			*/		
/*							*/
/*** C 2004 - The software with that certain something **/

/****************************************************************************************/
/* This dirver works for kernels from 2.4 onwards					*/
/*											*/
/* Currently only one ALTRO card is supported. Extending the driver for multiple cards	*/
/* would require to introduce a "minor" array as done in the usb-skeleton driver	*/
/****************************************************************************************/

#include <linux/config.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/init.h>
#include <linux/usb.h>
#include <linux/proc_fs.h>
#include <linux/devfs_fs_kernel.h>
#include <asm/uaccess.h>
#include "altro_common.h"

MODULE_PARM (debug, "i");
MODULE_PARM_DESC(debug, "1 = enable debugging   0 = disable debugging");
MODULE_PARM (errorlog, "i");
MODULE_PARM_DESC(errorlog, "1 = enable error logging   0 = disable error logging");
MODULE_PARM (timeout, "i");
MODULE_PARM_DESC(timeout, "timeout for usb_bulk_msg in units of jiffies");
MODULE_PARM (ddump, "i");
MODULE_PARM_DESC(ddump, "1 = enable data dumping   0 = disable data dumping");
MODULE_DESCRIPTION("USB driver for the ALICE ALTRO USB core");
MODULE_AUTHOR("Markus Joos, CERN/EP");
MODULE_LICENSE("Private: Contact markus.joos@cern.ch");


#ifndef USB_ALTRO_MINOR_BASE
# define USB_ALTRO_MINOR_BASE 192
#endif

#ifdef ALTRO_KERNEL_DEBUG
# define kdebug(x) {if (debug) printk x;}
#else
# define kdebug(x)
#endif

#define kerror(x) {if (errorlog) printk x;}


/*We need a local data structure, as it must be allocated for each new device plugged in the USB bus*/ 
struct altro_device 
{
  char data [10];
  char *name;                          /* the name of the device */
  struct urb urb;                      /* USB Request block, to get USB data*/
  int maxp;                            /* packet len */
  char output[80];                     /* used for printk at irq time */
  unsigned char minor;                 /* the starting minor number for this device */
  devfs_handle_t devfs;                /* devfs device node */
  struct usb_device *udev;             /* save off the usb device pointer */
  struct semaphore sem;                /* locks this structure */
  unsigned char *bulk_in_buffer;       /* the buffer to receive data */
  int bulk_in_size;                    /* the size of the receive buffer */
  __u8 bulk_in_endpointAddr;           /* the address of the bulk in endpoint */
  unsigned char *bulk_out_buffer;      /* the buffer to send data */
  int bulk_out_size;                   /* the size of the send buffer */
  struct urb *write_urb;               /* the urb used to send data */
  __u8 bulk_out_endpointAddr;          /* the address of the bulk out endpoint */
  int open_count;		       /* number of times this port has been opened */
};

/* Data in /proc file system */
struct altro_proc_data_t
{
  char name[10];
  char value[100];
};

/* Prototypes */
static int altro_open (struct inode *inode, struct file *file);
static int altro_release(struct inode *inode, struct file *file);
static int proc_write_altro(struct file *file, const char *buffer, unsigned long count, void *data);
static int proc_read_altro(char *page, char **start, off_t off, int count, int *eof, void *data);
static int altro_ioctl(struct inode *inode, struct file *file, unsigned int cmd, unsigned long arg);
static void altro_delete (struct altro_device *dev);
static void altro_disconnect(struct usb_device *udev, void *clientdata);
static void *altro_probe(struct usb_device *udev, unsigned int ifnum, const struct usb_device_id *id);
int altro_init(void);
void altro_exit(void);

/* Globals */ 
static int upid[MAX_ALTRO_DEVICES], cardtype = 0, ncards = 0, debug = 1, errorlog = 1, timeout = HZ * 10, ddump = 0;
static struct proc_dir_entry *altro_file;
static struct altro_proc_data_t altro_proc_data;
static struct altro_device *altro_table[MAX_ALTRO_DEVICES];
static DECLARE_MUTEX (minor_table_mutex);  /* lock to protect the minor_table structure */
static char *proc_read_text;

/* Externals */
extern devfs_handle_t usb_devfs_handle;


/* The id_table, lists all devices that can be handled by this driver. */
/* The three numbers are class, subclass, protocol. <linux/usb.h> has */
/* more details about interface matches and vendor/device matches. */
/* Here we use a fake usb_device_id structure defined in ./usb-sysdep.h */

/*The parameters for the ALTRO are*/
/*Class    = */
/*Subclass = */
/*Protocol = */

static struct usb_device_id altro_id_table [] = 
{
  { 
    USB_DEVICE(0x1556, 0x350),
    driver_info: (unsigned long)"ALICE U2F (invalid device id 0x350)"
  },
  {
    USB_DEVICE(0x1556, 0x15e),
    driver_info: (unsigned long)"ALICE U2F"
  },
  { 
    USB_DEVICE(0x1556, 0x15f),
    driver_info: (unsigned long)"ALICE GOOFIE"
  },
  {
    USB_DEVICE(0x1556, 0x163),
    driver_info: (unsigned long)"ALICE MPT"
  },
  {
    0, /* no more matches */
  }
};

MODULE_DEVICE_TABLE (usb, altro_id_table);

/* file operations supported */
static struct file_operations altro_fops = 
{
  owner:       THIS_MODULE,
  ioctl:       altro_ioctl,
  open:        altro_open,
  release:     altro_release,
};

/* The callbacks are registered within the USB subsystem using the */
/* usb_driver data structure */
static struct usb_driver altro_usb_driver = 
{
  owner:       THIS_MODULE,
  name:        "altro",
  probe:       altro_probe,
  disconnect:  altro_disconnect,
  fops:        &altro_fops,
  minor:       USB_ALTRO_MINOR_BASE,
  id_table:    altro_id_table,
};


/********************************************************/
static inline void altro_delete (struct altro_device *dev)
/********************************************************/
{
  altro_table[dev->minor] = NULL;

  if (dev->bulk_in_buffer != NULL)
    kfree (dev->bulk_in_buffer);

  if (dev->bulk_out_buffer != NULL)
    kfree (dev->bulk_out_buffer);

  if (dev->write_urb != NULL)
    usb_free_urb (dev->write_urb);

  kfree (dev);
}


/************************************************************/
static int altro_open (struct inode *inode, struct file *file)
/************************************************************/
{
  int subminor;
  struct altro_device *dev = NULL;
	  
  kdebug(("altro_drv(altro_open): function called\n"));

  subminor = MINOR(inode->i_rdev) - USB_ALTRO_MINOR_BASE;
  if ((subminor < 0) || (subminor >= MAX_ALTRO_DEVICES)) 
  {
    kerror(("altro_drv(altro_open): failed to get minor number\n"));
    return -ENODEV;
  }

  kdebug(("altro_drv(altro_open): subminor = %d\n", subminor));

  /* lock our minor table and get our local data for this minor */
  down (&minor_table_mutex);
  dev = altro_table[subminor];
  if (dev == NULL) 
  {
    up (&minor_table_mutex);
    kerror(("altro_drv(altro_open): altro_table[%d] has not yet been initialized\n", subminor));
    up (&minor_table_mutex);
    return -ENODEV;
  }

  kdebug(("altro_drv(altro_open): open count = %d\n", dev->open_count));
  if (dev->open_count)
  {
    kerror(("altro_drv(altro_open): ERROR. This device has already been opened by process %d\n", upid[subminor]));
    up (&minor_table_mutex);
    return -EBUSY;
  }
  
  /* lock this device */
  down (&dev->sem);

  /* unlock the minor table */
  up (&minor_table_mutex);

  /* increment our usage count for the driver and store the PID*/
  /* MJ: using both open_count and upid does not make much sense....*/
  ++dev->open_count;
  upid[subminor] = current->pid;

  /* save our object in the file's private structure */
  file->private_data = dev;

  /* unlock this device */
  up (&dev->sem);
  
  kdebug(("altro_drv(altro_open): minor number = %d\n", MINOR(inode->i_rdev)));
  kdebug(("altro_drv(altro_open): function done\n"));
  return 0;
}


/**************************************************************/
static int altro_release(struct inode *inode, struct file *file)
/**************************************************************/
{
  int subminor;
  struct altro_device *dev;

  kdebug(("altro_drv(altro_release): function called\n"));

  subminor = MINOR(inode->i_rdev) - USB_ALTRO_MINOR_BASE;
  
  dev = (struct altro_device *)file->private_data;
  if (dev == NULL) 
  {
    kerror(("altro_drv(altro_release): This device has not yet been created\n"));
    return -ENODEV;
  }

  /* lock our minor table */
  down (&minor_table_mutex);
  kdebug(("altro_drv(altro_release): minor_table_mutex locked\n"));

  /* lock our device */
  down (&dev->sem);
  kdebug(("altro_drv(altro_release): dev->sem locked\n"));

  if (dev->open_count <= 0) 
  {
    kerror(("altro_drv(altro_release): This device has not yet been opened\n"));
    up (&dev->sem);
    up (&minor_table_mutex);
    kerror(("altro_drv(altro_release): dev->sem and minor_table_mutex released\n"));
    return -ENODEV;
  }

  if (dev->udev == NULL) 
  {
    kerror(("altro_drv(altro_release): The device was unplugged before the file was released\n"));
    up (&dev->sem);
    altro_delete (dev);
    up (&minor_table_mutex);
    kerror(("altro_drv(altro_release): dev->sem and minor_table_mutex released\n"));
    return 0;
  }

  /* decrement our usage count for the device */
  --dev->open_count;
  if (dev->open_count <= 0) 
  {
    /* shutdown any bulk writes that might be going on */
    usb_unlink_urb (dev->write_urb);
    dev->open_count = 0;
    upid[subminor] = 0;
  }

  up (&dev->sem);
  up (&minor_table_mutex);
  kdebug(("altro_drv(altro_release): dev->sem and minor_table_mutex released\n"));
  return 0;
}


/* This callback is invoked when a USB device is detached from the bus*/
/*********************************************************************/
static void altro_disconnect(struct usb_device *udev, void *clientdata)
/*********************************************************************/
{
  /* the clientdata is the altro_device we passed originally */
  struct altro_device *altro;
  int minor;

  kdebug(("altro_drv(altro_disconnect): function called\n"));
  altro = (struct altro_device *)clientdata;

  down (&minor_table_mutex);
  kdebug(("altro_drv(altro_disconnect): minor_table_mutex locked\n"));
  down (&altro->sem);
  kdebug(("altro_drv(altro_disconnect): altro->sem locked\n"));

  /* remove the URB, remove the input device, free memory */
  usb_unlink_urb(&altro->urb);
  kdebug(("altro_drv(altro_disconnect): altro->urb unlinked\n"));

  minor = altro->minor;
  kdebug(("altro_drv(altro_disconnect): minor number = %d\n", minor));

#ifdef CONFIG_DEVFS_FS
  /* remove our devfs node */
  kdebug(("altro_drv(altro_disconnect): Removing entry from devfs\n"));
  devfs_unregister(altro->devfs);
#endif

  /* if the device is not opened, then we clean up right now */
  if (!altro->open_count) 
  {
    kerror(("altro_drv(altro_disconnect): Device was not opened\n"));
    up (&altro->sem);
    kerror(("altro_drv(altro_disconnect): altro->sem relased\n"));
    altro_delete (altro);
  } 
  else 
  {
    altro->udev = NULL;
    kdebug(("altro_drv(altro_disconnect): altro->udev set to NULL\n"));
    up (&altro->sem);
    kdebug(("altro_drv(altro_disconnect): altro->sem relased\n"));
  }

  up (&minor_table_mutex);
  kdebug(("altro_drv(altro_disconnect): minor_table_mutex relased\n"));
  kdebug(("altro_drv(altro_disconnect): USB %s disconnected\n", altro->name));
  
  ncards--;
  kdebug(("altro_drv(altro_disconnect): ncards = %d\n", ncards));
  if (!ncards)
  {
    cardtype = 0;
    kdebug(("altro_drv(altro_disconnect): cardtype reset\n"));
  }
  return;
}


/*This callback is invoked when a USB device is attached to the bus */ 
/***************************************************************************************************/
static void *altro_probe(struct usb_device *udev, unsigned int ifnum, const struct usb_device_id *id)
/***************************************************************************************************/
{
  /* The probe procedure is pretty standard. Device matching has already */
  /* been performed based on the id_table structure */

  struct usb_interface *iface;
  struct usb_interface_descriptor *interface;
  struct usb_endpoint_descriptor *endpoint;
  struct altro_device *altro;
  int i, buffer_size, inok, outok, subminor;
#ifdef CONFIG_DEVFS_FS
  char name[10];
#endif

  kdebug(("altro_drv(altro_probe): probe called for %s device\n", (char *)id->driver_info));
  kdebug(("altro_drv(altro_probe): ifnum = %d\n", ifnum));

  /* select a "subminor" number (part of a minor number) */
  down (&minor_table_mutex);
  for (subminor = 0; subminor < MAX_ALTRO_DEVICES; ++subminor) 
  {
    if (altro_table[subminor] == NULL)
    {
      kdebug(("altro_drv(altro_probe): minor number %d is still available\n", subminor));
      break;
    }
    else
    {
      kdebug(("altro_drv(altro_probe): minor number %d is in use\n", subminor));
    }
  }
  
  if (subminor == MAX_ALTRO_DEVICES) 
  {
    kerror(("altro_drv(altro_probe): Too many devices plugged in, can not handle this device."));
    up (&minor_table_mutex);
    return NULL;
  }

  /* allocate memory for our device state and intialize it */
  altro = kmalloc(sizeof(struct altro_device), GFP_KERNEL);
  if (altro == NULL) 
  {
    kerror(("altro_drv(altro_probe): ERROR failed to kmalloc altro_device\n"));
    up (&minor_table_mutex);
    return NULL;    
  }

  memset(altro, 0, sizeof(*altro));
  altro_table[subminor] = altro;

  init_MUTEX(&altro->sem);
  altro->udev      = udev;
  altro->minor     = subminor;
  altro->name      = (char *)id->driver_info;

  kdebug(("altro_drv(altro_probe): udev->descriptor.idVendor = 0x%08x\n", udev->descriptor.idVendor));
  kdebug(("altro_drv(altro_probe): udev->descriptor.idProduct = 0x%08x\n", udev->descriptor.idProduct));

  if (ncards == 0)
  {
    ncards++;
    cardtype = udev->descriptor.idProduct;
    kdebug(("altro_drv(altro_probe): First card of type %d fround\n", cardtype));
  }
  else
  {
    if (cardtype == udev->descriptor.idProduct)
    {
      ncards++;
      kdebug(("altro_drv(altro_probe): Another card of type %d fround\n", cardtype));
    }  
    else
    {
      kerror(("altro_drv(altro_probe): Card of type %d fround\n", udev->descriptor.idProduct));
      kerror(("altro_drv(altro_probe): A card of type %d is already connected\n", cardtype));
      up (&minor_table_mutex);
      return NULL;
    }
  }
  kdebug(("altro_drv(altro_probe): ncards = %d\n", ncards));
  
  iface = &udev->actconfig->interface[ifnum];
  interface = &iface->altsetting[iface->act_altsetting];

  if (interface->bNumEndpoints != 2) 
  {
    kerror(("altro_drv(altro_probe): ERROR interface->bNumEndpoints != 2 (number = %d)\n", interface->bNumEndpoints));
    altro_delete(altro);
    up (&minor_table_mutex);
    return NULL;
  }
  
  inok = 0;
  outok = 0;
    
  for (i = 0; i < interface->bNumEndpoints; ++i) 
  {
    kdebug(("altro_drv(altro_probe): Processing endpoint %d ...\n", i));
    endpoint = &interface->endpoint[i];
    
    if ((endpoint->bEndpointAddress & 0x80) && ((endpoint->bmAttributes & 3) == 0x02)) 
    {
      if (!inok) 
      {
        kdebug(("altro_drv(altro_probe): we found the first bulk in endpoint\n"));
	buffer_size = endpoint->wMaxPacketSize;
	kdebug(("altro_drv(altro_probe): buffer_size = %d\n", buffer_size));
	if (BULK_IN_SIZE != buffer_size)
          kerror(("altro_drv(altro_probe): The constant BULK_IN_SIZE should be 0x%08x\n", buffer_size));

	altro->bulk_in_size = buffer_size;
	altro->bulk_in_endpointAddr = endpoint->bEndpointAddress;
	kdebug(("altro_drv(altro_probe): bulk_in_endpointAddr = %d\n", altro->bulk_in_endpointAddr));
	altro->bulk_in_buffer = kmalloc(buffer_size, GFP_KERNEL);
	if (!altro->bulk_in_buffer) 
	{
          kerror(("altro_drv(altro_probe): Couldn't allocate bulk_in_buffer"));
          altro_delete(altro);
          up (&minor_table_mutex);
          return NULL;
	}
	inok = 1;
      }
      else
        kdebug(("altro_drv(altro_probe): we found another bulk in endpoint\n"));
    }
    
    else if (((endpoint->bEndpointAddress & 0x80) == 0x00) && ((endpoint->bmAttributes & 3) == 0x02)) 
    {
      if (!outok) 
      {
	kdebug(("altro_drv(altro_probe): we found the first bulk out endpoint\n"));
	altro->write_urb = usb_alloc_urb(0);
	if (!altro->write_urb) 
	{
          kerror(("altro_drv(altro_probe): No free urbs available"));
          altro_delete(altro);
          up (&minor_table_mutex);
          return NULL;
	}
	buffer_size = endpoint->wMaxPacketSize;
	kdebug(("altro_drv(altro_probe): buffer_size = %d\n", buffer_size));
	if (BULK_IN_SIZE != buffer_size)
          kerror(("altro_drv(altro_probe): The constant BULK_IN_SIZE should be 0x%08x\n", buffer_size));

	altro->bulk_out_size = buffer_size;
	altro->bulk_out_endpointAddr = endpoint->bEndpointAddress;
	kdebug(("altro_drv(altro_probe): bulk_out_endpointAddr = %d\n", altro->bulk_out_endpointAddr));
	altro->bulk_out_buffer = kmalloc (buffer_size, GFP_KERNEL);
	if (!altro->bulk_out_buffer) 
	{
          kerror(("altro_drv(altro_probe): Couldn't allocate bulk_out_buffer"));
          altro_delete(altro);
          up (&minor_table_mutex);
          return NULL;
	}
	outok = 1;
      }	

      else
        kdebug(("altro_drv(altro_probe): we found another bulk out endpoint\n"));
    }
    else
    {
      kerror(("altro_drv(altro_probe): we found an unidentifiable endpoint\n"));
      up (&minor_table_mutex);
      return NULL;
    }
  }
  
  altro->udev = udev;

#ifdef CONFIG_DEVFS_FS
  kdebug(("altro_drv(altro_probe): Now creating entry in devfs\n"));
  /* initialize the devfs node for this device and register it */
  sprintf(name, "altro%d", altro->minor);
  kdebug(("altro_drv(altro_probe): Creating /dev/%s\n", name));
  kdebug(("altro_drv(altro_probe): USB_MAJOR = %d \n", USB_MAJOR));
  kdebug(("altro_drv(altro_probe): USB_MINOR = %d \n", USB_ALTRO_MINOR_BASE + altro->minor));
  
  altro->devfs = devfs_register (usb_devfs_handle, name, DEVFS_FL_DEFAULT, USB_MAJOR, USB_ALTRO_MINOR_BASE + altro->minor,
			         S_IFCHR | S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH, &altro_fops, NULL);
  if (altro->devfs == NULL)
    kdebug(("altro_drv(altro_probe): devfs is NULL\n"));
	       
#else
  kerror(("altro_drv(altro_probe): This kernel does not support devfs\n"));
#endif
  kdebug(("altro_drv(altro_probe): End of function\n"));
 
  up (&minor_table_mutex);
  return altro;
}


/* Function called at module load time: only register the USB callbacks */
/******************/
int altro_init(void)
/******************/
{
  int subminor;
  
  kdebug(("altro_drv(altro_init): function called\n"));
  kdebug(("altro_drv(altro_init): debug = %d\n", debug));

  proc_read_text = (char *)kmalloc(MAX_PROC_TEXT_SIZE, GFP_KERNEL);
  if (proc_read_text == NULL)
  {
    kerror(("altro_drv(altro_init): error from kmalloc\n"));
    return(-EFAULT);
  }

  /* Install /proc entry */
  altro_file = create_proc_entry("altro", 0644, NULL);
  if (altro_file == NULL) 
  {
    kerror(("altro_drv(altro_init): ERROR from call to create_proc_entry\n"));
    return (-ENOMEM);
  }

  strcpy(altro_proc_data.name, "altro");
  strcpy(altro_proc_data.value, "altro");
  altro_file->data = &altro_proc_data;
  altro_file->read_proc = proc_read_altro;
  altro_file->write_proc = proc_write_altro;
  altro_file->owner = THIS_MODULE;

  for (subminor = 0; subminor < MAX_ALTRO_DEVICES; ++subminor) 
    upid[subminor] = 0;

  return usb_register(&altro_usb_driver);
}


/* Function called at module unload time: only unregister the USB callbacks */ 
/*******************/
void altro_exit(void)
/*******************/
{
  kdebug(("altro_drv(altro_exit): function called\n"));

  /* Remove /proc entry */
  remove_proc_entry("altro", NULL);
  kfree(proc_read_text);
  
  usb_deregister(&altro_usb_driver);
}


/**********************/
module_init(altro_init);
/**********************/


/**********************/
module_exit(altro_exit);
/**********************/


/*************************************************************************************************/
static int proc_write_altro(struct file *file, const char *buffer, unsigned long count, void *data)
/*************************************************************************************************/
{
  int len;
  struct altro_proc_data_t *fb_data = (struct altro_proc_data_t *)data;

  kdebug(("altro_drv(proc_write_altro): proc_write_altro called\n"));
  
  if(count > 99)
    len = 99;
  else
    len = count;
  
  if (copy_from_user(fb_data->value, buffer, len)) 
  {
    kerror(("altro_drv(proc_write_altro): error from copy_from_user\n"));
    return(-EFAULT);
  }

  kdebug(("altro_drv(proc_write_altro): len = %d\n", len));
  fb_data->value[len - 1] = '\0';
  kdebug(("altro_drv(proc_write_altro): text passed = %s\n", fb_data->value));
  
  if (!strcmp(fb_data->value, "debug")) 
  {
    debug = 1;
    kdebug(("altro_drv(proc_write_altro): debugging enabled\n")); 
  }
 
  if (!strcmp(fb_data->value, "nodebug")) 
  {
    kdebug(("altro_drv(proc_write_altro): debugging disabled\n")); 
    debug = 0; 
  }
  
  if (!strcmp(fb_data->value, "elog")) 
  {
    errorlog = 1;
    kdebug(("altro_drv(proc_write_altro): error logging enabled\n")); 
  }
 
  if (!strcmp(fb_data->value, "noelog")) 
  {
    kdebug(("altro_drv(proc_write_altro): error logging disabled\n")); 
    errorlog = 0; 
  }
  
  if (!strcmp(fb_data->value, "datadump")) 
  {
    ddump = 1;
    kdebug(("altro_drv(proc_write_altro): data dumping enabled\n")); 
  }
 
  if (!strcmp(fb_data->value, "nodatadump")) 
  {
    ddump = 0; 
    kdebug(("altro_drv(proc_write_altro): data dumping disabled\n")); 
  }
  
  if (!strcmp(fb_data->value, "dec")) 
  {
    MOD_DEC_USE_COUNT;
    kdebug(("altro_drv(proc_write_altro): Use count decremented\n"));
  }

  if (!strcmp(fb_data->value, "inc")) 
  {
    MOD_INC_USE_COUNT;
    kdebug(("altro_drv(proc_write_altro): Use count incremented\n"));
  }
 
  return len;
}


/************************************************************************************************/
static int proc_read_altro(char *buf, char **start, off_t offset, int count, int *eof, void *data)
/************************************************************************************************/
{
  int subminor, loop, nchars = 0;
  static int len = 0;

  kdebug(("altro_drv(proc_read_altro): Called with buf    = 0x%08x\n", (unsigned int)buf));
  kdebug(("altro_drv(proc_read_altro): Called with *start = 0x%08x\n", (unsigned int)*start));
  kdebug(("altro_drv(proc_read_altro): Called with offset = %d\n", (unsigned int)offset));
  kdebug(("altro_drv(proc_read_altro): Called with count  = %d\n", count));

  if (offset == 0)
  {
    kdebug(("exp(exp_read_procmem): Creating text....\n"));
    len = 0;
    len = sprintf(proc_read_text + len, "ALICE ALTRO USB driver\n");
    len += sprintf(proc_read_text + len, " \n");
    len += sprintf(proc_read_text + len, "Type of card(s) connected: ");
    if (cardtype == 0x15e || cardtype == 0x350)
      len += sprintf(proc_read_text + len, "ALICE U2F\n");
    else if (cardtype == 0x15f)
      len += sprintf(proc_read_text + len, "ALICE GOOFIE\n");
    else if (cardtype == 0x163)
      len += sprintf(proc_read_text + len, "ALICE MPT\n");
    else
      len += sprintf(proc_read_text + len, "UNKNOWN (maybe your card is using an invalid device ID)\n");
    len += sprintf(proc_read_text + len, "Number of cards connected: %d", ncards);
    len += sprintf(proc_read_text + len, "\n");

    for (subminor = 0; subminor < MAX_ALTRO_DEVICES; ++subminor) 
    {
      if (upid[subminor])
        len += sprintf(proc_read_text + len, "The Device %d is locked by process %d\n", subminor, upid[subminor]);
    }
    len += sprintf(proc_read_text + len, "\n");

    len += sprintf(proc_read_text + len, "The time-out of usb_bulk_msg() is %d jiffies (usually: 1 jiffie = 10 ms)\n", timeout);
    len += sprintf(proc_read_text + len, "\n");
    len += sprintf(proc_read_text + len, "The command 'echo <action> > /proc/altro', executed as root,\n");
    len += sprintf(proc_read_text + len, "allows you to interact with the driver. Possible actions are:\n");
    len += sprintf(proc_read_text + len, "debug      -> enable debugging\n");
    len += sprintf(proc_read_text + len, "nodebug    -> disable debugging\n");
    len += sprintf(proc_read_text + len, "eolg       -> enable error logging\n");
    len += sprintf(proc_read_text + len, "noeolg     -> disable error logging\n");    
    len += sprintf(proc_read_text + len, "datadump   -> enable data dumping (use with care)\n");
    len += sprintf(proc_read_text + len, "nodatadump -> disable data dumping\n");
    len += sprintf(proc_read_text + len, "dec        -> Decrement use count\n");
    len += sprintf(proc_read_text + len, "inc        -> Increment use count\n");
  }
  kdebug(("altro_drv(proc_read_altro): number of characters in text buffer = %d\n", len));

  if (count < (len - offset))
    nchars = count;
  else
    nchars = len - offset;
  kdebug(("altro_drv(proc_read_altro): min nchars         = %d\n", nchars));
  
  if (nchars > 0)
  {
    for (loop = 0; loop < nchars; loop++)
      buf[loop + (offset & (PAGE_SIZE - 1))] = proc_read_text[offset + loop];
    *start = buf + (offset & (PAGE_SIZE - 1));
  }
  else
  {
    nchars = 0;
    *eof = 1;
  }
 
  kdebug(("altro_drv(proc_read_altro): returning *start   = 0x%08x\n", (unsigned int)*start));
  kdebug(("altro_drv(proc_read_altro): returning nchars   = %d\n", nchars));
  return(nchars);
}


/**************************************************************************************************/
static int altro_ioctl (struct inode *inode, struct file *file, unsigned int cmd, unsigned long arg)
/**************************************************************************************************/
{
  struct altro_device *altro;
  int pipe, retval, loop;
  
  kdebug(("altro_drv(altro_ioctl): function called\n")); 
  kdebug(("altro_drv(altro_ioctl): minor number = %d\n", MINOR (inode->i_rdev)));

  altro = (struct altro_device *)file->private_data;
  
  /* lock this object */
  down (&altro->sem);
  kdebug(("altro_drv(altro_ioctl): semaphore locked\n")); 
  
  /* verify that the device wasn't unplugged */
  if (altro->udev == NULL) 
  {
    up (&altro->sem);
    kerror(("altro_drv(altro_ioctl): ERROR device was unplugged\n"));
    return -ENODEV;
  }
  kdebug(("altro_drv(altro_ioctl): The device is still connected\n")); 
  
  switch (cmd) 
  {    
    case GET_DATA: 
    {   
      altro_bulk_in_t params; /*MJ: this memory gets allocated on the stack. Better move it to the private data*/
      kdebug(("altro_drv(ioctl:GET_DATA): function called for endpoint 0x%08x\n", altro->bulk_in_endpointAddr & 0x7f));
      kdebug(("altro_drv(ioctl:GET_DATA): altro->bulk_in_size = %d\n", altro->bulk_in_size));

      pipe = usb_rcvbulkpipe(altro->udev, altro->bulk_in_endpointAddr);
      kdebug(("altro_drv(ioctl:GET_DATA): reading from pipe %d\n", pipe));

      /* do an immediate bulk read to get data from the device */
      /* MJ: The parameter altro->bulk_in_size requests as much data as the ALTRO can actually send */
      /* MJ: The real amount of data may be less than that.*/
      /* MJ: The constant BULK_IN_SIZE in the header file has to be tuned to the value of endpoint->wMaxPacketSize*/ 
      retval = usb_bulk_msg(altro->udev, pipe, &params.data[0], altro->bulk_in_size, &params.nbytes, timeout);
      kdebug(("altro_drv(ioctl:GET_DATA): usb_bulk_msg returns %d\n", retval));

      if (retval)  
      {
	kdebug(("altro_drv(ioctl:GET_DATA): No data received\n"));
	params.nbytes = 0;
      }
      else
	kdebug(("altro_drv(ioctl:GET_DATA): %d bytes received\n", params.nbytes));

      if (ddump)
      {
	kdebug(("altro_drv(ioctl:GET_DATA): Now dumping newly received data.......\n"));
        for (loop = 0; loop < params.nbytes; loop++)
	  kdebug(("altro_drv(ioctl:GET_DATA): Byte #%d = 0x%02x\n", loop, params.data[loop]));
      } 

      if (copy_to_user ((void *)arg, &params, sizeof(altro_bulk_in_t)) != 0) 
      {
	kerror(("altro_drv(ioctl:GET_DATA): error from copy_to_user\n"));
	up (&altro->sem);
	return(-ALTRO_EFAULT);
      }
      break;
    }    

    case SEND_DATA: 
    {   
      altro_bulk_out_t params;  /*MJ: this memory gets allocated on the stack. Better move it to the private data*/
      int maxp, ndone;

      kdebug(("altro_drv(ioctl:SEND_DATA): function called for endpoint 0x%08x\n", altro->bulk_out_endpointAddr & 0x7f));
      kdebug(("altro_drv(ioctl:SEND_DATA): altro->bulk_in_size = %d\n", altro->bulk_out_size));

      if (copy_from_user(&params, (void *)arg, sizeof(altro_bulk_out_t)) !=0) 
      {
	kerror(("altro_drv(ioctl:SEND_DATA): error from copy_from_user\n"));
	up (&altro->sem);
	return(-ALTRO_EFAULT);
      }

      pipe = usb_sndbulkpipe(altro->udev, altro->bulk_out_endpointAddr);
      kdebug(("altro_drv(ioctl:SEND_DATA): reading from pipe %d\n", pipe));

      maxp = usb_maxpacket(altro->udev, pipe, usb_pipeout(pipe));
      if (maxp > altro->bulk_out_size) 
	maxp = altro->bulk_out_size; 
      kdebug(("altro_drv(ioctl:SEND_DATA): it is possible to send %d bytes\n", maxp));

      /* check if we would write more data than possible */
      if (params.nbytes > maxp) 
      {	
	kerror(("altro_drv(ioctl:SEND_DATA): Cannot write %d bytes to pipe of %d bytes\n", params.nbytes, maxp));
	up (&altro->sem);
	return(-ALTRO_EFAULT);
      }
      kdebug(("altro_drv(ioctl:SEND_DATA): trying to send %d bytes\n", params.nbytes));

      if (ddump)
      {
	kdebug(("altro_drv(ioctl:SEND_DATA): Dumping data berfore sending it.......\n"));
        for (loop = 0; loop < params.nbytes; loop++)
	  kdebug(("altro_drv(ioctl:SEND_DATA): Byte #%d = 0x%02x\n", loop, params.data[loop]));
      } 

      /* do an immediate bulk write to send data to the device */
      retval = usb_bulk_msg(altro->udev, pipe, &params.data[0], params.nbytes, &ndone, timeout);
      kdebug(("altro_drv(ioctl:SEND_DATA): usb_bulk_msg returns %d\n", retval));
      if (retval) 
      {
	kerror(("altro_drv(ioctl:SEND_DATA): No data sent\n"));
	up (&altro->sem);
	return(-ALTRO_EFAULT);
      }
      else
	kdebug(("altro_drv(ioctl:SEND_DATA): %d bytes sent\n", ndone));

      break;
    }     

    case CONTROL: 
    {
      altro_control_t params;  /*MJ: this memory gets allocated on the stack. Better move it to the private data*/

      kdebug(("altro_drv(ioctl:CONTROL): function called\n"));

      if (copy_from_user(&params, (void *)arg, sizeof(altro_control_t)) !=0 ) 
      {
	kerror(("altro_drv(ioctl:CONTROL): error from copy_from_user\n"));
	up (&altro->sem);
	return(-ALTRO_EFAULT);
      }

      kdebug(("altro_drv(ioctl:CONTROL): params.bRequest      = %d\n", params.bRequest));
      kdebug(("altro_drv(ioctl:CONTROL): params.bmRequestType = %d\n", params.bmRequestType));
      kdebug(("altro_drv(ioctl:CONTROL): params.wValue        = %d\n", params.wValue));
      kdebug(("altro_drv(ioctl:CONTROL): params.wIndex        = %d\n", params.wIndex));
      kdebug(("altro_drv(ioctl:CONTROL): params.data[0]       = %d\n", params.data[0]));
      kdebug(("altro_drv(ioctl:CONTROL): params.wLength       = %d\n", params.wLength));

      retval = usb_control_msg(altro->udev, usb_rcvctrlpipe(altro->udev,0), params.bRequest, params.bmRequestType, 
			       params.wValue, params.wIndex, &params.data[0], params.wLength, HZ);
      if (retval) 
      {
	kerror(("altro_drv(ioctl:CONTROL): ERROR  failed to register control URB. Error code = %d\n", retval));
	up (&altro->sem); 
	return(-ALTRO_EFAULT);
      }
      break;
    }
  }
  /* unlock the device */
  up (&altro->sem);
  return(0);
}
