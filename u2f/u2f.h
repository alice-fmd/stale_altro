/************************************************************************/
/*									*/
/*  This is the header file for the U2F library				*/
/*									*/
/*  24. Jun. 04  MAJO  created						*/
/*									*/
/*******C 2004 - The software with that certain something****************/

#ifndef _U2F_H
#define _U2F_H

#include <altro/altro.h>

/*************/
/* Constants */
/*************/
#define MAX_U2F_DEVICES 16

/* Modes */
#define HW 0  /* Use the U2F and ALTRO H/W */
#define SW 1  /* Emulate the H/W */

/* Instruction codes */
#define I_WRITE  0
#define I_READ   1
#define I_GRESET 2

/* Addresses */
#define A_RMEM      0x6000
#define A_ACL       0x6400
#define A_PMEM      0x6800
#define A_IMEM      0x7000
#define A_DML1      0x7400
#define A_DMH1      0x7500
#define A_REGISTERS 0x7800
#define A_DML2      0x7c00
#define A_DMH2      0x7d00
#define A_DM2HH	    0xfd00

/* Registers */
#define O_ERRST   0
#define O_TRCFG1  1
#define O_TRCNT   2
#define O_LWADD   3
#define O_IRADD   4
#define O_IRDAT   5
#define O_EVWORD  6
#define O_ACTFEC  7
#define O_FMIREG  8
#define O_FMOREG  9
#define O_TRCFG2 10
#define O_RESREG 11
#define O_ERRREG 12
#define O_INTMOD 13
#define O_INTREG 14

/* Sizes */
#define S_IMEM  256
#define S_PMEM 1024
#define B_PMEM  256  
#define S_RMEM  128
#define S_ACL   256
#define S_DM    256

/* Commands */
#define C_RS_STATUS 0x6c01
#define C_RS_TRCFG  0x6c02
#define C_RS_TRCNT  0x6c03
#define C_EXEC      0x0000
#define C_ABORT     0x0800
#define C_FECRST    0x2001
#define C_SWTRG	    0xD000
#define C_RS_DMEM1  0xD001
#define C_RS_DMEM2  0xD002
#define C_TRG_CLR   0xD003
#define C_WRFM	    0xD004
#define C_RDFM      0xD005
#define C_RDABORT   0xD006
#define C_CLR_EVTAG 0xD007 
#define C_SCCOMMAND 0xC000

/* read out modes */
#define M_FIRST   1  /* start a new read-out   */
#define M_LAST    2  /* terminate the read out */
#define M_TRIGGER 4  /* Send a S/W trigger */

/*********/
/* Types */
/*********/
typedef struct 
{
  u_int pattern_error; /* Possible values are TRUE and FALSE */
  u_int abort;         /* Possible values are TRUE and FALSE */
  u_int timeout;       /* Possible values are TRUE and FALSE */
  u_int altro_error;   /* Possible values are TRUE and FALSE */
} errst_t;

typedef struct 
{
  u_int tw;    /* Legal values are 0 - 0x3fff     */
  u_int bmd;   /* Legal values are 0 or 1         */
  u_int mode;  /* Legal values are 0, 2 or 3      */
  u_int opt;   /* Legal values are 0 or 1  	  */
  u_int pop;   /* Legal values are 0 or 1  	  */
  u_int remb;  /* Legal values are 0 - 0xf        */
  u_int empty; /* Legal values are TRUE and FALSE */
  u_int full;  /* Legal values are TRUE and FALSE */
  u_int rd_pt; /* Legal values are 0 to 7         */
  u_int wr_pt; /* Legal values are 0 to 7         */
} trcfg_t;

typedef struct 
{
  u_int ntr; /* Possible values are 0 - 0xffff */
  u_int nta; /* Possible values are 0 - 0xffff */
} trcnt_t;

typedef struct 
{
  u_int id;    /* The bank number */
  u_int size;  /* The size of the bank */
  u_int* data; /* Data array for emulation mode*/
  u_int mask1;
  u_int mask2;
} u2f_bank_t;

/*************/
/*error codes*/
/*************/
enum
{
  U2F_SUCCESS = 0,  
  U2F_FILE = (P_ID_U2F<<8) + 1,
  U2F_NOTOPEN,
  U2F_ALTRO_FAIL,
  U2F_RANGE,
  U2F_ODD_BYTES,
  U2F_NODATA,
  U2F_NO_CODE,
  U2F_INC_BYTES,
  U2F_MALLOC,
  U2F_LOW_MEM,
  U2F_ERROR_FAIL
};


/***************/
/*error strings*/
/***************/
#define U2F_SUCCESS_STR        "Function successfully executed"
#define U2F_FILE_STR           "Failed to open / close data file"
#define U2F_NOTOPEN_STR        "The library has not been opened yet"
#define U2F_ALTRO_FAIL_STR     "Error from ALTRO library"
#define U2F_RANGE_STR          "A parameter is out of range"
#define U2F_ODD_BYTES_STR      "Wrong number of bytes received"
#define U2F_NO_CODE_STR        "Unknown error"
#define U2F_INC_BYTES_STR      "DML and DMH have delivered inconsistent amounts of data in a read operation"
#define U2F_NODATA_STR         "The files you are reading does not contain enought data"
#define U2F_MALLOC_STR         "Error from call to malloc()"
#define U2F_LOW_MEM_STR        "The next USB burst would overflow the data array"


/********/
/*Macros*/
/********/

#ifdef __cplusplus
extern "C" 
{ 
#endif

/***********************************/ 
/*Official functions of the library*/
/***********************************/ 
u_int U2F_Open(char* node, int* handle, int mode);
u_int U2F_Close(int handle);
u_int U2F_IMEM_Read(int handle, u_int isize, u_int *osize, u_int offset, 
		    u_int data[]);
u_int U2F_PMEM_Read(int handle, u_int isize, u_int *osize, u_int offset, 
		    u_int data[]);
u_int U2F_RMEM_Read(int handle, u_int isize, u_int *osize, u_int offset, 
		    u_int data[]);
u_int U2F_ACL_Read(int handle, u_int isize, u_int *osize, u_int offset, 
		   u_int data[]);
u_int U2F_DM1_Read(int handle, u_int isize, u_int *osize, u_int offset, 
		   u_int data[]);
u_int U2F_DM2_Read(int handle, u_int isize, u_int *osize, u_int offset, 
		   u_int data[]);
u_int U2F_IMEM_Write(int handle, u_int osize, u_int offset, u_int data[]);
u_int U2F_PMEM_Write(int handle, u_int osize, u_int offset, u_int data[]);
u_int U2F_RMEM_Write(int handle, u_int osize, u_int offset, u_int data[]);
u_int U2F_ACL_Write(int handle, u_int osize, u_int offset, u_int data[]);
u_int U2F_DM1_Write(int handle, u_int osize, u_int offset, u_int data[]);
u_int U2F_DM2_Write(int handle, u_int osize, u_int offset, u_int data[]);
u_int U2F_Reg_Write(int handle, u_int reg, u_int data);
u_int U2F_Reg_Read(int handle, u_int reg, u_int *data);
u_int U2F_ERRST_decode(u_int data, errst_t *parameters);
u_int U2F_TRCFG1_decode(u_int data, trcfg_t *parameters);
u_int U2F_TRCNT_decode(u_int data, trcnt_t *parameters);
u_int U2F_TRCFG1_encode(u_int *data, trcfg_t parameters);
u_int U2F_File_Read(char *name, u_int size, u_int data[]);
u_int U2F_File_Write(char *name, u_int size, u_int data[]);
u_int U2F_Exec_Command(int handle, u_int command, u_int data);
u_int U2F_Reset(int handle);
u_int U2F_DM_Read(int handle, u_int which, u_int isize, u_int *osize, 
		  u_int offset, u_int data[]);
u_int U2F_FMIREG_Read(int handle, u_int* data);
u_int U2F_FMOREG_Read(int handle, u_int* data);
u_int U2F_ERRST_Read(int handle, u_int* data);
u_int U2F_TRCFG1_Read(int handle, u_int* data);
u_int U2F_TRCFG2_Read(int handle, u_int* data);
u_int U2F_TRCNT_Read(int handle, u_int* data);
u_int U2F_LWADD_Read(int handle, u_int* data);
u_int U2F_IRADD_Read(int handle, u_int* data);
u_int U2F_IRDAT_Read(int handle, u_int* data);
u_int U2F_EVWORD_Read(int handle, u_int* data);
u_int U2F_ACTFEC_Read(int handle, u_int* data);
u_int U2F_ERRST_Get(int handle, errst_t *parameters);
u_int U2F_TRCFG1_Get(int handle, trcfg_t *parameters);
u_int U2F_TRCNT_Get(int handle, trcnt_t *parameters);
u_int U2F_DM_Write(int handle, u_int which, u_int osize, u_int offset, 
		   u_int data[]);
u_int U2F_TRCFG1_Write(int handle, u_int data);
u_int U2F_ACTFEC_Write(int handle, u_int data);
u_int U2F_TRCFG1_Set(int handle, trcfg_t parameters);
u_int U2F_Exec_RS_STATUS(int handle); 
u_int U2F_Exec_RS_TRCFG(int handle); 
u_int U2F_Exec_RS_TRCNT(int handle);  
u_int U2F_Exec_EXEC(int handle, u_int data); 
u_int U2F_Exec_ABORT(int handle); 
u_int U2F_Exec_FECRST(int handle);
u_int U2F_Exec_SWTRG(int handle); 
u_int U2F_Exec_TRG_CLR(int handle); 
u_int U2F_Exec_RS_DMEM1(int handle);  
u_int U2F_Exec_RS_DMEM2(int handle);
u_int U2F_ReadOut(int handle, u_int bsize, u_int *osize, u_char data[], 
		  u_int mode);


/***********************************/ 
/*Internal functions of the library*/
/***********************************/
u_int U2F_err_get(err_pack err, err_str pid, err_str code);
  

#ifdef __cplusplus
}
#endif

#endif
